package edge.bundling.util;

import java.awt.Point;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class Geometric {

    //*************************************************************************************************************
    /**
     * Calcula o feixo convexo entre uma lista de pontos
     *
     * @param points - pontos de entrada
     * @return lista dos pontos do feixo convexo
     */
//*************************************************************************************************************     
    public static ArrayList<Point.Double> convexHull(ArrayList<Point.Double> points) {
        ArrayList<Point.Double> pointsOfHull = new ArrayList<Point.Double>();

        // Se existir apenas três pontos eles serão o feixo convexo
        if (points.size() < 3) {
            return points;
        }

        List<Point.Double> hull = new ArrayList<Point.Double>();

        // Encontra o ponto mais a esquerda
        int l = 0;
        for (int i = 1; i < points.size(); i++) {
            if (points.get(i).x < points.get(l).x) {
                l = i;
            }
        }

        //Iniciando com o ponto mais a esquerda move-se no setindo antiorário até 
        //alcançar o ponto inicial
        int p = l, q;
        do {
            // Adiciona o ponto corrente ao feixo
            hull.add(points.get(p));

            //Busca pelo ponto q cuja orientação (p, x, q) é horária para todos os pontos x
            q = (p + 1) % points.size();
            for (int i = 0; i < points.size(); i++) {
                //Se e é mais antiorário que p, atualiza q
                if (orientation(points.get(p), points.get(i), points.get(q)) == 2) {
                    q = i;
                }
            }

            //q é o mais antiorário com base em p. Seta p como q para a próxima iteração
            p = q;
            //System.out.println("p:" + p + " l: " + l);
        } while (p != l);

        ListIterator<Point.Double> listIterator = hull.listIterator();
        while (listIterator.hasNext()) {
            pointsOfHull.add(listIterator.next());
        }

        return pointsOfHull;
    }

    //*************************************************************************************************************
    /**
     * Encontra a orientação da tripla (p, q, r)
     *
     * @param p
     * @param q
     * @param r
     * @return orientação: 0 se forem colineares, 1 se for horário e 2 se
     * antiorário
     */
//*************************************************************************************************************     
    private static int orientation(Point.Double p, Point.Double q, Point.Double r) {
        int val = (int) ((q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y));
        if (val == 0) {
            return 0;
        }
        return (val > 0) ? 1 : 2;
    }

    //*************************************************************************************************************
    /**
     * Calcula a distância euclidiana entre dois pontos (tamanho da aresta)
     *
     * @param P - ponto 1
     * @param Q - ponto 2
     * @return distance
     */
//*************************************************************************************************************     
    public static double euclideanDistance(Point.Double p, Point.Double q) {
        return Math.sqrt(Math.pow(p.x - q.x, 2) + Math.pow(p.y - q.y, 2));
    }

    //*************************************************************************************************************
    /**
     * Calcula a visibilidade entre duas arestas
     *
     * @param P1 - ponto 1 da aresta 1
     * @param P2 - ponto 2 da aresta 1
     * @param P3 - ponto 1 da aresta 2
     * @param P4 - ponto 2 da aresta 2
     * @return visibility
     */
//*************************************************************************************************************     
    public static double edgeVisibility(Point.Double P1, Point.Double P2, Point.Double P3, Point.Double P4) {
        Point.Double I0 = projectPointOnLine(P3, P1, P2);
        Point.Double I1 = projectPointOnLine(P4, P1, P2);

        double x = (I0.x + I1.x) / 2.0;
        double y = (I0.y + I1.y) / 2.0;
        Point.Double midI = new Point.Double(x, y);

        x = (P1.x + P2.x) / 2.0;
        y = (P1.y + P2.y) / 2.0;
        Point.Double midP = new Point.Double(x, y);

        double result = Math.max(0, (1 - ((2 * euclideanDistance(midP, midI)) / euclideanDistance(I0, I1))));

        return result;
    }

    //*************************************************************************************************************
    /**
     * Calcula a projeção de um ponto em uma linha
     *
     * @param p - ponto a ser projetado
     * @param P - ponto 1 da linha
     * @param Q - ponto 2 da linha
     * @return proint
     */
//*************************************************************************************************************     
    public static Point.Double projectPointOnLine(Point.Double p, Point.Double P, Point.Double Q) {
        double L = Math.sqrt((Q.x - P.x) * (Q.x - P.x) + (Q.y - P.y) * (Q.y - P.y));
        double r = ((P.y - p.y) * (P.y - Q.y) - (P.x - p.x) * (Q.x - P.x)) / (L * L);

        double x = P.x + r * (Q.x - P.x);
        double y = P.y + r * (Q.y - P.y);
        Point.Double point = new Point.Double(x, y);
        return point;
    }

    //*************************************************************************************************************
    /**
     * Calcula o angulo entre duas retas
     *
     * @param P1 - ponto 1 da reta 1
     * @param P2 - ponto 2 da reta 1
     * @param P3 - ponto 1 da reta 2
     * @param P4 - ponto 2 da reta 2
     * @return angle
     */
//*************************************************************************************************************     
    public static double angle(Point.Double p1, Point.Double p2, Point.Double p3, Point.Double p4) {
        double angle1 = 0;
        double angle2 = 0;
        double angle = 0;
        double valueAngle = 0;
        BigDecimal value = null;

        //Fórmula dos artigos
        //double num = vectorDotProduct(edgeAsVector(p2, p), edgeAsVector(p3, p));
        //double den = (edgeLength(p2, p) * edgeLength(p3, p));
        //double result = Math.abs(num / den);
        //Minha fórmula
        angle1 = Math.atan2(p1.y - p2.y, p1.x - p2.x);
        angle2 = Math.atan2(p3.y - p4.y, p3.x - p4.x);
        if (Math.toDegrees(angle1 - angle2) > 180) {
            valueAngle = Math.abs(360 - Math.toDegrees(angle1 - angle2));
        } else {
            valueAngle = Math.abs(Math.toDegrees(angle1 - angle2));
        }
        value = new BigDecimal(valueAngle).setScale(0, RoundingMode.HALF_DOWN);
        angle = (double) value.doubleValue();

        if (angle > 180) {
            angle = 360 - angle;
        }

        return (double) angle;
    }

    //*************************************************************************************************************
    /**
     * Normaliza um valor de angulo
     *
     * @param angle - angulo a ser normalizado
     * @return value
     */
//*************************************************************************************************************     
    public static double angleNormalized(double angle) {
        double v = 0;
        BigDecimal value = null;
        v = 1 - (angle * 0.5) / 90;
        value = new BigDecimal(v).setScale(3, RoundingMode.HALF_DOWN);
        return (double) value.doubleValue();
    }
}
