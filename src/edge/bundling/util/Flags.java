package edge.bundling.util;

import edge.bundling.graph.Graph;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Flags {

    private static boolean LABEL = true;
    private static boolean COVER = true;   
    private static boolean LOG = false;
    private static Graph GRAPH = new Graph(false);
    private static String NAMEFILE = "";   
    private static String TITLE = "..:: Evolutionary Edge Bundling ::..";
    private static List COVERLIST = new ArrayList();
    
    private static int SELECTIVE_PRESSURE = 2;
    private static int STEPCONVEXHUL = 2;   
    private static int PENALTY = -1;
    private static boolean EB_STAR = false;

    private static Color DEFAULTCOLORSOURCE = new Color(255, 0, 0);
    private static Color DEFAULTCOLORTARGET = new Color(153, 255, 153);
    private static Color DEFAULTCOLOREDGE = new Color(204, 204, 204, 255);
    private static Color DEFAULTCOLORNODE = new Color(0,0,0);
    private static Color DEFAULTCOLORLABELNODE = new Color(255,255,255);

    private static Color COLORSOURCE = new Color(255, 0, 0);
    private static Color COLORMIDDLE = new Color(255, 0, 0, 10);
    private static Color COLORTARGET = new Color(153, 255, 153);   
    private static Color COLOREDGE = new Color(204, 204, 204, 255);
    private static Color COLORNODE = new Color(0,0,0);
    private static Color COLORLABELNODE = new Color(255,255,255);
    
    //private static Color COLORTARGET = new Color(254, 241, 241);
    //private static Color COLOREDGE = new Color(237, 235, 235);
    //private static Color COLOREDGE = new Color(245, 245, 245);
    //private static Color COLORMIDDLE = new Color(255, 204,204);
    
    private static boolean PED = false;
    private static boolean PEDSOURCE = true;
    private static boolean PEDTARGET = false;
    private static int PEDTRANSPARENT = 10;
   
    private static int XSCREENSIZE = 687;
    private static int YSCREENSIZE = 980;
    private static int SCALEDEFAULT = 20;
    private static int PREVIOUSSCALE = 20;
    private static int SCALEUSER = 20;
    private static int ZOOMFACTOR = 5;
    private static int FONTSIZE = 6;
    private static int SCALEPANEL = 250;

    private static boolean SOUND = false;
    private static boolean AUTOMATIC_SAVING = false;

    private static boolean STATISTICS = false;
    private static boolean DATATABLE = false;

    private static boolean ANGULAR = true;
    private static boolean SCALE = true;
    private static boolean POSITION = false;
    private static boolean VISIBILITY = false;
    
    private static boolean OBJECTIVE =true;
    private static boolean CONSTRAINT = false;

    private static double COMPATIBILITY_ANGULAR_THRESHOLD_EB = 45.0; 
    private static double COMPATIBILITY_MAX_ANGULAR_THRESHOLD = 0.833;    //0.750; //0 -  1; 30 - 0.833; 45 - 0.750; 70 - 0.611; 110 - 0.389; 180 - 0
    private static double COMPATIBILITY_MAX_SCALE_THRESHOLD =  0.890;     //0.890;
    private static double COMPATIBILITY_MAX_POSITION_THRESHOLD = 0.710;
    private static double COMPATIBILITY_MAX_VISIBILITY_THRESHOLD = 0.960;   
    private static double COMPATIBILITY_MIN_ANGULAR_THRESHOLD =  1;  
    private static double COMPATIBILITY_MIN_SCALE_THRESHOLD = 1;
    private static double COMPATIBILITY_MIN_POSITION_THRESHOLD = 1;
    private static double COMPATIBILITY_MIN_VISIBILITY_THRESHOLD = 1;

    private static int TAM_POPULATION = 150;
    private static int MAX_GENERATION = 500;
    private static int MAX_GENERATION_TO_WAIT = 500;
    private static int MAX_GENERATION_FINAL = 500;
    private static float CROSSOVER_RATIO = (float) 0.98;
    private static float MUTATION_RATIO = (float) 0.40;

    private static boolean ONE_POINT = true;
    private static boolean UNIFORM = false;

    private static boolean MUTATION_ONE = true;
    private static boolean MUTATION_TWO = true;
    private static boolean MUTATION_THREE = true;
    private static boolean MUTATION_FOUR = true;
    private static boolean MUTATION_FIVE = true;

    private static boolean ROULETTE_WHEEL = false;
    private static boolean RANKING = false;
    private static boolean TOURNAMENT = true;
    private static boolean RANDOM = false;

    private static int ROULETTE_INTERACTION = 100;
    private static int TOURNAMENT_TOUR = 5;

    private static double COMPATIBILITYWEIGHT = 0.2;
    private static double BUNDLESYWEIGHT = 0.8;

    private static boolean TERMINATION_BY_GENERATION = false;
    private static boolean TERMINATION_BY_DIVERSITY = true;

   
    public static boolean isANGULAR() {
        return ANGULAR;
    }

    public static void ANGULAR(boolean a) {
        ANGULAR = a;
        GraphData.initializeCompatibility(GRAPH());
    }

    public static boolean isSCALE() {
        return SCALE;
    }

    public static void SCALE(boolean s) {
        SCALE = s;
        GraphData.initializeCompatibility(GRAPH());
    }

    
    
    
    public static boolean isPOSITION() {
        return POSITION;
    }

    public static void POSITION(boolean p) {
        POSITION = p;
        GraphData.initializeCompatibility(GRAPH());
    }

    public static boolean isVISIBILITY() {
        return VISIBILITY;
    }

    public static void VISIBILITY(boolean aVisibility) {
        VISIBILITY = aVisibility;
        GraphData.initializeCompatibility(GRAPH());
    }

    public static int TAM_POPULATION() {
        return TAM_POPULATION;
    }

    public static void TAM_POPULATION(int aTAM_POPULATION) {
        TAM_POPULATION = aTAM_POPULATION;
    }

    public static int MAX_GENERATION() {
        return MAX_GENERATION;
    }

    public static void MAX_GENERATION(int aMAX_GENERATION) {
        MAX_GENERATION = aMAX_GENERATION;
    }

    public static float CROSSOVER_RATIO() {
        return CROSSOVER_RATIO;
    }

    public static void CROSSOVER_RATIO(float aCROSSOVER_RATIO) {
        CROSSOVER_RATIO = aCROSSOVER_RATIO;
    }

    public static float MUTATION_RATIO() {
        return MUTATION_RATIO;
    }

    public static void MUTATION_RATIO(float aMUTATION_RATIO) {
        MUTATION_RATIO = aMUTATION_RATIO;
    }

    public static int ROULETTE_INTERACTION() {
        return ROULETTE_INTERACTION;
    }

    public static void ROULETTE_INTERACTION(int aROULETTE_INTERACTION) {
        ROULETTE_INTERACTION = aROULETTE_INTERACTION;
    }

    public static double COMPATIBILITY_MAX_ANGULAR_THRESHOLD() {
        return COMPATIBILITY_MAX_ANGULAR_THRESHOLD;
    }

    public static void COMPATIBILITY_MAX_ANGULAR_THRESHOLD(double aSTART_COMPATIBILITY_ANGULAR_THRESHOLD) {
        COMPATIBILITY_MAX_ANGULAR_THRESHOLD = aSTART_COMPATIBILITY_ANGULAR_THRESHOLD;
    }

    public static double COMPATIBILITY_MAX_SCALE_THRESHOLD() {
        return COMPATIBILITY_MAX_SCALE_THRESHOLD;
    }

    public static void COMPATIBILITY_MAX_SCALE_THRESHOLD(double aSTART_COMPATIBILITY_SCALE_THRESHOLD) {
        COMPATIBILITY_MAX_SCALE_THRESHOLD = aSTART_COMPATIBILITY_SCALE_THRESHOLD;
    }

    public static double COMPATIBILITY_MAX_POSITION_THRESHOLD() {
        return COMPATIBILITY_MAX_POSITION_THRESHOLD;
    }

    public static void COMPATIBILITY_MAX_POSITION_THRESHOLD(double aSTART_COMPATIBILITY_POSITION_THRESHOLD) {
        COMPATIBILITY_MAX_POSITION_THRESHOLD = aSTART_COMPATIBILITY_POSITION_THRESHOLD;
    }

    public static double COMPATIBILITY_MAX_VISIBILITY_THRESHOLD() {
        return COMPATIBILITY_MAX_VISIBILITY_THRESHOLD;
    }

    public static void COMPATIBILITY_MAX_VISIBILITY_THRESHOLD(double aSTART_COMPATIBILITY_VISIBILITY_THRESHOLD) {
        COMPATIBILITY_MAX_VISIBILITY_THRESHOLD = aSTART_COMPATIBILITY_VISIBILITY_THRESHOLD;
    }

    public static int XSCREENSIZE() {
        return XSCREENSIZE;
    }

    public static int YSCREENSIZE() {
        return YSCREENSIZE;
    }

    public static int PREVIOUSSCALE() {
        return PREVIOUSSCALE;
    }

    public static void PREVIOUSSCALE(int aScaleDefault) {
        PREVIOUSSCALE = aScaleDefault;
    }

    public static int SCALE() {
        return SCALEUSER;
    }

    public static void SCALE(int aScaleUser) {
        SCALEUSER = aScaleUser;
    }

    public static int ZOOMFACTOR() {
        return ZOOMFACTOR;
    }

    public static void ZOOMFACTOR(int aZoomInOut) {
        ZOOMFACTOR = aZoomInOut;
    }

    public static int FONTSIZE() {
        return FONTSIZE;
    }

    public static void FONTSIZE(int aFontSize) {
        FONTSIZE = aFontSize;
    }

    public static List COVERLIST() {
        return COVERLIST;
    }

    public static void COVERLIST(List aCoverList) {
        COVERLIST = aCoverList;
    }

    public static boolean isAUTOMATIC_SAVING() {
        return AUTOMATIC_SAVING;
    }

    public static void AUTOMATIC_SAVING(boolean aAutomaticSaving) {
        AUTOMATIC_SAVING = aAutomaticSaving;
    }

    public static double BUNDLESYWEIGHT() {
        return BUNDLESYWEIGHT;
    }

    public static void BUNDLESYWEIGHT(double aBUNDLES) {
        BUNDLESYWEIGHT = aBUNDLES;
    }

    public static double COMPATIBILITYYWEIGHT() {
        return COMPATIBILITYWEIGHT;
    }

    public static void COMPATIBILITYYWEIGHT(double aCOMPATIBILITY) {
        COMPATIBILITYWEIGHT = aCOMPATIBILITY;
    }

    public static boolean isONE_POINT() {
        return ONE_POINT;
    }

    public static void ONE_POINT(boolean aOnePoint) {
        ONE_POINT = aOnePoint;
    }

    public static boolean isUNIFORM() {
        return UNIFORM;
    }

    public static void UNIFORM(boolean aUniform) {
        UNIFORM = aUniform;
    }

    public static boolean isMUTATION_JOIN() {
        return MUTATION_ONE;
    }

    public static void MUTATION_ONE(boolean aMutation1) {
        MUTATION_ONE = aMutation1;
    }

    public static boolean isMUTATION_SPLIT() {
        return MUTATION_TWO;
    }

    public static void MUTATION_TWO(boolean aMutation2) {
        MUTATION_TWO = aMutation2;
    }

    public static boolean isMUTATION_MOVE() {
        return MUTATION_THREE;
    }

    public static void MUTATION_THREE(boolean aMutation3) {
        MUTATION_THREE = aMutation3;
    }

    public static boolean isMUTATION_MERGE() {
        return MUTATION_FOUR;
    }

    public static void MUTATION_FOUR(boolean aMutation4) {
        MUTATION_FOUR = aMutation4;
    }

    public static boolean isMUTATION_REMOVE() {
        return MUTATION_FIVE;
    }

    public static void MUTATION_FIVE(boolean aMutation5) {
        MUTATION_FIVE = aMutation5;
    }

    public static boolean isSTATISTICS() {
        return STATISTICS;
    }

    public static void STATISTICS(boolean aStatistics) {
        STATISTICS = aStatistics;
    }

    public static Graph GRAPH() {
        return GRAPH;
    }

    public static void GRAPH(Graph aGraph) {
        GRAPH = aGraph;
    }

    public static boolean isDATATABLE() {
        return DATATABLE;
    }

    public static void DATATABLE(boolean aDataTable) {
        DATATABLE = aDataTable;
    }

    public static boolean isOBJECTIVE() {
        return OBJECTIVE;
    }

    public static void OBJECTIVE(boolean aObjective) {
        OBJECTIVE = aObjective;
    }

    public static boolean isCONSTRAINT() {
        return CONSTRAINT;
    }

    public static void CONSTRAINT(boolean aConstraint) {
        CONSTRAINT = aConstraint;
    }

    public static boolean isSOUND() {
        return SOUND;
    }

    public static void SOUND(boolean aSound) {
        SOUND = aSound;
    }

    public static boolean isROULETTE_WHEEL() {
        return ROULETTE_WHEEL;
    }

    public static void ROULETTE_WHEEL(boolean aROULETTEWHEEL) {
        ROULETTE_WHEEL = aROULETTEWHEEL;
    }

    public static boolean isRANKING() {
        return RANKING;
    }

    public static void RANKING(boolean aRANKING) {
        RANKING = aRANKING;
    }

    public static boolean isTOURNAMENT() {
        return TOURNAMENT;
    }

    public static void TOURNAMENT(boolean aTOURNAMENT) {
        TOURNAMENT = aTOURNAMENT;
    }

    public static boolean isRANDOM() {
        return RANDOM;
    }

    public static void RANDOM(boolean aRANDOM) {
        RANDOM = aRANDOM;
    }

    public static int TOURNAMENT_TOUR() {
        return TOURNAMENT_TOUR;
    }

    public static void TOURNAMENT_TOUR(int aTOURNAMENT_INTERACTION) {
        TOURNAMENT_TOUR = aTOURNAMENT_INTERACTION;
    }

    public static int PENALTY() {
        return PENALTY;
    }

    public static void PENALTY(int aPENALTY) {
        PENALTY = aPENALTY;
    }

    public static boolean isTERMINATION_BY_GENERATION() {
        return TERMINATION_BY_GENERATION;
    }

    public static void TERMINATION_BY_GENERATION(boolean aCONVERGENCE_BY_GENERATION) {
        TERMINATION_BY_GENERATION = aCONVERGENCE_BY_GENERATION;
    }

    public static boolean isTERMINATION_BY_DIVERSITY() {
        return TERMINATION_BY_DIVERSITY;
    }

    public static void TERMINATION_BY_DIVERSITY(boolean aCONVERGENCE_BY_DIVERSITY) {
        TERMINATION_BY_DIVERSITY = aCONVERGENCE_BY_DIVERSITY;
    }

    public static boolean isEB_STAR() {
        return EB_STAR;
    }

    public static void EB_STAR(boolean aEB_STAR) {
        EB_STAR = aEB_STAR;
        GraphData.initializeData(GRAPH());
    }

    public static double COMPATIBILITY_ANGULAR_THRESHOLD_EB() {
        return COMPATIBILITY_ANGULAR_THRESHOLD_EB;
    }

    public static void COMPATIBILITY_ANGULAR_THRESHOLD_EB(double aCOMPATIBILITY_ANGULAR_THRESHOLD_EB) {
        COMPATIBILITY_ANGULAR_THRESHOLD_EB = aCOMPATIBILITY_ANGULAR_THRESHOLD_EB;
    }

    public static int SELECTIVE_PRESSURE() {
        return SELECTIVE_PRESSURE;
    }

    public static void SELECTIVE_PRESSURE(int aSELECTIVE_PRESSURE) {
        SELECTIVE_PRESSURE = aSELECTIVE_PRESSURE;
    }

    public static String NAMEFILE() {
        return NAMEFILE;
    }

    public static void NAMEFILE(String aNameFile) {
        NAMEFILE = aNameFile;
    }

    public static boolean isLOG() {
        return LOG;
    }

    public static void LOG(boolean aLOG) {
        LOG = aLOG;
    }
    
    public static boolean isPED() {
        return PED;
    }

    public static void PED(boolean aPED) {
        PED = aPED;
    }

    public static Color COLORSOURCE() {
        return COLORSOURCE;
    }

    public static void COLORSOURCE(Color aColorSource) {
        COLORSOURCE = aColorSource;
    }

    public static Color COLORTARGET() {
        return COLORTARGET;
    }

    public static void COLORTARGET(Color aColorTarget) {
        COLORTARGET = aColorTarget;
    }

    public static Color DEFAULTCOLORSOURCE() {
        return DEFAULTCOLORSOURCE;
    }

    public static Color DEFAULTCOLORTARGET() {
        return DEFAULTCOLORTARGET;
    }

    public static Color COLOREDGE() {
        return COLOREDGE;
    }

    public static void COLOREDGE(Color aCOLOREDGE) {
        COLOREDGE = aCOLOREDGE;
    }

    public static Color DEFAULTCOLOREDGE() {
        return DEFAULTCOLOREDGE;
    }

    public static String TITLE() {
        return TITLE;
    }

    public static void TITLE(String aTITLE) {
        TITLE = aTITLE;
    }

    public static boolean isLABEL() {
        return LABEL;
    }

    public static void LABEL(boolean aLABEL) {
        LABEL = aLABEL;
    }

    public static double COMPATIBILITY_MIN_ANGULAR_THRESHOLD() {
        return COMPATIBILITY_MIN_ANGULAR_THRESHOLD;
    }

    public static void COMPATIBILITY_MIN_ANGULAR_THRESHOLD(double aCOMPATIBILITY_MIN_ANGULAR_THRESHOLD) {
        COMPATIBILITY_MIN_ANGULAR_THRESHOLD = aCOMPATIBILITY_MIN_ANGULAR_THRESHOLD;
    }

    public static double COMPATIBILITY_MIN_SCALE_THRESHOLD() {
        return COMPATIBILITY_MIN_SCALE_THRESHOLD;
    }

    public static void COMPATIBILITY_MIN_SCALE_THRESHOLD(double aCOMPATIBILITY_MIN_SCALE_THRESHOLD) {
        COMPATIBILITY_MIN_SCALE_THRESHOLD = aCOMPATIBILITY_MIN_SCALE_THRESHOLD;
    }

    public static double COMPATIBILITY_MIN_POSITION_THRESHOLD() {
        return COMPATIBILITY_MIN_POSITION_THRESHOLD;
    }

    public static void COMPATIBILITY_MIN_POSITION_THRESHOLD(double aCOMPATIBILITY_MIN_POSITION_THRESHOLD) {
        COMPATIBILITY_MIN_POSITION_THRESHOLD = aCOMPATIBILITY_MIN_POSITION_THRESHOLD;
    }

    public static double COMPATIBILITY_MIN_VISIBILITY_THRESHOLD() {
        return COMPATIBILITY_MIN_VISIBILITY_THRESHOLD;
    }

    public static void COMPATIBILITY_MIN_VISIBILITY_THRESHOLD(double aCOMPATIBILITY_MIN_VISIBILITY_THRESHOLD) {
        COMPATIBILITY_MIN_VISIBILITY_THRESHOLD = aCOMPATIBILITY_MIN_VISIBILITY_THRESHOLD;
    }

    public static int STEPCONVEXHUL() {
        return STEPCONVEXHUL;
    }

    public static void STEPCONVEXHUL(int aSTEPCONVEXHUL) {
        STEPCONVEXHUL = aSTEPCONVEXHUL;
    }

    public static int SCALEDEFAULT() {
        return SCALEDEFAULT;
    }

    public static void SCALEDEFAULT(int aSCALEDEFAULT) {
        SCALEDEFAULT = aSCALEDEFAULT;
    }

    public static boolean isCOVER() {
        return COVER;
    }

    public static void COVER(boolean aCOVER) {
        COVER = aCOVER;
    }
    
    public static int SCALEPANEL() {
        return SCALEPANEL;
    }

    public static void SCALEPANEL(int aSCALEPANEL) {
        SCALEPANEL = aSCALEPANEL;
    }

    /**
     * @return the DEFAULTCOLORMIDDLE
     */
    public static Color COLORMIDDLE() {
        return COLORMIDDLE;
    }

    /**
     * @param aDEFAULTCOLORMIDDLE the DEFAULTCOLORMIDDLE to set
     */
    public static void COLORMIDDLE(Color aCOLORMIDDLE) {
       COLORMIDDLE = aCOLORMIDDLE;
    }

    /**
     * @return the MAX_GENERATION_TO_WAIT
     */
    public static int MAX_GENERATION_TO_WAIT() {
        return MAX_GENERATION_TO_WAIT;
    }

    /**
     * @param aMAX_GENERATION_TO_WAIT the MAX_GENERATION_TO_WAIT to set
     */
    public static void MAX_GENERATION_TO_WAIT(int aMAX_GENERATION_TO_WAIT) {
        MAX_GENERATION_TO_WAIT = aMAX_GENERATION_TO_WAIT;
    }

    /**
     * @return the MAX_GENERATION_TO_FINAL
     */
    public static int MAX_GENERATION_FINAL() {
        return MAX_GENERATION_FINAL;
    }

    /**
     * @param aMAX_GENERATION_TO_FINAL the MAX_GENERATION_TO_FINAL to set
     */
    public static void MAX_GENERATION_FINAL(int aMAX_GENERATION_FINAL) {
        MAX_GENERATION_FINAL = aMAX_GENERATION_FINAL;
    }

    /**
     * @return the PEDSOURCE
     */
    public static boolean isPEDSOURCE() {
        return PEDSOURCE;
    }

    /**
     * @param aPEDSOURCE the PEDSOURCE to set
     */
    public static void PEDSOURCE(boolean aPEDSOURCE) {
        PEDSOURCE = aPEDSOURCE;
    }

    /**
     * @return the PEDTARGET
     */
    public static boolean isPEDTARGET() {
        return PEDTARGET;
    }

    /**
     * @param aPEDTARGET the PEDTARGET to set
     */
    public static void PEDTARGET(boolean aPEDTARGET) {
        PEDTARGET = aPEDTARGET;
    }

    /**
     * @return the PEDTRANSPARENT
     */
    public static int getPEDTRANSPARENT() {
        return PEDTRANSPARENT;
    }

    /**
     * @param aPEDTRANSPARENT the PEDTRANSPARENT to set
     */
    public static void setPEDTRANSPARENT(int aPEDTRANSPARENT) {
        PEDTRANSPARENT = aPEDTRANSPARENT;
    }

    /**
     * @return the COLORNODE
     */
    public static Color COLORNODE() {
        return COLORNODE;
    }

    /**
     * @param aCOLORNODE the COLORNODE to set
     */
    public static void COLORNODE(Color aCOLORNODE) {
        COLORNODE = aCOLORNODE;
    }

    /**
     * @return the DEFAULTCOLORNODE
     */
    public static Color DEFAULTCOLORNODE() {
        return DEFAULTCOLORNODE;
    }

    /**
     * @param aDEFAULTCOLORNODE the DEFAULTCOLORNODE to set
     */
    public static void DEFAULTCOLORNODE(Color aDEFAULTCOLORNODE) {
        DEFAULTCOLORNODE = aDEFAULTCOLORNODE;
    }

    /**
     * @return the DEFAULTCOLORLETTER
     */
    public static Color DEFAULTCOLORLABELNODE() {
        return DEFAULTCOLORLABELNODE;
    }

    /**
     * @param aDEFAULTCOLORLETTER the DEFAULTCOLORLETTER to set
     */
    public static void DEFAULTCOLORLABELNODE(Color aDEFAULTLABELNODE) {
        DEFAULTCOLORLABELNODE =  aDEFAULTLABELNODE;
    }

    /**
     * @return the COLORLETTER
     */
    public static Color COLORLABELNODE() {
        return COLORLABELNODE;
    }

    /**
     * @param aCOLORLETTER the COLORLETTER to set
     */
    public static void COLORLABELNODE(Color aCOLORLABELNODE) {
        COLORLABELNODE = aCOLORLABELNODE;
    }
    
}
