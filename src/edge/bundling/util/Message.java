package edge.bundling.util;

import edge.bundling.graph.GraphBundled;
import edge.bundling.methods.genetic.Individual;
import edge.bundling.statistics.GlobalStatistic;
import edge.bundling.statistics.LocalStatistic;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Message {

    public static String individialDataFile(Individual solution, boolean cover, String text) {
        String msgLog = "";

        if (cover) {
            msgLog += solution.showCoverList() + "\n";
        }

        msgLog += "\n-----------------------------------------------------------------------------------";
        msgLog += "\n" + text + "\n" + solution.showBundles();
        msgLog += "\n[Generation]\t\t: " + solution.getGeneration() + "\n";
        msgLog += "[Compatibility (MAX)]\t\t: " + solution.getCompatibility() + "\n";
        msgLog += "[Number Of Bundles (MIN)]\t: " + solution.getNumberOfBundles() + "\n";
        msgLog += "[Fitness (MAX)]\t\t\t: " + solution.getFitness() + "\n";
        msgLog += "[Density]\t\t\t: " + solution.getDensity() + "\n";
        msgLog += "[Bundles Bigger Than One]\t: " + solution.getNumberOfBundlesMoreThanOne() + "\n";
        msgLog += "[Bundles Equal One]\t\t: " + solution.getNumberOfBundlesEqualOne() + "\n";
        msgLog += "[Size Edge]\t\t\t: " + solution.getSizeEdge() + "\n";
        msgLog += "[Number Of Bundles/Size Edge]\t: " + (double) ((double) solution.getNumberOfBundles() / (double) solution.getSizeEdge()) + "\n";
        msgLog += "-----------------------------------------------------------------------------------\n";

        return msgLog;
    }

    public static String individialData(Individual solution, boolean cover, String text) {
        String msgLog = "";

        if (cover) {
            msgLog += solution.showCoverList() + "\n";
        }

        msgLog += "\n" + text + "\n" + solution.showBundles();
        msgLog += "\n[Generation]\t\t: " + solution.getGeneration() + "\n";
        msgLog += "[Density]\t\t: " + solution.getDensity() + "\n";
        msgLog += "[Bundles Bigger Than One]\t: " + solution.getNumberOfBundlesMoreThanOne() + "\n";
        msgLog += "[Bundles Equal One]\t: " + solution.getNumberOfBundlesEqualOne() + "\n";
        msgLog += "[Size Edge]\t\t: " + solution.getSizeEdge() + "\n";
        msgLog += "[Number Of Bundles/Size Edge]: " + (double) ((double) solution.getNumberOfBundles() / (double) solution.getSizeEdge()) + "\n";
        msgLog += "[Compatibility (MAX)]\t: " + solution.getCompatibility() + "\n";
        msgLog += "[Number Of Bundles (MIN)]\t: " + solution.getNumberOfBundles() + "\n";
        msgLog += "[Fitness (MAX)]\t\t: " + solution.getFitness() + "\n";

        return msgLog;
    }

    public static String generateDataLog(int convergence, int size, int nBestIndvidual, Individual solution) {
        String msgLog = "";

        BigDecimal value;

        msgLog += convergence + "\t";
        msgLog += size + "\t";
        msgLog += nBestIndvidual + "\t";

        msgLog += solution.getGeneration() + "\t";
        msgLog += solution.getDensity() + "\t";
        msgLog += solution.getNumberOfBundlesMoreThanOne() + "\t";
        msgLog += solution.getNumberOfBundlesEqualOne() + "\t";
        msgLog += solution.getSizeEdge() + "\t";
        msgLog += ((double) ((double) solution.getNumberOfBundles() / (double) solution.getSizeEdge())) + "\t";
        msgLog += solution.getNumberOfBundles() + "\t";
        msgLog += new BigDecimal(solution.getCompatibility()).setScale(4, RoundingMode.HALF_DOWN).doubleValue() + "\t";
        msgLog += new BigDecimal(solution.getFitness()).setScale(4, RoundingMode.HALF_DOWN).doubleValue() + "\t";
        msgLog += Flags.MAX_GENERATION_FINAL() + "\t";
 
        return msgLog;
    }

    public static String statistics(int generations, GraphBundled bestSolution, GraphBundled worstSolution, LocalStatistic localData, GlobalStatistic globalData, boolean cover) {
        String msgLog = "";

        if (cover) {
            msgLog += bestSolution.showCoverList() + "\n";
        }
        DecimalFormat df = new DecimalFormat("0.#######");

        msgLog += generations + "\t";
        msgLog += df.format(bestSolution.getCompatibility()) + "\t";
        msgLog += bestSolution.getNumberOfBundles() + "\t";
        msgLog += df.format(bestSolution.getFitness()) + "\t";
        msgLog += df.format(bestSolution.getFitness()) + "\t" + df.format((localData.getSumLocalFitness() / localData.getQuantityLocalIndividuals())) + "\t" + df.format(worstSolution.getFitness()) + "\t";
        msgLog += df.format(globalData.getBestGlobalFitness()) + "\t" + df.format((globalData.getSumGlobalFitness() / globalData.getQuantityGlobalIndividuals())) + "\t" + df.format(globalData.getWorstGlobalFitness()) + "\n";

        return msgLog;
    }

    public static String statistics(Individual bestSolution) {
        String msgLog = "";

        DecimalFormat df = new DecimalFormat("0.#######");

        msgLog += "\n\n" + bestSolution.getGeneration() + "\t";
        msgLog += df.format(bestSolution.getCompatibility()) + "\t";
        msgLog += bestSolution.getNumberOfBundles() + "\t";
        msgLog += df.format(bestSolution.getFitness()) + "\n";

        return msgLog;
    }
}
