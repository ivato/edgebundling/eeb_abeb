package edge.bundling.util;

import java.util.Random;

public class Utils {

    
    
        
   public static boolean escape(Random rndSeed, int seedValue, int randomValue){
        int seed = rndSeed.nextInt(seedValue);
        Random rnd = new Random(seed);
        int continueFor1 = (int) rnd.nextInt(randomValue);
        if (continueFor1 == 0) {
            return true;
        }
        return false;
    }
    
    
    //*************************************************************************************************************     
    /**
     * Recupera a permutação de 1:N
     *
     * @param N valor limite da permitação
     * @return array com a permutação
     */
//*************************************************************************************************************     
    public static int[] getPermutation(int N) {
        int[] res = new int[N];
        int i, k;

        for (i = 0; i < N; i++) {
            res[i] = i + 1;
        }

        for (i = 0; i < N - 1; i++) {
            k = i + (int) (Math.random() * (N - i));
            int temp = res[i];
            res[i] = res[k];
            res[k] = temp;
        }
        return res;
    }

    //*************************************************************************************************************     
    /**
     * Retorna um número aleatório entre dois valores
     */
//*************************************************************************************************************     
    public static int getRandomInt(int lowValue, int highValue) {
        Random m_random = new Random();
        return (int) Math.floor(m_random.nextDouble() * (highValue - lowValue + 1)) + lowValue;
    }

    //*************************************************************************************************************     
    /**
     * Verifica se o valor passado como parâmetro é numérico
     *
     * @param value
     */
//*************************************************************************************************************     
    public static boolean isNumeric(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    
    public static boolean isNumericNegative(String value) {
        try {
            int val = Integer.parseInt(value);
            if (val >=0 ){
                return false;
            }
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    //*************************************************************************************************************     
    /**
     * Verifica se o valor passado como parâmetro é real
     *
     * @param value
     */
//*************************************************************************************************************     
    public static boolean isReal(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    //*************************************************************************************************************     
    /**
     * Verifica se um valor x é maior ou igual a y
     *
     * @param x
     * @param y
     */
//*************************************************************************************************************     
    public static boolean greatOrEqual(double x, double y) {
        if ((x - y) > (Math.abs(y) * (-1E-16))) {
            return true;
        }
        return false;
    }

    //*************************************************************************************************************     
    /**
     * Verifica se um valor x é menor ou igual a y
     *
     * @param x
     * @param y
     */
//*************************************************************************************************************     
    public static boolean lessOrEqual(double x, double y) {
        if ((y - x) > (Math.abs(x) * (-1E-16))) {
            return true;
        }
        return false;
    }

    //*************************************************************************************************************     
    /**
     * Verifica se um valor x é menor a y
     *
     * @param x
     * @param y
     */
//*************************************************************************************************************     
    public static boolean less(double x, double y) {
        if ((y - x) > (Math.abs(x) * (1E-16))) {
            return true;
        }
        return false;
    }

    //*************************************************************************************************************     
    /**
     * Verifica se um valor x é igual a y
     *
     * @param x
     * @param y
     */
//*************************************************************************************************************     
    public static boolean equal(double x, double y) {
        boolean res = false;
        if (Math.abs(x - y) < 1E-16) {
            return true;
        }
        return false;
    }

    //*************************************************************************************************************     
    /**
     * Ordena um array usando o algoritmo Bubble Sort
     *
     * @param array vetor de dados original
     * @param index vetor de índice
     * @return array ordenado em order descrescente
     */
//*************************************************************************************************************     
    static public double[] bubbleSort(double array[], int index[]) {
        for (int i = 0; i < (array.length - 1); i++) {
            for (int j = 1; j < (array.length - i); j++) {
                if (array[j - 1] < array[j]) {
                    double temp1 = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = temp1;

                    int temp2 = index[j];
                    index[j] = index[j - 1];
                    index[j - 1] = temp2;
                }
            }
        }
        return array;
    }
}
