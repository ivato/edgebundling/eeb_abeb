package edge.bundling.view.rendering;

import edge.bundling.graph.Bundle;
import edge.bundling.graph.Edge;
import edge.bundling.graph.Graph;
import edge.bundling.graph.GraphBundled;
import edge.bundling.graph.Node;
import edge.bundling.methods.force.ForceParams;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

public class RenderBundle {

    private LinkedList<Edge> edges;
    private GraphBundled graphBundled;
    private final ForceParams params;

    public RenderBundle(GraphBundled graph) {
        this.graphBundled = graph;
        this.params = new ForceParams();
        graph.startSubNodesList();

        //Recupera os feixes individualmente e executa o algoritmo de força em cada subgrafo induzido
        ListIterator<Bundle> listIterator = this.graphBundled.getBundleList().listIterator();
        while (listIterator.hasNext()) {
            Bundle bundle = new Bundle();
            bundle = listIterator.next();
            
            //Recupera a lista de arestas do feixe
            edges = bundle.getEdges();
            
            //Desenha as arestas da lista edge
            rendering();
        }
    }

    //*************************************************************************************************************
    /**
     * Desenha um conjunto de arestas usando uma abordagem de força
     *
     */
//*************************************************************************************************************
    private void rendering() {
        int cycles = params.CYCLES();
        int iterations = params.INTERACTIONS();
        double stepSize = params.STEPSIZE();
        double stiffness = params.STIFFINESS();

        //Controla os ciclos do processo de bundling
        for (int c = 0; c < cycles; c++) {

            //Aumenta a quantidade de seguimentos da aresta
            for (Edge e : edges) {
                e.increaseSubNodes();
            }

            //Controla a quantidade de iterações por ciclo
            for (int i = 0; i < iterations; i++) {

                for (Edge edgeP : edges) {

                    double kp = stiffness / edgeP.distance() * (edgeP.getSubNodes().size() - 1);

                    ArrayList<Node> tempSubNodes = new ArrayList<Node>();
                    tempSubNodes.add(edgeP.getStartNode());

                    for (int n = 1; n < edgeP.getSubNodes().size() - 1; n++) {
                        double electrostaticForceX = 0;
                        double electrostaticForceY = 0;

                        Node p = edgeP.getSubNodes().get(n);
                        Node pPrevious = edgeP.getSubNodes().get(n - 1);
                        Node pNext = edgeP.getSubNodes().get(n + 1);

                        // Calcula a força eletrostática
                        for (Edge edgeQ : edges) {
                            if (edgeP != edgeQ) {
                                Node q = edgeQ.getSubNodes().get(n);
                                double distance = Double.POSITIVE_INFINITY;
                                for (Node u : edgeQ.getSubNodes()) {
                                    if (p.distance(u) < distance) {
                                        distance = p.distance(u);
                                        q = u;
                                    }
                                }
                                if (p.distance(q) > 0) {
                                    electrostaticForceX += 1 * ((q.getX() - p.getX()) / p.distance(q));
                                    electrostaticForceY += 1 * ((q.getY() - p.getY()) / p.distance(q));
                                }
                            }
                        }

                        //Calcula a força de atração 
                        double springForceX = (((pPrevious.getX() - p.getX())) + ((pNext.getX() - p.getX())));
                        double springForceY = (((pPrevious.getY() - p.getY())) + ((pNext.getY() - p.getY())));

                        if (Math.abs(kp) < 1.0) {
                            springForceX *= kp;
                            springForceY *= kp;
                        }

                        //Move os sub-vértices
                        double x = p.getX() + (electrostaticForceX + springForceX) * stepSize;
                        double y = p.getY() + (electrostaticForceY + springForceY) * stepSize;

                        tempSubNodes.add(new Node(x, y));
                    }

                    tempSubNodes.add(edgeP.getEndNode());
                    edgeP.setTempSubNodes(tempSubNodes);
                }

                for (Edge e : edges) {
                    e.finalizeSubNodes();
                }
            }
            // Diminui a quantidade de iterações e stepSize por ciclo
            iterations = (int) Math.round(iterations * 2 / 3);
            stepSize = stepSize / 2;
        }
    }
   
}
