package edge.bundling.view.rendering;

import edge.bundling.controller.ControllerMainWindow;
import edge.bundling.graph.Edge;
import edge.bundling.graph.Node;
import edge.bundling.util.Flags;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

public class LinearDrawPanelGraph extends LinearAbstractDrawPanel {

    private static final long serialVersionUID = 1L;

    public LinearDrawPanelGraph() {
        setParameters();
    }

    public void registerController(ControllerMainWindow windowPanelController) {
        this.windowPanelController = windowPanelController;
    }

    //*************************************************************************************************************
    /**
     * Desenho o grafo inicial conforme informações o arquivo de entrada
     *
     * @param screen - área de plotagem na tela
     */
//*************************************************************************************************************                           
    public void paintComponent(Graphics screen) {
        
        Font font = new java.awt.Font("Arial", Font.BOLD, Flags.FONTSIZE());
        g2 = (Graphics2D) screen;
        //g2.scale(Flags.SCALEPANEL()/100, Flags.SCALEPANEL()/100);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setPaint(Color.WHITE);
        g2.setFont(font);
        
        for (int i = 0; i <  this.windowPanelController.getGraph().getEdgeList().size(); i++) {
            Edge e;
            e =  this.windowPanelController.getGraph().getEdgeList().get(i);
            drawEdge(g2, e.getStartNode().getX(), e.getStartNode().getY(), e.getEndNode().getX(), e.getEndNode().getY(), String.valueOf(e.getId()));
        }

        for (int i = 0; i <  this.windowPanelController.getGraph().getNodeList().size(); i++) {
            Node v;
            v =  this.windowPanelController.getGraph().getNodeList().get(i);
            drawNode(g2, v.getX(), v.getY(), v.getData(),  0); 
        }
    }
}
