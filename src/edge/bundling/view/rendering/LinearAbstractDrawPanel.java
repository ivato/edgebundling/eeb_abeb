package edge.bundling.view.rendering;

import edge.bundling.controller.ControllerMainWindow;
import edge.bundling.graph.Bundle;
import edge.bundling.graph.Edge;
import edge.bundling.graph.Graph;
import edge.bundling.graph.GraphBundled;
import edge.bundling.graph.Node;
import edge.bundling.methods.force.Force;
import edge.bundling.util.Flags;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.LinearGradientPaint;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.LinkedList;
import javax.swing.JPanel;

abstract class LinearAbstractDrawPanel extends JPanel {

    private static final long serialVersionUID = 1L;
    protected ControllerMainWindow windowPanelController;
    protected Graphics2D g2;
    protected boolean active = false;

    public void setParameters() {
        this.setBounds(0, 0, Flags.XSCREENSIZE(), Flags.YSCREENSIZE());
        this.setBackground(Color.WHITE);
    }

    //*************************************************************************************************************
    /**
     * Desenha os vértices
     *
     * @param cg - área de plotagem na tela
     * @param xCenter - coordenada x do centro do vértice
     * @param yCenter - coordenada y do centro do vértice
     * @param zoom - valor corrente do zoom
     * @param label - rótulo do vértice
     * @param hasLabel - marcação se a exibição do rótulo esta ativa
     * @param cover - indicação se o vértice pertence (1) ou não (0) a cobertura
     * de vértices
     */
//************************************************************************************************************* 
    public void drawNode(Graphics2D cg, double xCenter, double yCenter, String label, int cover) {

        int zoom = Flags.ZOOMFACTOR();

        /*if (cover == 0) {
            cg.setColor(Color.BLACK);
        } else {
            if (Flags.isCOVER()) {
                cg.setColor(Flags.COLORSOURCE());
            } else {
                cg.setColor(Color.BLACK);
            }
        }*/
        
        if (cover == 0) {
            cg.setColor(Flags.COLORNODE());
        } else {
            if (Flags.isCOVER()) {
                cg.setColor(Flags.COLORSOURCE());
            } else {
                cg.setColor(Flags.COLORNODE());
            }
        }

        cg.fillOval((int) xCenter - zoom, (int) yCenter - zoom, 2 * zoom, 2 * zoom);
        cg.setColor(Color.BLACK);
        cg.drawOval((int) xCenter - zoom, (int) yCenter - zoom, 2 * zoom, 2 * zoom);

        //cg.setColor(Color.white);
        cg.setColor(Flags.COLORLABELNODE());

        if (Flags.isLABEL()) {
            FontMetrics fm = cg.getFontMetrics();
            double textWidth = fm.getStringBounds(label, cg).getWidth();
            cg.setFont(new Font("Arial", Font.PLAIN, zoom - 2));
            cg.drawString(label, (int) (xCenter - textWidth / 2) + 1, (int) (yCenter - 3 + fm.getMaxAscent() / 2) + 1);
        }
    }

    //*************************************************************************************************************
    /**
     * Desenha as arestas
     *
     * @param cg - área de plotagem na tela
     * @param xStart - coordenada x do centro do vértice
     * @param yStart - coordenada y do centro do vértice
     * @param xEnd - coordenada x do centro do vértice
     * @param yEnd - coordenada y do centro do vértice
     * @param zoom - valor corrente do zoom
     * @param label - rótulo do vértice
     * @param hasLabel - marcação se a exibição do rótulo esta ativa
     */
//************************************************************************************************************* 
    public void drawEdge(Graphics2D cg, double xStart, double yStart, double xEnd, double yEnd, String label) {

        int zoom = Flags.ZOOMFACTOR();

        double medianX = (xStart + xEnd) / 2;
        double medianY = (yStart + yEnd) / 2;

        BasicStroke stroke = new BasicStroke(0.5f);   
        cg.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        cg.setStroke(stroke);
        cg.setColor(new Color(0, 0, 0));
        cg.draw(new Line2D.Double(xStart, yStart, xEnd, yEnd));

        if (Flags.isLABEL()) {
            FontMetrics fm = cg.getFontMetrics();
            double textWidth = fm.getStringBounds(label, cg).getWidth();
            cg.setFont(new Font("Arial", Font.PLAIN, 1 + zoom));
            cg.drawString(label, (int) (medianX - textWidth / 2), (int) (medianY - 3 + fm.getMaxAscent() / 2));
        }
    }

    //*************************************************************************************************************
    /**
     * Desenha os feixes do grafo oriundo das abordagens (Evolutionary and
     * Hybrid Edge Bundling)
     *
     * @param g2 - área de plotagem na tela
     * @param graph - grafo com feixes de arestas
     */
//*************************************************************************************************************     
    public void drawBundle(Graphics2D g2, GraphBundled graph) {

        int zoom = Flags.ZOOMFACTOR();
      
        BasicStroke stroke = new BasicStroke(0.5f);   
        g2.setStroke(stroke);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        
        GradientPaint gradient;
        LinearGradientPaint linearGradient;

        ArrayList<Bundle> bundles = graph.getBundleList();

        //Executa o algoritmo de força para cada feixe do grafo individualmente
        //Esta etapa subdivide as arestas
        RenderBundle force = new RenderBundle(graph);

        //Recupera os feixes e desenha os seguimentos de cada aresta
        for (Bundle bundle : bundles) {

            if (bundle.getSize() > 1) {

                LinkedList<Edge> edges = bundle.getEdges();

                Node centralNode = (Node) bundle.getCenterNode();
                int x1 = centralNode.getXInt();
                int y1 = centralNode.getYInt();
                int x2, y2;

                //Recupera as arestas do feixe
                for (Edge edge : edges) {

                    if (edge.getStartNode() != centralNode) {
                        x2 = edge.getStartNode().getXInt();
                        y2 = edge.getStartNode().getYInt();
                    } else {
                        x2 = edge.getEndNode().getXInt();
                        y2 = edge.getEndNode().getYInt();
                    }

                    //Estipula o gradiente do feixe
                    //gradient = new GradientPaint(x1, y1, Flags.COLORSOURCE(), x2, y2, Flags.COLORTARGET(), false);
                    //g2.setPaint(gradient);
                    
                    Point2D start = new Point2D.Float(x1, y1);
                    Point2D end = new Point2D.Float(x2, y2);
                    
                    if (Flags.isPED()){  
                        //float[] dist = {0.0f, 0.1f, 0.9f, 1.0f};  
                        //int alpha = 0; // 50% transparent
                        //Color myColour = new Color(255, 0, 0, alpha);
                        //Color white = new Color(255, 255, 255, alpha);
                        //Color[] colors = {Flags.COLORSOURCE(), myColour, white, Flags.COLORTARGET()}; 
                        
                        Color COLORTARGET = Flags.COLORTARGET();
                        Color COLORSOURCE = Flags.COLORSOURCE();
                        
                        int tranparency = Flags.getPEDTRANSPARENT();
                        int r = Flags.COLORSOURCE().getRed();
                        int g = Flags.COLORSOURCE().getGreen();
                        int b = Flags.COLORSOURCE().getBlue();
                        Color COLORMIDDLE = new Color(r,g,b,tranparency);
                        
                        
                        if (Flags.isPEDSOURCE()){
                            r = Flags.COLORTARGET().getRed();
                            g = Flags.COLORTARGET().getGreen();
                            b = Flags.COLORTARGET().getBlue();
                            COLORTARGET = new Color(r,g,b,tranparency);
                        }else{
                             if (Flags.isPEDTARGET()){
                                r = Flags.COLORSOURCE().getRed();
                                g = Flags.COLORSOURCE().getGreen();
                                b = Flags.COLORSOURCE().getBlue();
                                COLORSOURCE = new Color(r,g,b,tranparency);
                             }
                        }
                        
                        float[] dist = {0.0f, 0.3f, 0.7f, 1.0f};
                        //Color[] colors = {Flags.COLORSOURCE(), Flags.COLORMIDDLE(), Flags. COLORMIDDLE(), Flags.COLORTARGET()};      
                        Color[] colors = {COLORSOURCE, COLORMIDDLE, COLORMIDDLE, COLORTARGET};  
                        linearGradient = new LinearGradientPaint(start, end, dist, colors);
                        
                    }else{                  
                        //float[] dist =  {0.0f, 0.5f, 1.0f};  
                        
                        
                        float[] dist =  {0.0f,1.0f}; 
                        Color[] colors = {Flags.COLORSOURCE(), Flags.COLORTARGET()};
                        linearGradient = new LinearGradientPaint(start, end, dist, colors);
                    }
                    
                    g2.setPaint(linearGradient);
                   

                    //Desenha os seguimentos das arestas gerando o efeito visual de feixe
                    Path2D path = new Path2D.Double();
                    Point point = new Point(edge.subNodesXInt()[0], edge.subNodesYInt()[0]);
                    path.moveTo(point.x, point.y);
                    for (int i = 1; i < edge.getSubNodes().size(); i++) {
                        Point pointNext = new Point(edge.subNodesXInt()[i], edge.subNodesYInt()[i]);
                        path.lineTo(pointNext.x, pointNext.y);
                        point = new Point(edge.subNodesXInt()[i], edge.subNodesYInt()[i]);
                    }
                    g2.draw(path);
                }
            } else {
                //Caso o feixe só tenha uma aresta
                //desenha o feixe com uma cor sólida
                Color color = Flags.COLOREDGE();
                //Color color = new Color(237, 235, 235, 127);
                Edge edge = bundle.getEdges().get(0);
                g2.setColor(color);
                g2.drawLine(edge.getStartNode().getXInt(), edge.getStartNode().getYInt(), edge.getEndNode().getXInt(), edge.getEndNode().getYInt());
            }
        }

    }

    //*************************************************************************************************************
    /**
     * Desenha os feixes oriundos da abordagem de Star Force-Edge Bundling
     *
     * @param g2 - área de plotagem na tela
     * @param graph - grafo com feixes de arestas
     */
//*************************************************************************************************************     
    public void drawBundleForce(Graphics2D g2, GraphBundled graph) {
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        ArrayList<Edge> edges = graph.getEdgeList();
        
        BasicStroke stroke = new BasicStroke(0.5f);   
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setStroke(stroke);
        
        int r = Flags.COLORSOURCE().getRed();
        int g = Flags.COLORSOURCE().getGreen();
        int b = Flags.COLORSOURCE().getBlue();
        
        g2.setColor(new Color(r, g, b, 70));
        
        for (Edge edge : edges) {
            g2.drawPolyline(edge.subNodesXInt(), edge.subNodesYInt(), edge.getSubNodes().size());
        }
    }
    

    //*************************************************************************************************************
    /**
     * Controla zoom out dos vértices (somente)
     *
     */
//************************************************************************************************************* 
    public void zoomOutNode() {
        if (Flags.ZOOMFACTOR() > 1) {
            Flags.ZOOMFACTOR(Flags.ZOOMFACTOR() - 1);
        }
    }

    //*************************************************************************************************************
    /**
     * Controla zoom in dos vértices (somente)
     *
     */
//************************************************************************************************************* 
    public void zoomInNode() {
        if (Flags.ZOOMFACTOR() < 32) {
            Flags.ZOOMFACTOR(Flags.ZOOMFACTOR() + 1);
        }
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
