package edge.bundling.view.rendering;

import edge.bundling.controller.ControllerMainWindow;
import edge.bundling.graph.Node;
import edge.bundling.util.Flags;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

public class LinearDrawPanelGraphBundled extends LinearAbstractDrawPanel {

    private static final long serialVersionUID = 1L;

    private boolean force = false;

    public LinearDrawPanelGraphBundled() {
        setParameters();
    }

    public void registerController(ControllerMainWindow windowPanelController) {
        this.windowPanelController = windowPanelController;
    }

    //*************************************************************************************************************
    /**
     * Desenho o grafo com feixes
     *
     * @param screen - área de plotagem na tela
     */
//************************************************************************************************************* 
    public void paintComponent(Graphics screen) {

        Font font = new java.awt.Font("Arial", Font.BOLD, Flags.FONTSIZE());
        g2 = (Graphics2D) screen;
        //g2.scale(Flags.SCALEPANEL()/100, Flags.SCALEPANEL()/100);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setPaint(Color.WHITE);
        g2.setFont(font);

        if (!isForce()) {
            drawBundle(g2, this.windowPanelController.getGraphBundled());
        } else {
            drawBundleForce(g2, this.windowPanelController.getGraphBundled());
        }

        for (int i = 0; i < this.windowPanelController.getGraphBundled().getNodeList().size(); i++) {
            Node v;
            v = this.windowPanelController.getGraphBundled().getNodeList().get(i);
            if (v.isCover()) {
                drawNode(g2, v.getX(), v.getY(), v.getData(), 1);
            } else {
                drawNode(g2, v.getX(), v.getY(), v.getData(), 0);
            }
        }
    }

    public boolean isForce() {
        return force;
    }

    public void setForce(boolean force) {
        this.force = force;
    }
}
