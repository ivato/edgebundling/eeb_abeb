package edge.bundling.view.components;

import edge.bundling.controller.ControllerMainWindow;
import edge.bundling.util.Flags;
import java.awt.Dimension;
import javax.swing.JComboBox;
import javax.swing.JToolBar;

public class ComponentToolBar extends JToolBar {

    private static final long serialVersionUID = 1L;
    private ControllerMainWindow windowPanelController;

    private javax.swing.JButton jBOpenGraph;
    private javax.swing.JButton jBSaveImage;
    private javax.swing.JButton jBOpenSolution;
    private javax.swing.JButton jBGraphic;
    private javax.swing.JButton jBEvolution;
    private javax.swing.JButton jBZoomIn;
    private javax.swing.JButton jBZoomOut;
    private javax.swing.JButton jBZoomInNode;
    private javax.swing.JButton jBZoomOutNode;
    private javax.swing.JButton jBRunAlgorithm;
    private javax.swing.JToggleButton jTBSound;
    private javax.swing.JToggleButton jTBSaveBestSolutionFile;
    private javax.swing.JToggleButton jTBDataTableFiles;
    private javax.swing.JToggleButton jTBStatisticsFiles;
    private javax.swing.JCheckBox jCBLabels;
    private javax.swing.JCheckBox jCBCover;
    private javax.swing.JCheckBox jCBLog;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar.Separator jSeparator2;
    private javax.swing.JToolBar.Separator jSeparator3;
    private javax.swing.JToolBar.Separator jSeparator4;
    private javax.swing.JComboBox jCBAlgorithms;
    private javax.swing.JButton jBBestSolution;

    private javax.swing.JPanel jPColorNode;
    private javax.swing.JButton jBColorNode;
    private javax.swing.JPanel jPColorLabelNode;
    private javax.swing.JButton jBColorLabelNode;
    
    String[] algorithmsList = {"Evolutionary Edge Bundling"};

    public ComponentToolBar() {

        addComponents();

        setRollover(true);
        setBorderPainted(false);
        setPreferredSize(new java.awt.Dimension(366, 30));
    }

    public void registerController(ControllerMainWindow windowPanelController) {
        this.windowPanelController = windowPanelController;
    }

    public void addComponents() {

        jBOpenGraph = new javax.swing.JButton();
        jBSaveImage = new javax.swing.JButton();
        jBOpenSolution = new javax.swing.JButton();
        jBGraphic = new javax.swing.JButton();
        jBEvolution = new javax.swing.JButton();
        jBZoomIn = new javax.swing.JButton();
        jBZoomOut = new javax.swing.JButton();
        jBZoomInNode = new javax.swing.JButton();
        jBZoomOutNode = new javax.swing.JButton();
        jBRunAlgorithm = new javax.swing.JButton();
        jTBSound = new javax.swing.JToggleButton();
        jTBSaveBestSolutionFile = new javax.swing.JToggleButton();
        jTBDataTableFiles = new javax.swing.JToggleButton();
        jTBStatisticsFiles = new javax.swing.JToggleButton();
        jCBLabels = new javax.swing.JCheckBox();
        jCBCover = new javax.swing.JCheckBox();
        jCBLog = new javax.swing.JCheckBox();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        jSeparator2 = new javax.swing.JToolBar.Separator();
        jSeparator3 = new javax.swing.JToolBar.Separator();
        jSeparator4 = new javax.swing.JToolBar.Separator();
        jCBAlgorithms = new JComboBox(algorithmsList);
        jBBestSolution = new javax.swing.JButton("Show Best Solution");
        setjPColorNode(new javax.swing.JPanel());
        setjBColorNode(new javax.swing.JButton("Node"));
        setjPColorLabelNode(new javax.swing.JPanel());
        setjBColorLabelNode(new javax.swing.JButton("Label"));
      
        
        getjBOpenGraph().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/fldr_obj.gif")));
        getjBOpenGraph().setBorder(null);
        getjBOpenGraph().setBorderPainted(false);
        getjBOpenGraph().setFocusable(false);
        getjBOpenGraph().setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getjBOpenGraph().setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        getjBOpenGraph().setToolTipText("Open a new graph");
        add(getjBOpenGraph());

        getjBSaveImage().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/saveas_edit.gif")));
        getjBSaveImage().setBorder(null);
        getjBSaveImage().setBorderPainted(false);
        getjBSaveImage().setFocusable(false);
        getjBSaveImage().setEnabled(false);
        getjBSaveImage().setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getjBSaveImage().setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        getjBSaveImage().setToolTipText("Save the image presented in the screen");
        add(getjBSaveImage());

        getjBOpenSolution().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/copy_edit.gif")));
        getjBOpenSolution().setBorder(null);
        getjBOpenSolution().setBorderPainted(false);
        getjBOpenSolution().setEnabled(false);
        getjBOpenSolution().setFocusable(false);
        getjBOpenSolution().setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getjBOpenSolution().setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        getjBOpenSolution().setToolTipText("Open another solution of the current graph.");
        add(getjBOpenSolution());

        add(jSeparator1);

        getjBZoomIn().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/ampulhetaMais.png")));
        getjBZoomIn().setBorder(null);
        getjBZoomIn().setBorderPainted(false);
        getjBZoomIn().setEnabled(false);
        getjBZoomIn().setFocusable(false);
        getjBZoomIn().setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getjBZoomIn().setMaximumSize(new java.awt.Dimension(28, 20));
        getjBZoomIn().setMinimumSize(new java.awt.Dimension(28, 20));
        getjBZoomIn().setPreferredSize(new java.awt.Dimension(28, 20));
        getjBZoomIn().setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        add(getjBZoomIn());

        getjBZoomOut().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/ampulhetaMenos.png")));
        getjBZoomOut().setBorder(null);
        getjBZoomOut().setBorderPainted(false);
        getjBZoomOut().setEnabled(false);
        getjBZoomOut().setFocusable(false);
        getjBZoomOut().setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getjBZoomOut().setIgnoreRepaint(true);
        getjBZoomOut().setMaximumSize(new java.awt.Dimension(28, 20));
        getjBZoomOut().setMinimumSize(new java.awt.Dimension(28, 20));
        getjBZoomOut().setPreferredSize(new java.awt.Dimension(28, 20));
        getjBZoomOut().setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        add(getjBZoomOut());

        getjBZoomInNode().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/ampulhetaMaisPonto.png")));
        getjBZoomInNode().setBorder(null);
        getjBZoomInNode().setBorderPainted(false);
        getjBZoomInNode().setEnabled(false);
        getjBZoomInNode().setFocusable(false);
        getjBZoomInNode().setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getjBZoomInNode().setMaximumSize(new java.awt.Dimension(28, 20));
        getjBZoomInNode().setMinimumSize(new java.awt.Dimension(28, 20));
        getjBZoomInNode().setPreferredSize(new java.awt.Dimension(28, 20));
        getjBZoomInNode().setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        add(getjBZoomInNode());

        getjBZoomOutNode().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/ampulhetaMenosPonto.png")));
        getjBZoomOutNode().setBorder(null);
        getjBZoomOutNode().setBorderPainted(false);
        getjBZoomOutNode().setEnabled(false);
        getjBZoomOutNode().setFocusable(false);
        getjBZoomOutNode().setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getjBZoomOutNode().setMaximumSize(new java.awt.Dimension(28, 20));
        getjBZoomOutNode().setMinimumSize(new java.awt.Dimension(28, 20));
        getjBZoomOutNode().setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        add(getjBZoomOutNode());
        
        
        getjBColorNode().setPreferredSize(new Dimension(30, 30));
        getjBColorNode().setEnabled(false);
        add(getjBColorNode());
        
        getjPColorNode().setBackground(Flags.COLORNODE());
        getjPColorNode().setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        getjPColorNode().setMaximumSize(new java.awt.Dimension(18, 18));
        getjPColorNode().setEnabled(false);
        add(getjPColorNode());
        
        
        getjBColorLabelNode().setPreferredSize(new Dimension(30, 30));
        getjBColorLabelNode().setEnabled(false);
        add(getjBColorLabelNode());
        
        getjPColorLabelNode().setBackground(Flags.COLORLABELNODE());
        getjPColorLabelNode().setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        getjPColorLabelNode().setMaximumSize(new java.awt.Dimension(18, 18));
        getjPColorLabelNode().setEnabled(false);
        add(getjPColorLabelNode());
        
        getjCBLabels().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjCBLabels().setSelected(true);
        getjCBLabels().setText("Show Labels");
        getjCBLabels().setEnabled(false);
        getjCBLabels().setFocusable(false);
        getjCBLabels().setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        getjCBLabels().setPreferredSize(new java.awt.Dimension(100, 41));
        getjCBLabels().setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        add(getjCBLabels());

        getjCBCover().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjCBCover().setSelected(true);
        getjCBCover().setText("Show Cover Set");
        getjCBCover().setEnabled(false);
        getjCBCover().setFocusable(false);
        getjCBCover().setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        getjCBCover().setPreferredSize(new java.awt.Dimension(100, 41));
        getjCBCover().setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        add(getjCBCover());

        getjTBSound().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/sound_mute.png")));
        getjTBSound().setFocusable(false);
        getjTBSound().setEnabled(false);
        getjTBSound().setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getjTBSound().setMaximumSize(new java.awt.Dimension(35, 28));
        getjTBSound().setMinimumSize(new java.awt.Dimension(35, 28));
        getjTBSound().setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        getjTBSound().setToolTipText("Emit finished signal when execution completes");
        add(getjTBSound());

        add(jSeparator2);

        getjCBAlgorithms().setSelectedIndex(0);
        getjCBAlgorithms().setPreferredSize(new Dimension(250, 25));
        getjCBAlgorithms().setMaximumSize(getjCBAlgorithms().getPreferredSize());
        add(getjCBAlgorithms());

        getjBRunAlgorithm().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/play.png")));
        getjBRunAlgorithm().setBorderPainted(false);
        getjBRunAlgorithm().setEnabled(true);
        getjBRunAlgorithm().setFocusable(false);
        getjBRunAlgorithm().setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getjBRunAlgorithm().setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        getjBRunAlgorithm().setToolTipText("Execute selected algorithm");
        add(getjBRunAlgorithm());

        getjBGraphic().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/chart_bar.png")));
        getjBGraphic().setBorder(null);
        getjBGraphic().setBorderPainted(false);
        getjBGraphic().setEnabled(false);
        getjBGraphic().setFocusable(false);
        getjBGraphic().setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getjBGraphic().setMaximumSize(new java.awt.Dimension(28, 20));
        getjBGraphic().setMinimumSize(new java.awt.Dimension(28, 20));
        getjBGraphic().setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        getjBGraphic().setToolTipText("Graphic of solution generated");
        add(getjBGraphic());

        getjBEvolution().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/chart_line.png")));
        getjBEvolution().setBorder(null);
        getjBEvolution().setBorderPainted(false);
        getjBEvolution().setEnabled(false);
        getjBEvolution().setFocusable(false);
        getjBEvolution().setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getjBEvolution().setMaximumSize(new java.awt.Dimension(28, 20));
        getjBEvolution().setMinimumSize(new java.awt.Dimension(28, 20));
        getjBEvolution().setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        getjBEvolution().setToolTipText("Graphic of evolution of best solution");
        add(getjBEvolution());

        add(jSeparator3);

        getjTBSaveBestSolutionFile().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/table_save.png")));
        getjTBSaveBestSolutionFile().setToolTipText(" Save in files data about initial population and evolution process");
        getjTBSaveBestSolutionFile().setBorder(null);
        getjTBSaveBestSolutionFile().setFocusable(false);
        getjTBSaveBestSolutionFile().setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getjTBSaveBestSolutionFile().setIconTextGap(0);
        getjTBSaveBestSolutionFile().setMaximumSize(new java.awt.Dimension(35, 28));
        getjTBSaveBestSolutionFile().setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        add(getjTBSaveBestSolutionFile());

        getjTBDataTableFiles().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/database_gear.png")));
        getjTBDataTableFiles().setToolTipText("Save in file the compatibility data tables");
        getjTBDataTableFiles().setBorder(null);
        getjTBDataTableFiles().setFocusable(false);
        getjTBDataTableFiles().setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getjTBDataTableFiles().setIconTextGap(0);
        getjTBDataTableFiles().setMaximumSize(new java.awt.Dimension(35, 28));
        getjTBDataTableFiles().setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        add(getjTBDataTableFiles());

        getjTBStatisticsFiles().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/chart_pie_add.png")));
        getjTBStatisticsFiles().setToolTipText("Save in file statistics about each generation");
        getjTBStatisticsFiles().setBorder(null);
        getjTBStatisticsFiles().setFocusable(false);
        getjTBStatisticsFiles().setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getjTBStatisticsFiles().setIconTextGap(0);
        getjTBStatisticsFiles().setMaximumSize(new java.awt.Dimension(35, 28));
        getjTBStatisticsFiles().setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        add(getjTBStatisticsFiles());

        getjCBLog().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjCBLog().setText("Create Logs");
        getjCBLog().setToolTipText("Save in file image and data of the best solution, and information about the process ");
        getjCBLog().setFocusable(false);
        getjCBLog().setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        getjCBLog().setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        add(getjCBLog());
        
        add(jSeparator4);
        
         // Botão Best Solution
        getjBBestSolution().setPreferredSize(new Dimension(30, 30));
        getjBBestSolution().setEnabled(false);
        add(getjBBestSolution());

    }

    public javax.swing.JButton getjBOpenGraph() {

        return jBOpenGraph;
    }

    public javax.swing.JButton getjBSaveImage() {
        return jBSaveImage;
    }

    public javax.swing.JButton getjBOpenSolution() {
        return jBOpenSolution;
    }

    public javax.swing.JButton getjBGraphic() {
        return jBGraphic;
    }

    public javax.swing.JButton getjBEvolution() {
        return jBEvolution;
    }

    public javax.swing.JButton getjBZoomIn() {
        return jBZoomIn;
    }

    public javax.swing.JButton getjBZoomOut() {
        return jBZoomOut;
    }

    public javax.swing.JButton getjBZoomInNode() {
        return jBZoomInNode;
    }

    public javax.swing.JButton getjBZoomOutNode() {
        return jBZoomOutNode;
    }

    public javax.swing.JButton getjBRunAlgorithm() {
        return jBRunAlgorithm;
    }

    public javax.swing.JToggleButton getjTBSound() {
        return jTBSound;
    }

    public javax.swing.JToggleButton getjTBSaveBestSolutionFile() {
        return jTBSaveBestSolutionFile;
    }

    public javax.swing.JToggleButton getjTBDataTableFiles() {
        return jTBDataTableFiles;
    }

    public javax.swing.JToggleButton getjTBStatisticsFiles() {
        return jTBStatisticsFiles;
    }

    public javax.swing.JCheckBox getjCBLabels() {
        return jCBLabels;
    }

    public javax.swing.JCheckBox getjCBLog() {
        return jCBLog;
    }

    public javax.swing.JComboBox getjCBAlgorithms() {
        return jCBAlgorithms;
    }

    public javax.swing.JCheckBox getjCBCover() {
        return jCBCover;
    }
    
     public javax.swing.JButton getjBBestSolution() {
        return jBBestSolution;
    }

    public javax.swing.JPanel getjPColorNode() {
        return jPColorNode;
    }

    public void setjPColorNode(javax.swing.JPanel jPColorNode) {
        this.jPColorNode = jPColorNode;
    }

    public javax.swing.JButton getjBColorNode() {
        return jBColorNode;
    }

    public void setjBColorNode(javax.swing.JButton jBColorNode) {
        this.jBColorNode = jBColorNode;
    }

    /**
     * @return the jPColorLabelNode
     */
    public javax.swing.JPanel getjPColorLabelNode() {
        return jPColorLabelNode;
    }

    /**
     * @param jPColorLabelNode the jPColorLabelNode to set
     */
    public void setjPColorLabelNode(javax.swing.JPanel jPColorLabelNode) {
        this.jPColorLabelNode = jPColorLabelNode;
    }

    /**
     * @return the jBColorLabelNode
     */
    public javax.swing.JButton getjBColorLabelNode() {
        return jBColorLabelNode;
    }

    /**
     * @param jBColorLabelNode the jBColorLabelNode to set
     */
    public void setjBColorLabelNode(javax.swing.JButton jBColorLabelNode) {
        this.jBColorLabelNode = jBColorLabelNode;
    }
}
