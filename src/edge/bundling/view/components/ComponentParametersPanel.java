package edge.bundling.view.components;

import edge.bundling.controller.ControllerMainWindow;
import edge.bundling.methods.force.ForceParams;
import edge.bundling.util.Flags;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JPanel;

public class ComponentParametersPanel extends JPanel {

    private static final long serialVersionUID = 1L;
    private ControllerMainWindow windowPanelController;

    private GridBagLayout layout;
    private GridBagConstraints constraints;

    private javax.swing.JPanel jPMoving;
    private javax.swing.JPanel jPCompatibilityMeasures;
    private javax.swing.JPanel jPTerminationCondition;
    private javax.swing.JPanel jPMeasuresUsed;
    private javax.swing.JPanel jPEvolutionaryParameters;
    private javax.swing.JPanel jPSelectionPopulation;
    private javax.swing.JPanel jPRenderParameters;
    private javax.swing.JPanel jPPEDParameters;
    //private javax.swing.JButton jBBestSolution;
    private javax.swing.JRadioButton jRBDiversity;
    private javax.swing.JRadioButton jRBGeneration;
    private javax.swing.JRadioButton jRConstraint;
    private javax.swing.JRadioButton jRObjective;
    private javax.swing.JCheckBox jCBMinAngular;
    private javax.swing.JCheckBox jCBMinPosition;
    private javax.swing.JCheckBox jCBMinScale;
    private javax.swing.JSlider jSMinAngular;
    private javax.swing.JSlider jSMinPosition;
    private javax.swing.JSlider jSMinScale;
    private javax.swing.JLabel jLMinAngular;
    private javax.swing.JLabel jLMinPosition;
    private javax.swing.JLabel jLMinScale;
    private javax.swing.JCheckBox jCBMaxAngular;
    private javax.swing.JCheckBox jCBMaxPosition;
    private javax.swing.JCheckBox jCBMaxScale;
    private javax.swing.JSlider jSMaxAngular;
    private javax.swing.JSlider jSMaxPosition;
    private javax.swing.JSlider jSMaxScale;
    private javax.swing.JLabel jLMaxAngular;
    private javax.swing.JLabel jLMaxPosition;
    private javax.swing.JLabel jLMaxScale;
    private javax.swing.JRadioButton jRBRandom;
    private javax.swing.JRadioButton jRBRanking;
    private javax.swing.JRadioButton jRBRoulette;
    private javax.swing.JRadioButton jRBTournament;
    private javax.swing.JTextField jTSelectivePressure;
    private javax.swing.JLabel jLSelectivePressure;
    private javax.swing.ButtonGroup jBGTermination;
    private javax.swing.ButtonGroup jBGMeasure;
    private javax.swing.ButtonGroup jBGSelection;
    private javax.swing.JTextField jTCrossoverRatio;
    private javax.swing.JTextField jTMaxGeneration;
    private javax.swing.JTextField jTMutationRatio;
    private javax.swing.JTextField jTTamPopulation;
    private javax.swing.JLabel jLTamPopulation;
    private javax.swing.JLabel jLMaxGeneration;
    private javax.swing.JLabel jLCrossoverRatio;
    private javax.swing.JLabel jLMutationRatio;
    private javax.swing.JCheckBox jCBMutation1;
    private javax.swing.JCheckBox jCBMutation2;
    private javax.swing.JCheckBox jCBMutation3;
    private javax.swing.JCheckBox jCBMutation4;
    private javax.swing.JCheckBox jCBMutation5;
    private javax.swing.JTextField jTCycles;
    private javax.swing.JTextField jTIteractions;
    private javax.swing.JTextField jTStepSize;
    private javax.swing.JLabel jLCycles;
    private javax.swing.JLabel jLIteractions;
    private javax.swing.JLabel jLStepSize;
    private javax.swing.JLabel jLStiffness;
    private javax.swing.JButton jBColorSource;
    private javax.swing.JButton jBColorTarget;
    private javax.swing.JButton jBEdge;
    private javax.swing.JButton jBReDraw;
    private javax.swing.JPanel jPEdges;
    private javax.swing.JPanel jPSource;
    private javax.swing.JPanel jPTarget;
    private javax.swing.JSlider jSStifness;
    private javax.swing.JTextField jTStepConvexHull;
    private javax.swing.JLabel jLStepConvexHull;
    
    
    private javax.swing.JTextField jTPenalty;
    private javax.swing.JLabel jLPenalty;
    
    private javax.swing.JCheckBox jCBPED;
    private javax.swing.JCheckBox jCBPEDSOURCE;
    private javax.swing.JCheckBox jCBPEDTARGET;
    private javax.swing.JLabel jLPEDTRANSPARENT;
    private javax.swing.JTextField jTPEDTRANSPARENT;

    public ComponentParametersPanel() {
        layout = new GridBagLayout();
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        this.setLayout(layout);

        addComponents();
        this.setBounds(0, 0, 335, 687);
    }

    public void registerController(ControllerMainWindow windowPanelController) {
        this.windowPanelController = windowPanelController;
    }

    public void addComponents() {

        GridBagConstraints constraintsComponents = new GridBagConstraints();
        FlowLayout flowLayout = new FlowLayout(FlowLayout.LEFT);

        jPMoving = new JPanel();
        jPCompatibilityMeasures = new JPanel();
        jPTerminationCondition = new JPanel();
        jPMeasuresUsed = new JPanel();
        jPEvolutionaryParameters = new JPanel();
        jPSelectionPopulation = new JPanel();
        jPRenderParameters = new JPanel();
        jPPEDParameters = new JPanel();
        //jBBestSolution = new javax.swing.JButton("Show Best Solution");
        jRBGeneration = new javax.swing.JRadioButton();
        jRBDiversity = new javax.swing.JRadioButton();
        jRObjective = new javax.swing.JRadioButton();
        jRConstraint = new javax.swing.JRadioButton();
        jCBMinAngular = new javax.swing.JCheckBox();
        jCBMinPosition = new javax.swing.JCheckBox();
        jCBMinScale = new javax.swing.JCheckBox();
        jSMinAngular = new javax.swing.JSlider();
        jSMinScale = new javax.swing.JSlider();
        jSMinPosition = new javax.swing.JSlider();
        jLMinAngular = new javax.swing.JLabel();
        jLMinScale = new javax.swing.JLabel();
        jLMinPosition = new javax.swing.JLabel();
        jCBMaxAngular = new javax.swing.JCheckBox();
        jCBMaxPosition = new javax.swing.JCheckBox();
        jCBMaxScale = new javax.swing.JCheckBox();
        jSMaxAngular = new javax.swing.JSlider();
        jSMaxScale = new javax.swing.JSlider();
        jSMaxPosition = new javax.swing.JSlider();
        jLMaxAngular = new javax.swing.JLabel();
        jLMaxScale = new javax.swing.JLabel();
        jLMaxPosition = new javax.swing.JLabel();
        //jTAngle = new javax.swing.JTextField();       
        jRBRoulette = new javax.swing.JRadioButton();
        jRBRanking = new javax.swing.JRadioButton();
        jRBTournament = new javax.swing.JRadioButton();
        jRBRandom = new javax.swing.JRadioButton();
        jTSelectivePressure = new javax.swing.JTextField();
        jLSelectivePressure = new javax.swing.JLabel();
        jBGSelection = new javax.swing.ButtonGroup();
        jBGTermination = new javax.swing.ButtonGroup();
        jBGMeasure = new javax.swing.ButtonGroup();
        jTTamPopulation = new javax.swing.JTextField();
        jTMaxGeneration = new javax.swing.JTextField();
        jTCrossoverRatio = new javax.swing.JTextField();
        jTMutationRatio = new javax.swing.JTextField();
        jLTamPopulation = new javax.swing.JLabel();
        jLMaxGeneration = new javax.swing.JLabel();
        jLCrossoverRatio = new javax.swing.JLabel();
        jLMutationRatio = new javax.swing.JLabel();
        jCBMutation1 = new javax.swing.JCheckBox();
        jCBMutation2 = new javax.swing.JCheckBox();
        jCBMutation3 = new javax.swing.JCheckBox();
        jCBMutation5 = new javax.swing.JCheckBox();
        jCBMutation4 = new javax.swing.JCheckBox();
        jLCycles = new javax.swing.JLabel();
        jLIteractions = new javax.swing.JLabel();
        jLStepSize = new javax.swing.JLabel();
        jLStiffness = new javax.swing.JLabel();
        jTCycles = new javax.swing.JTextField();
        jTIteractions = new javax.swing.JTextField();
        jTStepSize = new javax.swing.JTextField();
        jBColorSource = new javax.swing.JButton();
        jBColorTarget = new javax.swing.JButton();
        jBEdge = new javax.swing.JButton();
        jBReDraw = new javax.swing.JButton();
        jPEdges = new javax.swing.JPanel();
        jPSource = new javax.swing.JPanel();
        jPTarget = new javax.swing.JPanel();
        jSStifness = new javax.swing.JSlider();
        jTStepConvexHull = new javax.swing.JTextField();
        jLStepConvexHull = new javax.swing.JLabel();
        jCBPED = new javax.swing.JCheckBox();
        jCBPEDSOURCE = new javax.swing.JCheckBox();
        jCBPEDTARGET = new javax.swing.JCheckBox();
        setjTPEDTRANSPARENT(new javax.swing.JTextField());
        setjLPEDTRANSPARENT(new javax.swing.JLabel());

        setjLPenalty(new javax.swing.JLabel());
        setjTPenalty(new javax.swing.JTextField());
        

        jPMoving.setPreferredSize(new java.awt.Dimension(320, 100));
        jPMoving.setBorder(javax.swing.BorderFactory.createTitledBorder("Limit Parameters for Hybrid Algorithm"));
        jPCompatibilityMeasures.setPreferredSize(new java.awt.Dimension(320, 100));
        jPCompatibilityMeasures.setBorder(javax.swing.BorderFactory.createTitledBorder("Compatibility Measures for Evolution"));
        jPTerminationCondition.setPreferredSize(new java.awt.Dimension(180, 80));
        jPTerminationCondition.setBorder(javax.swing.BorderFactory.createTitledBorder("Termination Condition"));
        jPMeasuresUsed.setPreferredSize(new java.awt.Dimension(120, 80));
        jPMeasuresUsed.setBorder(javax.swing.BorderFactory.createTitledBorder("Measures Used as"));
        jPEvolutionaryParameters.setPreferredSize(new java.awt.Dimension(180, 180));
        jPEvolutionaryParameters.setBorder(javax.swing.BorderFactory.createTitledBorder("Evolutionary Parameters"));
        jPSelectionPopulation.setPreferredSize(new java.awt.Dimension(120, 180));
        jPSelectionPopulation.setBorder(javax.swing.BorderFactory.createTitledBorder("Type of Selection"));
        jPRenderParameters.setPreferredSize(new java.awt.Dimension(320, 160));
        jPRenderParameters.setBorder(javax.swing.BorderFactory.createTitledBorder("Force Parameters"));
        jPPEDParameters.setPreferredSize(new java.awt.Dimension(320, 25));
       

        // Painel Compatibility Measures
        jPMoving.setLayout(layout);

        getjCBMinAngular().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjCBMinAngular().setSelected(true);
        getjCBMinAngular().setEnabled(false);
        getjCBMinAngular().setText("Min. Angle  ");

        getjCBMinPosition().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjCBMinPosition().setEnabled(false);
        getjCBMinPosition().setText("Min. Position");

        getjCBMinScale().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjCBMinScale().setEnabled(false);
        getjCBMinScale().setText("Min. Scale    ");

        getjSMinAngular().setToolTipText("");
        double value = 100;
        int value2 = (int) value;
        getjSMinAngular().setValue(value2);
        getjSMinAngular().setEnabled(false);
        getjSMinAngular().setPreferredSize(new Dimension(130, 25));

        getjSMinScale().setToolTipText("");
        value = 100;
        value2 = (int) value;
        getjSMinScale().setValue(value2);
        getjSMinScale().setEnabled(false);
        getjSMinScale().setPreferredSize(new Dimension(130, 25));

        getjSMinPosition().setToolTipText("");
        value = 100;
        value2 = (int) value;
        getjSMinPosition().setValue(value2);
        getjSMinPosition().setEnabled(false);
        getjSMinPosition().setPreferredSize(new Dimension(130, 25));

        getjLMinAngular().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjLMinAngular().setPreferredSize(new Dimension(30, 25));
        getjLMinAngular().setText(Double.toString(1));
        getjLMinAngular().setToolTipText("");
        getjLMinAngular().setEnabled(false);

        getjLMinScale().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjLMinScale().setPreferredSize(new Dimension(30, 25));
        getjLMinScale().setText(Double.toString(1));
        getjLMinScale().setToolTipText("");
        getjLMinScale().setEnabled(false);

        getjLMinPosition().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjLMinPosition().setPreferredSize(new Dimension(30, 25));
        getjLMinPosition().setText(Double.toString(1));
        getjLMinPosition().setToolTipText("");
        getjLMinPosition().setEnabled(false);

        jLStepConvexHull.setFont(new java.awt.Font("Lucida Grande", 0, 10));
        jLStepConvexHull.setText("Step");
        jLStepConvexHull.setToolTipText("");
        jLStepConvexHull.setPreferredSize(new Dimension(50, 25));
        jLStepConvexHull.setEnabled(false);
        
        getjTStepConvexHull().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjTStepConvexHull().setToolTipText("");
        getjTStepConvexHull().setPreferredSize(new Dimension(40, 25));
        getjTStepConvexHull().setText(Integer.toString(Flags.STEPCONVEXHUL()));
        getjTStepConvexHull().setEnabled(false);

        constraintsComponents.gridx = 0;
        constraintsComponents.gridy = 0;
        jPMoving.add(getjCBMinAngular(), constraintsComponents);
        constraintsComponents.gridx = 0;
        constraintsComponents.gridy = 1;
        jPMoving.add(getjCBMinScale(), constraintsComponents);
        constraintsComponents.gridx = 0;
        constraintsComponents.gridy = 2;
        jPMoving.add(getjCBMinPosition(), constraintsComponents);
        constraintsComponents.gridx = 1;
        constraintsComponents.gridy = 0;
        jPMoving.add(getjSMinAngular(), constraintsComponents);
        constraintsComponents.gridx = 1;
        constraintsComponents.gridy = 1;
        jPMoving.add(getjSMinScale(), constraintsComponents);
        constraintsComponents.gridx = 1;
        constraintsComponents.gridy = 2;
        jPMoving.add(getjSMinPosition(), constraintsComponents);
        constraintsComponents.gridx = 2;
        constraintsComponents.gridy = 0;
        jPMoving.add(getjLMinAngular(), constraintsComponents);
        constraintsComponents.gridx = 2;
        constraintsComponents.gridy = 1;
        jPMoving.add(getjLMinScale(), constraintsComponents);
        constraintsComponents.gridx = 2;
        constraintsComponents.gridy = 2;
        jPMoving.add(getjLMinPosition(), constraintsComponents);

        constraintsComponents.gridx = 3;
        constraintsComponents.gridy = 0;
        jPMoving.add(jLStepConvexHull, constraintsComponents);
        constraintsComponents.gridx = 3;
        constraintsComponents.gridy = 1;
        jPMoving.add(getjTStepConvexHull(), constraintsComponents);

        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 2;
        add(jPMoving, constraints);

        // Painel Compatibility Measures
        jPCompatibilityMeasures.setLayout(layout);

        getjCBMaxAngular().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjCBMaxAngular().setText("Max. Angle  ");
        getjCBMaxAngular().setSelected(Flags.isANGULAR());

        getjCBMaxPosition().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjCBMaxPosition().setText("Max. Position");
        getjCBMaxPosition().setSelected(Flags.isPOSITION());

        getjCBMaxScale().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjCBMaxScale().setText("Max. Scale    ");
        getjCBMaxScale().setSelected(Flags.isSCALE());

        getjSMaxAngular().setToolTipText("");
        value = Flags.COMPATIBILITY_MAX_ANGULAR_THRESHOLD() * 100;
        value2 = (int) value;
        getjSMaxAngular().setValue(value2);
        getjSMaxAngular().setEnabled(true);
        getjSMaxAngular().setPreferredSize(new Dimension(130, 25));

        getjSMaxScale().setToolTipText("");
        value = Flags.COMPATIBILITY_MAX_SCALE_THRESHOLD() * 100;
        value2 = (int) value;
        getjSMaxScale().setValue(value2);
        getjSMaxScale().setEnabled(Flags.isSCALE());
        getjSMaxScale().setPreferredSize(new Dimension(130, 25));

        getjSMaxPosition().setToolTipText("");
        value = Flags.COMPATIBILITY_MAX_POSITION_THRESHOLD() * 100;
        value2 = (int) value;
        getjSMaxPosition().setValue(value2);
        getjSMaxPosition().setEnabled(Flags.isPOSITION());
        getjSMaxPosition().setPreferredSize(new Dimension(130, 25));

        getjLMaxAngular().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjLMaxAngular().setText(Double.toString(Flags.COMPATIBILITY_MAX_ANGULAR_THRESHOLD()));
        getjLMaxAngular().setPreferredSize(new Dimension(30, 25));
        getjLMaxAngular().setToolTipText("");
        getjLMaxAngular().setEnabled(true);

        getjLMaxScale().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjLMaxScale().setText(Double.toString(Flags.COMPATIBILITY_MAX_SCALE_THRESHOLD()));
        getjLMaxScale().setToolTipText("");
        getjLMaxScale().setPreferredSize(new Dimension(30, 25));
        getjLMaxScale().setEnabled(Flags.isSCALE());

        getjLMaxPosition().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjLMaxPosition().setText(Double.toString(Flags.COMPATIBILITY_MAX_POSITION_THRESHOLD()));
        getjLMaxPosition().setToolTipText("");
        getjLMaxPosition().setPreferredSize(new Dimension(30, 25));
        getjLMaxPosition().setEnabled(Flags.isPOSITION());
        
        
        getjLPenalty().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjLPenalty().setText("Penalty");
        getjLPenalty().setPreferredSize(new Dimension(50, 25));
        
        getjTPenalty().setBackground(new java.awt.Color(255, 255, 204));
        getjTPenalty().setText(Integer.toString(Flags.PENALTY()));
        getjTPenalty().setPreferredSize(new Dimension(40, 25));
        getjTPenalty().setEnabled(true);


        constraintsComponents.gridx = 0;
        constraintsComponents.gridy = 0;
        //constraintsComponents.gridwidth = 1;
        jPCompatibilityMeasures.add(getjCBMaxAngular(), constraintsComponents);
        constraintsComponents.gridx = 0;
        constraintsComponents.gridy = 1;
        jPCompatibilityMeasures.add(getjCBMaxScale(), constraintsComponents);
        constraintsComponents.gridx = 0;
        constraintsComponents.gridy = 2;
        jPCompatibilityMeasures.add(getjCBMaxPosition(), constraintsComponents);
        constraintsComponents.gridx = 1;
        constraintsComponents.gridy = 0;
        jPCompatibilityMeasures.add(getjSMaxAngular(), constraintsComponents);
        constraintsComponents.gridx = 1;
        constraintsComponents.gridy = 1;
        jPCompatibilityMeasures.add(getjSMaxScale(), constraintsComponents);
        constraintsComponents.gridx = 1;
        constraintsComponents.gridy = 2;
        jPCompatibilityMeasures.add(getjSMaxPosition(), constraintsComponents);
        constraintsComponents.gridx = 2;
        constraintsComponents.gridy = 0;
        jPCompatibilityMeasures.add(getjLMaxAngular(), constraintsComponents);
        constraintsComponents.gridx = 2;
        constraintsComponents.gridy = 1;
        jPCompatibilityMeasures.add(getjLMaxScale(), constraintsComponents);
        constraintsComponents.gridx = 2;
        constraintsComponents.gridy = 2;
        jPCompatibilityMeasures.add(getjLMaxPosition(), constraintsComponents);

        constraintsComponents.gridx = 3;
        constraintsComponents.gridy = 0;
        jPCompatibilityMeasures.add(getjLPenalty(), constraintsComponents);
        
        constraintsComponents.gridx = 3;
        constraintsComponents.gridy = 1;
        jPCompatibilityMeasures.add(getjTPenalty(), constraintsComponents);
        
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 2;
        add(jPCompatibilityMeasures, constraints);

        // Painel Termination Condition
        jPTerminationCondition.setLayout(flowLayout);

        jBGTermination.add(getjRBGeneration());
        getjRBGeneration().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjRBGeneration().setSelected(Flags.isTERMINATION_BY_GENERATION());
        getjRBGeneration().setText("Generation");
        jPTerminationCondition.add(getjRBGeneration());

        jBGTermination.add(getjRBDiversity());
        getjRBDiversity().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjRBDiversity().setSelected(Flags.isTERMINATION_BY_DIVERSITY());
        getjRBDiversity().setText("Diversity");
        jPTerminationCondition.add(getjRBDiversity());

        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        add(jPTerminationCondition, constraints);

        // Painel Measures Used
        jPMeasuresUsed.setLayout(flowLayout);

        jBGMeasure.add(getjRObjective());
        getjRObjective().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjRObjective().setSelected(Flags.isOBJECTIVE());
        getjRObjective().setText("Objective");
        jPMeasuresUsed.add(getjRObjective());

        jBGMeasure.add(getjRConstraint());
        getjRConstraint().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjRConstraint().setSelected(Flags.isCONSTRAINT());
        getjRConstraint().setText("Constraint");
        jPMeasuresUsed.add(getjRConstraint());

        constraints.gridx = 1;
        add(jPMeasuresUsed, constraints);

        // Painel Evolutionary Parameters
        jPEvolutionaryParameters.setLayout(layout);

        jLTamPopulation.setFont(new java.awt.Font("Lucida Grande", 0, 10));
        jLTamPopulation.setText("Population");
        jLTamPopulation.setPreferredSize(new Dimension(70, 25));

        jLMaxGeneration.setFont(new java.awt.Font("Lucida Grande", 0, 10));
        jLMaxGeneration.setText("Generations");
        jLMaxGeneration.setPreferredSize(new Dimension(70, 25));

        jLCrossoverRatio.setFont(new java.awt.Font("Lucida Grande", 0, 10));
        jLCrossoverRatio.setText("Crossover");
        jLCrossoverRatio.setPreferredSize(new Dimension(70, 25));

        jLMutationRatio.setFont(new java.awt.Font("Lucida Grande", 0, 10));
        jLMutationRatio.setText("Mutation");
        jLMutationRatio.setPreferredSize(new Dimension(70, 25));

        getjTTamPopulation().setText(Integer.toString(Flags.TAM_POPULATION()));
        getjTTamPopulation().setPreferredSize(new Dimension(80, 25));

        getjTMaxGeneration().setText(Integer.toString(Flags.MAX_GENERATION()));
        getjTMaxGeneration().setPreferredSize(new Dimension(80, 25));

        getjTCrossoverRatio().setText(Float.toString(Flags.CROSSOVER_RATIO()));
        getjTCrossoverRatio().setPreferredSize(new Dimension(80, 25));

        getjTMutationRatio().setText(Float.toString(Flags.MUTATION_RATIO()));
        getjTMutationRatio().setPreferredSize(new Dimension(80, 25));

        getjCBMutation1().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjCBMutation1().setSelected(Flags.isMUTATION_JOIN());
        getjCBMutation1().setText("M1");
        getjCBMutation1().setPreferredSize(new Dimension(47, 25));
        getjCBMutation1().setToolTipText("Monta um novo feixe com duas arestas adjacentes que estavam sozinhas");

        getjCBMutation2().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjCBMutation2().setSelected(Flags.isMUTATION_SPLIT());
        getjCBMutation2().setText("M2");
        getjCBMutation2().setPreferredSize(new Dimension(47, 25));
        getjCBMutation2().setToolTipText("Divide um feixe de tamanho maior que um em dois novos feixes");

        getjCBMutation3().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjCBMutation3().setSelected(Flags.isMUTATION_MOVE());
        getjCBMutation3().setText("M3");
        getjCBMutation3().setPreferredSize(new Dimension(47, 25));
        getjCBMutation3().setToolTipText("Retira uma aresta de um feixe e a coloca em outro feixe ao qual ela é adjacente");

        getjCBMutation4().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjCBMutation4().setSelected(Flags.isMUTATION_MERGE());
        getjCBMutation4().setText("M4");
        getjCBMutation4().setPreferredSize(new Dimension(47, 25));
        getjCBMutation4().setToolTipText("Une dois feixes que possuem o vértice central comum");

        getjCBMutation5().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjCBMutation5().setSelected(Flags.isMUTATION_REMOVE());
        getjCBMutation5().setText("M5");
        getjCBMutation5().setToolTipText("Retira uma aresta de um feixe de tamanho maior que um e monta um novo feixe ");

        constraintsComponents.gridx = 0;
        constraintsComponents.gridy = 0;
        jPEvolutionaryParameters.add(jLTamPopulation, constraintsComponents);
        constraintsComponents.gridx = 0;
        constraintsComponents.gridy = 1;
        jPEvolutionaryParameters.add(jLMaxGeneration, constraintsComponents);
        constraintsComponents.gridx = 0;
        constraintsComponents.gridy = 2;
        jPEvolutionaryParameters.add(jLCrossoverRatio, constraintsComponents);
        constraintsComponents.gridx = 0;
        constraintsComponents.gridy = 3;
        jPEvolutionaryParameters.add(jLMutationRatio, constraintsComponents);
        constraintsComponents.gridx = 1;
        constraintsComponents.gridy = 0;
        constraintsComponents.gridwidth = 2;
        jPEvolutionaryParameters.add(getjTTamPopulation(), constraintsComponents);
        constraintsComponents.gridx = 1;
        constraintsComponents.gridy = 1;
        constraintsComponents.gridwidth = 2;
        jPEvolutionaryParameters.add(getjTMaxGeneration(), constraintsComponents);
        constraintsComponents.gridx = 1;
        constraintsComponents.gridy = 2;
        constraintsComponents.gridwidth = 2;
        jPEvolutionaryParameters.add(getjTCrossoverRatio(), constraintsComponents);
        constraintsComponents.gridx = 1;
        constraintsComponents.gridy = 3;
        constraintsComponents.gridwidth = 2;
        jPEvolutionaryParameters.add(getjTMutationRatio(), constraintsComponents);
        constraintsComponents.gridx = 0;
        constraintsComponents.gridy = 4;
        constraintsComponents.gridwidth = 1;
        jPEvolutionaryParameters.add(getjCBMutation1(), constraintsComponents);
        constraintsComponents.gridx = 1;
        constraintsComponents.gridy = 4;
        jPEvolutionaryParameters.add(getjCBMutation2(), constraintsComponents);
        constraintsComponents.gridx = 2;
        constraintsComponents.gridy = 4;
        jPEvolutionaryParameters.add(getjCBMutation3(), constraintsComponents);
        constraintsComponents.gridx = 0;
        constraintsComponents.gridy = 5;
        jPEvolutionaryParameters.add(getjCBMutation4(), constraintsComponents);
        constraintsComponents.gridx = 1;
        constraintsComponents.gridy = 5;
        jPEvolutionaryParameters.add(getjCBMutation5(), constraintsComponents);

        constraints.gridx = 0;
        constraints.gridy = 3;
        add(jPEvolutionaryParameters, constraints);

        // Painel Selection Population		
        jPSelectionPopulation.setLayout(flowLayout);

        jBGSelection.add(getjRBRoulette());
        getjRBRoulette().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjRBRoulette().setText("RouletteWheel");
        getjRBRoulette().setPreferredSize(new Dimension(190, 20));
        getjRBRoulette().setSelected(Flags.isROULETTE_WHEEL());
        getjRBRoulette().setRequestFocusEnabled(false);
        jPSelectionPopulation.add(getjRBRoulette());

        jBGSelection.add(getjRBRanking());
        getjRBRanking().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjRBRanking().setText("Linear Ranking");
        getjRBRanking().setSelected(Flags.isRANKING());
        getjRBRanking().setPreferredSize(new Dimension(190, 20));
        jPSelectionPopulation.add(getjRBRanking());

        jBGSelection.add(getjRBTournament());
        getjRBTournament().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjRBTournament().setText("Tournament");
        getjRBTournament().setSelected(Flags.isTOURNAMENT());
        getjRBTournament().setPreferredSize(new Dimension(190, 20));
        jPSelectionPopulation.add(getjRBTournament());

        jBGSelection.add(getjRBRandom());
        getjRBRandom().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjRBRandom().setText("Random");
        getjRBRandom().setSelected(Flags.isRANDOM());
        getjRBRandom().setPreferredSize(new Dimension(190, 20));
        jPSelectionPopulation.add(getjRBRandom());

        jLSelectivePressure.setFont(new java.awt.Font("Lucida Grande", 0, 10));
        jLSelectivePressure.setText("Selective Pressure");
        jLSelectivePressure.setPreferredSize(new Dimension(190, 20));
        jPSelectionPopulation.add(jLSelectivePressure);

        getjTSelectivePressure().setBackground(new java.awt.Color(255, 255, 204));
        getjTSelectivePressure().setText(Integer.toString(Flags.SELECTIVE_PRESSURE()));
        getjTSelectivePressure().setPreferredSize(new Dimension(100, 25));
        getjTSelectivePressure().setEnabled(false);
        jPSelectionPopulation.add(getjTSelectivePressure());

        constraints.gridx = 1;
        add(jPSelectionPopulation, constraints);

        // Painel Render Parameters
        jPRenderParameters.setLayout(layout);

        jLCycles.setFont(new java.awt.Font("Lucida Grande", 0, 10));
        jLCycles.setText("Cycles");
        jLCycles.setPreferredSize(new Dimension(55, 25));

        jLIteractions.setFont(new java.awt.Font("Lucida Grande", 0, 10));
        jLIteractions.setText("Iterations");
        jLIteractions.setPreferredSize(new Dimension(55, 25));

        jLStepSize.setFont(new java.awt.Font("Lucida Grande", 0, 10));
        jLStepSize.setText("Step Size");
        jLStepSize.setPreferredSize(new Dimension(55, 25));

        jLStiffness.setFont(new java.awt.Font("Lucida Grande", 0, 10));
        jLStiffness.setText("Stiffness");
        jLStiffness.setPreferredSize(new Dimension(55, 25));

        getjTCycles().setText(Integer.toString(ForceParams.CYCLES()));
        getjTCycles().setPreferredSize(new Dimension(50, 25));

        getjTIteractions().setText(Integer.toString(ForceParams.INTERACTIONS()));
        getjTIteractions().setPreferredSize(new Dimension(50, 25));

        getjTStepSize().setText(Double.toString(ForceParams.STEPSIZE()));
        getjTStepSize().setPreferredSize(new Dimension(50, 25));

        getjPSource().setBackground(Flags.COLORSOURCE());
        getjPSource().setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        getjPSource().setPreferredSize(new java.awt.Dimension(15, 15));

        getjPTarget().setBackground(Flags.COLORTARGET());
        getjPTarget().setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        getjPTarget().setPreferredSize(new java.awt.Dimension(15, 15));

        getjPEdges().setBackground(Flags.COLOREDGE());
        getjPEdges().setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        getjPEdges().setPreferredSize(new java.awt.Dimension(15, 15));

        getjBColorSource().setText("Source Color");
        getjBColorSource().setPreferredSize(new java.awt.Dimension(110, 25));
        getjBColorTarget().setText("Target Color");
        getjBColorTarget().setPreferredSize(new java.awt.Dimension(110, 25));
        getjBEdge().setText("Edges Color");
        getjBEdge().setPreferredSize(new java.awt.Dimension(110, 25));

        getjSStifness().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjSStifness().setMajorTickSpacing(1);
        getjSStifness().setMaximum(10);
        getjSStifness().setMinimum(1);
        getjSStifness().setPaintLabels(true);
        getjSStifness().setPaintTicks(true);
        getjSStifness().setValue(ForceParams.STIFFINESS());
        getjSStifness().setPreferredSize(new Dimension(150, 45));

        getjBReDraw().setText("ReDraw");
        getjBReDraw().setEnabled(false);
        
        getjTPEDTRANSPARENT().setText(Integer.toString(Flags.getPEDTRANSPARENT()));
        getjTPEDTRANSPARENT().setPreferredSize(new Dimension(40, 25));
        

        constraintsComponents.gridx = 0;
        constraintsComponents.gridy = 0;
        jPRenderParameters.add(jLCycles, constraintsComponents);
        constraintsComponents.gridx = 0;
        constraintsComponents.gridy = 1;
        jPRenderParameters.add(jLIteractions, constraintsComponents);
        constraintsComponents.gridx = 0;
        constraintsComponents.gridy = 2;
        jPRenderParameters.add(jLStepSize, constraintsComponents);
        constraintsComponents.gridx = 0;
        constraintsComponents.gridy = 3;
        jPRenderParameters.add(jLStiffness, constraintsComponents);
        constraintsComponents.gridx = 1;
        constraintsComponents.gridy = 0;
        jPRenderParameters.add(getjTCycles(), constraintsComponents);
        constraintsComponents.gridx = 1;
        constraintsComponents.gridy = 1;
        jPRenderParameters.add(getjTIteractions(), constraintsComponents);
        constraintsComponents.gridx = 1;
        constraintsComponents.gridy = 2;
        jPRenderParameters.add(getjTStepSize(), constraintsComponents);
        constraintsComponents.gridx = 2;
        constraintsComponents.gridy = 0;
        jPRenderParameters.add(getjBColorSource(), constraintsComponents);
        constraintsComponents.gridx = 2;
        constraintsComponents.gridy = 1;
        jPRenderParameters.add(getjBColorTarget(), constraintsComponents);
        constraintsComponents.gridx = 2;
        constraintsComponents.gridy = 2;
        jPRenderParameters.add(getjBEdge(), constraintsComponents);
        constraintsComponents.gridx = 3;
        constraintsComponents.gridy = 0;
        jPRenderParameters.add(getjPSource(), constraintsComponents);
        constraintsComponents.gridx = 3;
        constraintsComponents.gridy = 1;
        jPRenderParameters.add(getjPTarget(), constraintsComponents);
        constraintsComponents.gridx = 3;
        constraintsComponents.gridy = 2;
        jPRenderParameters.add(getjPEdges(), constraintsComponents);
        constraintsComponents.gridx = 1;
        constraintsComponents.gridy = 3;
        constraintsComponents.gridwidth = 2;
        jPRenderParameters.add(getjSStifness(), constraintsComponents);
        constraintsComponents.gridx = 3;
        constraintsComponents.gridy = 3;
        constraintsComponents.gridwidth = 1;
        jPRenderParameters.add(getjBReDraw(), constraintsComponents);

        constraints.gridx = 0;
        constraints.gridy = 4;
        constraints.gridwidth = 2;
        add(jPRenderParameters, constraints);

        
        // Painel PED Parameters
        jPPEDParameters.setLayout(layout);
        
        getjCBPED().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjCBPED().setText("Partial Edge");
        getjCBPED().setSelected(false);
        getjCBPED().setEnabled(false);
        constraintsComponents.gridx = 0;
        constraintsComponents.gridy = 0;
        constraintsComponents.gridwidth = 1;
        jPPEDParameters.add(getjCBPED(), constraintsComponents);
        jCBPEDSOURCE.setFont(new java.awt.Font("Lucida Grande", 0, 10));
        jCBPEDSOURCE.setText("Source");
        jCBPEDSOURCE.setSelected(false);
        jCBPEDSOURCE.setEnabled(false);
        constraintsComponents.gridx = 1;
        constraintsComponents.gridy = 0;
        jPPEDParameters.add(jCBPEDSOURCE, constraintsComponents);
        jCBPEDTARGET.setFont(new java.awt.Font("Lucida Grande", 0, 10));
        jCBPEDTARGET.setText("Target");
        jCBPEDTARGET.setSelected(false);
        jCBPEDTARGET.setEnabled(false);
        constraintsComponents.gridx = 2;
        constraintsComponents.gridy = 0;
        jPPEDParameters.add(jCBPEDTARGET, constraintsComponents);
        getjLPEDTRANSPARENT().setFont(new java.awt.Font("Lucida Grande", 0, 10));
        getjLPEDTRANSPARENT().setText("Transp.");
        getjLPEDTRANSPARENT().setEnabled(false);
        constraintsComponents.gridx = 3;
        constraintsComponents.gridy = 0;
        jPPEDParameters.add(getjLPEDTRANSPARENT(), constraintsComponents);
        getjTPEDTRANSPARENT().setEnabled(false);
        constraintsComponents.gridx = 4;
        constraintsComponents.gridy = 0;
        jPPEDParameters.add(getjTPEDTRANSPARENT(), constraintsComponents);
              
        constraints.gridx = 0;
        constraints.gridy = 5;
        constraints.gridwidth = 2;
        add(jPPEDParameters, constraints);
        
        // Botão Best Solution
        //getjBBestSolution().setPreferredSize(new Dimension(300, 30));
       // getjBBestSolution().setEnabled(false);

        //constraints.gridx = 0;
        //constraints.gridy = 5;
        //add(getjBBestSolution(), constraints);
    }

    public void enableMovingPanel() {
        double value = Flags.COMPATIBILITY_MIN_ANGULAR_THRESHOLD() * 100;
        int value2 = (int) value;
        getjSMinAngular().setValue(value2);
        value = Flags.COMPATIBILITY_MIN_SCALE_THRESHOLD() * 100;
        value2 = (int) value;
        getjSMinScale().setValue(value2);
        value = Flags.COMPATIBILITY_MIN_POSITION_THRESHOLD() * 100;
        value2 = (int) value;
        getjSMinPosition().setValue(value2);

        getjLMinAngular().setText(Double.toString(Flags.COMPATIBILITY_MIN_ANGULAR_THRESHOLD()));
        getjLMinScale().setText(Double.toString(Flags.COMPATIBILITY_MIN_SCALE_THRESHOLD()));
        getjLMinPosition().setText(Double.toString(Flags.COMPATIBILITY_MIN_POSITION_THRESHOLD()));
        getjCBMinAngular().setSelected(true);
        getjCBMinPosition().setSelected(false);
        getjCBMinScale().setSelected(true);
        getjCBMinAngular().setEnabled(true);
        getjCBMinPosition().setEnabled(true);
        getjCBMinScale().setEnabled(true);
        getjSMinAngular().setEnabled(true);
        getjSMinScale().setEnabled(true);
        getjSMinPosition().setEnabled(false);
        getjLMinAngular().setEnabled(true);
        getjLMinScale().setEnabled(true);
        getjLMinPosition().setEnabled(false);
        getjTStepConvexHull().setEnabled(true);
        jLStepConvexHull.setEnabled(true);

        value = Flags.COMPATIBILITY_MAX_ANGULAR_THRESHOLD() * 100;
        value2 = (int) value;
        getjSMaxAngular().setValue(value2);
        value = Flags.COMPATIBILITY_MAX_SCALE_THRESHOLD() * 100;
        value2 = (int) value;
        getjSMaxScale().setValue(value2);
        value = Flags.COMPATIBILITY_MAX_POSITION_THRESHOLD() * 100;
        value2 = (int) value;

        getjLMaxAngular().setText(Double.toString(Flags.COMPATIBILITY_MAX_ANGULAR_THRESHOLD()));
        getjLMaxScale().setText(Double.toString(Flags.COMPATIBILITY_MAX_SCALE_THRESHOLD()));
        getjLMaxPosition().setText(Double.toString(Flags.COMPATIBILITY_MAX_POSITION_THRESHOLD()));
        getjSMaxPosition().setValue(value2);
        getjCBMaxScale().setSelected(true);
        getjCBMaxScale().setEnabled(true);
        getjSMaxScale().setEnabled(true);
        getjLMaxScale().setEnabled(true);
    }

    public void disableMovingPanel() {
        double value = Flags.COMPATIBILITY_MIN_ANGULAR_THRESHOLD() * 100;
        int value2 = (int) value;
        getjSMinAngular().setValue(value2);
        value = Flags.COMPATIBILITY_MIN_SCALE_THRESHOLD() * 100;
        value2 = (int) value;
        getjSMinScale().setValue(value2);
        value = Flags.COMPATIBILITY_MIN_POSITION_THRESHOLD() * 100;
        value2 = (int) value;
        getjSMinPosition().setValue(value2);

        getjTStepConvexHull().setEnabled(false);
        jLStepConvexHull.setEnabled(false);

        getjLMinAngular().setText(Double.toString(Flags.COMPATIBILITY_MIN_ANGULAR_THRESHOLD()));
        getjLMinScale().setText(Double.toString(Flags.COMPATIBILITY_MIN_SCALE_THRESHOLD()));
        getjLMinPosition().setText(Double.toString(Flags.COMPATIBILITY_MIN_POSITION_THRESHOLD()));
        getjCBMinAngular().setSelected(true);
        getjCBMinPosition().setSelected(false);
        getjCBMinScale().setSelected(false);
        getjCBMinAngular().setEnabled(false);
        getjCBMinPosition().setEnabled(false);
        getjCBMinScale().setEnabled(false);
        getjSMinAngular().setEnabled(false);
        getjSMinScale().setEnabled(false);
        getjSMinPosition().setEnabled(false);
        getjLMinAngular().setEnabled(false);
        getjLMinScale().setEnabled(false);
        getjLMinPosition().setEnabled(false);
    }

    public void enableEvolutionaryPanel() {
        double value = Flags.COMPATIBILITY_MAX_ANGULAR_THRESHOLD() * 100;
        int value2 = (int) value;
        getjSMaxAngular().setValue(value2);

        value = Flags.COMPATIBILITY_MAX_SCALE_THRESHOLD() * 100;
        value2 = (int) value;
        getjSMaxScale().setValue(value2);

        value = Flags.COMPATIBILITY_MAX_POSITION_THRESHOLD() * 100;
        value2 = (int) value;
        getjSMaxPosition().setValue(value2);

        getjLMaxAngular().setText(Double.toString(Flags.COMPATIBILITY_MAX_ANGULAR_THRESHOLD()));
        getjLMaxScale().setText(Double.toString(Flags.COMPATIBILITY_MAX_SCALE_THRESHOLD()));
        getjLMaxPosition().setText(Double.toString(Flags.COMPATIBILITY_MAX_POSITION_THRESHOLD()));
        getjCBMaxAngular().setSelected(true);
        getjCBMaxScale().setSelected(false);
        getjCBMaxPosition().setSelected(false);
        getjLMaxAngular().setEnabled(true);
        getjLMaxScale().setEnabled(false);
        getjLMaxPosition().setEnabled(false);
        getjCBMaxAngular().setEnabled(true);
        getjCBMaxScale().setEnabled(true);
        getjCBMaxPosition().setEnabled(true);
        getjSMaxAngular().setEnabled(true);
        getjSMaxScale().setEnabled(false);
        getjSMaxPosition().setEnabled(false);
        getjRBGeneration().setEnabled(true);
        getjRBDiversity().setEnabled(true);
        getjRObjective().setEnabled(true);
        getjRConstraint().setEnabled(true);
        getjTTamPopulation().setEnabled(true);
        getjTMaxGeneration().setEnabled(true);
        getjTCrossoverRatio().setEnabled(true);
        getjTMutationRatio().setEnabled(true);
        getjCBMutation1().setEnabled(true);
        getjCBMutation2().setEnabled(true);
        getjCBMutation3().setEnabled(true);
        getjCBMutation4().setEnabled(true);
        getjCBMutation5().setEnabled(true);
        getjRBRoulette().setEnabled(true);
        getjRBRanking().setEnabled(true);
        getjRBTournament().setEnabled(true);
        getjRBRandom().setEnabled(true);
        
        getjTPenalty().setEnabled(true);
        getjLPenalty().setEnabled(true);
    }

    public void disableEvolutionaryPanel() {
        double value = Flags.COMPATIBILITY_MAX_ANGULAR_THRESHOLD() * 100;
        int value2 = (int) value;
        getjSMaxAngular().setValue(value2);

        value = Flags.COMPATIBILITY_MAX_SCALE_THRESHOLD() * 100;
        value2 = (int) value;
        getjSMaxScale().setValue(value2);

        value = Flags.COMPATIBILITY_MAX_POSITION_THRESHOLD() * 100;
        value2 = (int) value;
        getjSMaxPosition().setValue(value2);

        getjLMaxAngular().setText(Double.toString(Flags.COMPATIBILITY_MAX_ANGULAR_THRESHOLD()));
        getjLMaxScale().setText(Double.toString(Flags.COMPATIBILITY_MAX_SCALE_THRESHOLD()));
        getjLMaxPosition().setText(Double.toString(Flags.COMPATIBILITY_MAX_POSITION_THRESHOLD()));
        getjCBMaxAngular().setSelected(true);
        getjCBMaxScale().setSelected(false);
        getjCBMaxPosition().setSelected(false);
        getjLMaxAngular().setEnabled(false);
        getjLMaxScale().setEnabled(false);
        getjLMaxPosition().setEnabled(false);
        getjCBMaxAngular().setEnabled(false);
        getjCBMaxScale().setEnabled(false);
        getjCBMaxPosition().setEnabled(false);
        getjSMaxAngular().setEnabled(false);
        getjSMaxScale().setEnabled(false);
        getjSMaxPosition().setEnabled(false);
        getjRBGeneration().setEnabled(false);
        getjRBDiversity().setEnabled(false);
        getjRObjective().setEnabled(false);
        getjRConstraint().setEnabled(false);
        getjTTamPopulation().setEnabled(false);
        getjTMaxGeneration().setEnabled(false);
        getjTCrossoverRatio().setEnabled(false);
        getjTMutationRatio().setEnabled(false);
        getjCBMutation1().setEnabled(false);
        getjCBMutation2().setEnabled(false);
        getjCBMutation3().setEnabled(false);
        getjCBMutation4().setEnabled(false);
        getjCBMutation5().setEnabled(false);
        getjRBRoulette().setEnabled(false);
        getjRBRanking().setEnabled(false);
        getjRBTournament().setEnabled(false);
        getjRBRandom().setEnabled(false);
        
        getjTPenalty().setEnabled(false);
        getjLPenalty().setEnabled(false);
    }

    public void disableForcePanel() {
        getjPSource().setBackground(new java.awt.Color(240, 240, 240));
        getjPTarget().setBackground(new java.awt.Color(240, 240, 240));
        getjPEdges().setBackground(new java.awt.Color(240, 240, 240));
        getjPSource().setEnabled(false);
        getjPTarget().setEnabled(false);
        getjPEdges().setEnabled(false);
        getjBReDraw().setEnabled(false);
        getjCBPED().setEnabled(false);
        jCBPEDSOURCE.setEnabled(false);
        jCBPEDTARGET.setEnabled(false);
        jLPEDTRANSPARENT.setEnabled(false);
        jTPEDTRANSPARENT.setEnabled(false);
        //getjBBestSolution().setEnabled(false);
        getjBColorSource().setEnabled(false);
        getjBColorTarget().setEnabled(false);
        getjBEdge().setEnabled(false);
        jLCycles.setEnabled(false);
        jLIteractions.setEnabled(false);
        jLStepSize.setEnabled(false);
        jLStiffness.setEnabled(false);
        jTCycles.setEnabled(false);
        jTIteractions.setEnabled(false);
        jTStepSize.setEnabled(false);
        jPEdges.setEnabled(false);
        jPSource.setEnabled(false);
        jPTarget.setEnabled(false);
        jSStifness.setEnabled(false);
    }

    public void enableForcePanel() {
        getjPSource().setBackground(new java.awt.Color(255, 0, 0));
        getjPTarget().setBackground(new java.awt.Color(240, 255, 255));
        getjPEdges().setBackground(new java.awt.Color(240, 240, 240));
        getjPSource().setEnabled(true);
        getjPTarget().setEnabled(true);
        getjPEdges().setEnabled(true);
        getjBReDraw().setEnabled(true);
        getjCBPED().setEnabled(true);
        jCBPEDSOURCE.setEnabled(true);
        jCBPEDTARGET.setEnabled(true);
        jLPEDTRANSPARENT.setEnabled(true);
        jTPEDTRANSPARENT.setEnabled(true);
        //getjBBestSolution().setEnabled(true);
        getjBColorSource().setEnabled(true);
        getjBColorTarget().setEnabled(true);
        getjBEdge().setEnabled(true);
        jLCycles.setEnabled(true);
        jLIteractions.setEnabled(true);
        jLStepSize.setEnabled(true);
        jLStiffness.setEnabled(true);
        jTCycles.setEnabled(true);
        jTIteractions.setEnabled(true);
        jTStepSize.setEnabled(true);
        jPEdges.setEnabled(true);
        jPSource.setEnabled(true);
        jPTarget.setEnabled(true);
        jSStifness.setEnabled(true);
    }

    //public javax.swing.JButton getjBBestSolution() {
    //    return jBBestSolution;
    //}

    public javax.swing.JRadioButton getjRBDiversity() {
        return jRBDiversity;
    }

    public javax.swing.JRadioButton getjRBGeneration() {
        return jRBGeneration;
    }

    public javax.swing.JRadioButton getjRConstraint() {
        return jRConstraint;
    }

    public javax.swing.JRadioButton getjRObjective() {
        return jRObjective;
    }

    public javax.swing.JCheckBox getjCBMaxAngular() {
        return jCBMaxAngular;
    }

    public javax.swing.JCheckBox getjCBMaxPosition() {
        return jCBMaxPosition;
    }

    public javax.swing.JCheckBox getjCBMaxScale() {
        return jCBMaxScale;
    }

    public javax.swing.JSlider getjSMaxAngular() {
        return jSMaxAngular;
    }

    public javax.swing.JSlider getjSMaxPosition() {
        return jSMaxPosition;
    }

    public javax.swing.JSlider getjSMaxScale() {
        return jSMaxScale;
    }

    public javax.swing.JRadioButton getjRBRandom() {
        return jRBRandom;
    }

    public javax.swing.JRadioButton getjRBRanking() {
        return jRBRanking;
    }

    public javax.swing.JRadioButton getjRBRoulette() {
        return jRBRoulette;
    }

    public javax.swing.JRadioButton getjRBTournament() {
        return jRBTournament;
    }

    public javax.swing.JTextField getjTSelectivePressure() {
        return jTSelectivePressure;
    }

    public javax.swing.JTextField getjTCrossoverRatio() {
        return jTCrossoverRatio;
    }

    public javax.swing.JTextField getjTMaxGeneration() {
        return jTMaxGeneration;
    }

    public javax.swing.JTextField getjTMutationRatio() {
        return jTMutationRatio;
    }

    public javax.swing.JTextField getjTTamPopulation() {
        return jTTamPopulation;
    }

    public javax.swing.JCheckBox getjCBMutation1() {
        return jCBMutation1;
    }

    public javax.swing.JCheckBox getjCBMutation2() {
        return jCBMutation2;
    }

    public javax.swing.JCheckBox getjCBMutation3() {
        return jCBMutation3;
    }

    public javax.swing.JCheckBox getjCBMutation4() {
        return jCBMutation4;
    }

    public javax.swing.JCheckBox getjCBMutation5() {
        return jCBMutation5;
    }

    public javax.swing.JTextField getjTCycles() {
        return jTCycles;
    }

    public javax.swing.JTextField getjTIteractions() {
        return jTIteractions;
    }

    public javax.swing.JTextField getjTStepSize() {
        return jTStepSize;
    }

    public javax.swing.JButton getjBColorSource() {
        return jBColorSource;
    }

    public javax.swing.JButton getjBColorTarget() {
        return jBColorTarget;
    }

    public javax.swing.JButton getjBEdge() {
        return jBEdge;
    }

    public javax.swing.JButton getjBReDraw() {
        return jBReDraw;
    }

    public javax.swing.JSlider getjSStifness() {
        return jSStifness;
    }

    public javax.swing.JPanel getjPEdges() {
        return jPEdges;
    }

    public javax.swing.JPanel getjPSource() {
        return jPSource;
    }

    public javax.swing.JPanel getjPTarget() {
        return jPTarget;
    }

    public javax.swing.JLabel getjLMaxAngular() {
        return jLMaxAngular;
    }

    public javax.swing.JLabel getjLMaxPosition() {
        return jLMaxPosition;
    }

    public javax.swing.JLabel getjLMaxScale() {
        return jLMaxScale;
    }

    public javax.swing.JCheckBox getjCBMinAngular() {
        return jCBMinAngular;
    }

    public javax.swing.JCheckBox getjCBMinPosition() {
        return jCBMinPosition;
    }

    public javax.swing.JCheckBox getjCBMinScale() {
        return jCBMinScale;
    }

    public javax.swing.JSlider getjSMinAngular() {
        return jSMinAngular;
    }

    public javax.swing.JSlider getjSMinPosition() {
        return jSMinPosition;
    }

    public javax.swing.JSlider getjSMinScale() {
        return jSMinScale;
    }

    public javax.swing.JLabel getjLMinAngular() {
        return jLMinAngular;
    }

    public javax.swing.JLabel getjLMinPosition() {
        return jLMinPosition;
    }

    public javax.swing.JLabel getjLMinScale() {
        return jLMinScale;
    }

    public javax.swing.JTextField getjTStepConvexHull() {
        return jTStepConvexHull;
    }

    public javax.swing.JCheckBox getjCBPED() {
        return jCBPED;
    }

    public void setjCBPED(javax.swing.JCheckBox jCBPED) {
        this.jCBPED = jCBPED;
    }
    
    public javax.swing.JCheckBox getjCBPEDSOURCE() {
        return jCBPEDSOURCE;
    }

    public void setjCBPEDSOURCE(javax.swing.JCheckBox jCBPEDSOURCE) {
        this.jCBPEDSOURCE = jCBPEDSOURCE;
    }
    
    public javax.swing.JCheckBox getjCBPEDTARGET() {
        return jCBPEDTARGET;
    }

    public void setjCBPEDTARGET(javax.swing.JCheckBox jCBPEDTARGET) {
        this.jCBPEDTARGET = jCBPEDTARGET;
    }

    /**
     * @return the jLPEDTRANSPARENT
     */
    public javax.swing.JLabel getjLPEDTRANSPARENT() {
        return jLPEDTRANSPARENT;
    }

    /**
     * @param jLPEDTRANSPARENT the jLPEDTRANSPARENT to set
     */
    public void setjLPEDTRANSPARENT(javax.swing.JLabel jLPEDTRANSPARENT) {
        this.jLPEDTRANSPARENT = jLPEDTRANSPARENT;
    }

    /**
     * @return the jTPEDTRANSPARENT
     */
    public javax.swing.JTextField getjTPEDTRANSPARENT() {
        return jTPEDTRANSPARENT;
    }

    /**
     * @param jTPEDTRANSPARENT the jTPEDTRANSPARENT to set
     */
    public void setjTPEDTRANSPARENT(javax.swing.JTextField jTPEDTRANSPARENT) {
        this.jTPEDTRANSPARENT = jTPEDTRANSPARENT;
    }

    /**
     * @return the jTPenalty
     */
    public javax.swing.JTextField getjTPenalty() {
        return jTPenalty;
    }

    /**
     * @param jTPenalty the jTPenalty to set
     */
    public void setjTPenalty(javax.swing.JTextField jTPenalty) {
        this.jTPenalty = jTPenalty;
    }

    /**
     * @return the jLPenalty
     */
    public javax.swing.JLabel getjLPenalty() {
        return jLPenalty;
    }

    /**
     * @param jLPenalty the jLPenalty to set
     */
    public void setjLPenalty(javax.swing.JLabel jLPenalty) {
        this.jLPenalty = jLPenalty;
    }
}
