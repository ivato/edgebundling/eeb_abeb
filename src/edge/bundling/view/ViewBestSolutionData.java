package edge.bundling.view;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ViewBestSolutionData extends JFrame {

    JTextArea jTABestSolution = new JTextArea();

     //*************************************************************************************************************
    /**
     *Janela que mosta os dados da melhor solução
     */
//*************************************************************************************************************    
    public ViewBestSolutionData(String solution) {
        super("Best Solution");

        jTABestSolution.setText("");
        jTABestSolution.setText(solution);
        jTABestSolution.setSize(300, 200);
        jTABestSolution.setLineWrap(true);

        JScrollPane scroll = new JScrollPane(jTABestSolution);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        this.add(scroll);
        this.setSize(600, 600);
        this.setVisible(true);
    }
}
