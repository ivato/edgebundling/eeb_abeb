package edge.bundling.view.io;

import edge.bundling.graph.Graph;
import edge.bundling.graph.GraphBundled;

import java.io.File;

interface InterfaceGraphReader {
	Graph importGraph(File xmlFile, GraphBundled graphBundled) throws Exception;
}
