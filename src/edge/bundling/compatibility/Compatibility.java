package edge.bundling.compatibility;

import edge.bundling.graph.Bundle;
import edge.bundling.graph.Edge;
import edge.bundling.graph.Node;
import edge.bundling.util.Flags;
import edge.bundling.util.Geometric;
import edge.bundling.util.GraphData;
import java.awt.Point;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;

public class Compatibility {

    public double xP;
    public double yP;
    public double xPc;
    public double yPc;

    public double xQ;
    public double yQ;
    public double xQc;
    public double yQc;

    public double compatibility(Edge edge1, Edge edge2) {
        setCoordinates(edge1, edge2);
        return GraphData.getCompatibility(edge1.getId(), edge2.getId());
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade do feixe - somatória das compatibilidades das
     * arestas duas a duas
     *
     * @param bundle - feixe para o qual será calculada a compatibilidade
     * @return totalCompatibility
     */
//*************************************************************************************************************     
    public double compatibility(Bundle bundle) {
        double min = 1;
        double compatibility;
        int penaltyCount = 0;

        double threshold = threshold();

        Edge edge1;
        Edge edge2;

        double totalBundleCompatibility = 0;

        //Recupera as arestas do feixe
        LinkedList<Edge> listEdgeBundle = bundle.getEdges();
        int nrEdges = listEdgeBundle.size();

        if (nrEdges > 1) {

            edge1 = (Edge) listEdgeBundle.get(0);
            edge2 = (Edge) listEdgeBundle.get(1);
            min = GraphData.getCompatibility(edge1.getId(), edge2.getId());

            //Compara cada aresta com as outras arestas do feixe
            for (int j = 0; j < nrEdges; j++) {
                edge1 = (Edge) listEdgeBundle.get(j);

                //Calcula a compatibilidade com as outras arestas
                for (int i = (j + 1); i < nrEdges; i++) {
                    edge2 = (Edge) listEdgeBundle.get(i);

                    compatibility = GraphData.getCompatibility(edge1.getId(), edge2.getId());

                    if (min > compatibility) {
                        min = compatibility;
                    }

                    compatibility = scaling(compatibility);
                    totalBundleCompatibility += compatibility;
                }
            }
            

            //Verifica se o menor valor de compatibilidade do feixe ultrapassa o threshold
            //Caso afirmativo seta uma penalidade de 1 e estabelece totalBundleCompatibility = -1
            //Isso diferencia o caso de ter apenas uma aresta que tem totalBundleCompatibility = 0
            if (min < threshold) {
                penaltyCount = 1;
                totalBundleCompatibility = Flags.PENALTY();
                //totalBundleCompatibility = -1;
                //System.out.println("Penalizou");
            }

        } else {
            totalBundleCompatibility = 0;
        }

        bundle.setPenalty(penaltyCount);

        return (totalBundleCompatibility);
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade angular do feixe - somatória das
     * compatibilidades das arestas duas a duas
     *
     * @param bundle - feixe para o qual será calculada a compatibilidade
     * @return totalCompatibility
     */
//*************************************************************************************************************     
    public double compatibilityAngular(Bundle bundle) {
        double min = 1;
        double compatibility;

        Edge edge1;
        Edge edge2;

        double totalBundleCompatibility = 0;

        //Recupera as arestas do feixe
        LinkedList<Edge> listEdgeBundle = bundle.getEdges();
        int nrEdges = listEdgeBundle.size();

        if (nrEdges > 1) {

            edge1 = (Edge) listEdgeBundle.get(0);
            edge2 = (Edge) listEdgeBundle.get(1);
            min = GraphData.getCompatibility(edge1.getId(), edge2.getId());

            //Compara cada aresta com as outras arestas do feixe
            for (int j = 0; j < nrEdges; j++) {
                edge1 = (Edge) listEdgeBundle.get(j);

                //Calcula a compatibilidade com as outras arestas
                for (int i = (j + 1); i < nrEdges; i++) {
                    edge2 = (Edge) listEdgeBundle.get(i);

                    compatibility = GraphData.getCompatibilityAngular(edge1.getId(), edge2.getId());

                    if (min > compatibility) {
                        min = compatibility;
                    }
                }
            }

            totalBundleCompatibility = scaling(min);

        } else {
            totalBundleCompatibility = 0.2;
        }
        return (totalBundleCompatibility);
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade escalar do feixe - somatória das
     * compatibilidades das arestas duas a duas
     *
     * @param bundle - feixe para o qual será calculada a compatibilidade
     * @return totalCompatibility
     */
//*************************************************************************************************************        
    public double compatibilityScale(Bundle bundle) {
        double min = 1;
        double compatibility;

        Edge edge1;
        Edge edge2;

        double totalBundleCompatibility = 0;

        //Recupera as arestas do feixe
        LinkedList<Edge> listEdgeBundle = bundle.getEdges();
        int nrEdges = listEdgeBundle.size();

        if (nrEdges > 1) {

            edge1 = (Edge) listEdgeBundle.get(0);
            edge2 = (Edge) listEdgeBundle.get(1);
            min = GraphData.getCompatibility(edge1.getId(), edge2.getId());

            //Compara cada aresta com as outras arestas do feixe
            for (int j = 0; j < nrEdges; j++) {
                edge1 = (Edge) listEdgeBundle.get(j);

                //Calcula a compatibilidade com as outras arestas
                for (int i = (j + 1); i < nrEdges; i++) {
                    edge2 = (Edge) listEdgeBundle.get(i);

                    compatibility = GraphData.getCompatibilityScale(edge1.getId(), edge2.getId());

                    if (min > compatibility) {
                        min = compatibility;
                    }
                }
            }
            totalBundleCompatibility = scaling(min);

        } else {
            totalBundleCompatibility = 0.2;
        }
        return (totalBundleCompatibility);
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade de posição do feixe - somatória das
     * compatibilidades das arestas duas a duas
     *
     * @param bundle - feixe para o qual será calculada a compatibilidade
     * @return totalCompatibility
     */
//*************************************************************************************************************     
    public double compatibilityPosition(Bundle bundle) {
        double min = 1;
        double compatibility;

        Edge edge1;
        Edge edge2;

        double totalBundleCompatibility = 0;

        //Recupera as arestas do feixe
        LinkedList<Edge> listEdgeBundle = bundle.getEdges();
        int nrEdges = listEdgeBundle.size();

        if (nrEdges > 1) {

            edge1 = (Edge) listEdgeBundle.get(0);
            edge2 = (Edge) listEdgeBundle.get(1);
            min = GraphData.getCompatibility(edge1.getId(), edge2.getId());

            //Compara cada aresta com as outras arestas do feixe
            for (int j = 0; j < nrEdges; j++) {
                edge1 = (Edge) listEdgeBundle.get(j);

                //Calcula a compatibilidade com as outras arestas
                for (int i = (j + 1); i < nrEdges; i++) {
                    edge2 = (Edge) listEdgeBundle.get(i);
                    compatibility = GraphData.getCompatibilityPosition(edge1.getId(), edge2.getId());

                    if (min > compatibility) {
                        min = compatibility;
                    }
                }
            }

            totalBundleCompatibility = scaling(min);

        } else {
            totalBundleCompatibility = 0.1;
        }
        return (totalBundleCompatibility);
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade total
     *
     * @return totalCompatibility
     */
//*************************************************************************************************************    
    public double totalCompatibility(Edge edge1, Edge edge2) {
        setCoordinates(edge1, edge2);
        double c = execute();
        return c;
    }

    //*************************************************************************************************************
    /**
     * Método auxiliar que executa os calculos de compatibilidade entre duas
     * arestas
     *
     * @return totalCompatibility
     */
//*************************************************************************************************************     
    private double execute() {
        double Ca = 1;
        double Cs = 1;
        double Cp = 1;
        double Cv = 1;
        double Ct;

        if (Flags.isANGULAR()) {
            Ca = angular();
        }

        if (Flags.isSCALE()) {
            Cs = scale();
        }

        if (Flags.isPOSITION()) {
            Cp = position();
        }

        if (Flags.isVISIBILITY()) {
            Cv = visibility();
        }

        Ct = Ca * Cs * Cp * Cv;

        return Ct;
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade angular entre duas arestas retorna um valor
     * entre 0 e 1
     *
     * @return angle
     */
//*************************************************************************************************************     
    private double angular() {
        Point.Double p1 = new Point.Double(xP, yP);
        Point.Double p2 = new Point.Double(xPc, yPc);
        Point.Double p3 = new Point.Double(xQ, yQ);
        Point.Double p4 = new Point.Double(xQc, yQc);

        double angle = Geometric.angle(p1, p2, p3, p4);
        double v = angle;

        v = Geometric.angleNormalized(angle);

        if (angle == 0) {
            return 1;
        }
        if (angle == 180) {
            return 0;
        }
        return (double) v;
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade angular entre duas arestas retorna um valor
     * entre 0 e 1
     *
     * @return angle
     */
//*************************************************************************************************************     
    public double angular(Edge edge1, Edge edge2) {
        setCoordinates(edge1, edge2);
        double Ca = angular();
        return Ca;
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade de escala entre duas arestas
     *
     * @return escala
     */
//*************************************************************************************************************     
    private double scale() {
        Point.Double p1 = new Point.Double(xP, yP);
        Point.Double p2 = new Point.Double(xPc, yPc);
        Point.Double p3 = new Point.Double(xQ, yQ);
        Point.Double p4 = new Point.Double(xQc, yQc);

        double le1 = Geometric.euclideanDistance(p1, p2);
        double le2 = Geometric.euclideanDistance(p3, p4);

        double lavg = (le1 + le2) / 2.0;
        double result = 2.0 / ((lavg / Math.min(le1, le2)) + (Math.max(le1, le2) / lavg));
        BigDecimal value = new BigDecimal(result).setScale(3, RoundingMode.HALF_DOWN);

        return value.doubleValue();
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade de escala entre duas arestas
     *
     * @return escala
     */
//*************************************************************************************************************     
    public double scale(Edge edge1, Edge edge2) {
        setCoordinates(edge1, edge2);
        double Cs = scale();
        return Cs;
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade de posição entre duas arestas retorna um valor
     * entre 0 e 1
     *
     * @return posição
     */
//*************************************************************************************************************     
    private double position() {
        Point.Double p1 = new Point.Double(xP, yP);
        Point.Double p2 = new Point.Double(xPc, yPc);
        Point.Double p3 = new Point.Double(xQ, yQ);
        Point.Double p4 = new Point.Double(xQc, yQc);

        double lavg = (Geometric.euclideanDistance(p1, p2) + Geometric.euclideanDistance(p3, p4)) / 2.0;
        Point.Double midP = new Point.Double((p2.x + p1.x) / 2.0, (p2.y + p1.y) / 2.0);
        Point.Double midQ = new Point.Double((p4.x + p3.x) / 2.0, (p4.y + p3.y) / 2.0);
        double result = lavg / (lavg + Geometric.euclideanDistance(midP, midQ));
        BigDecimal value = new BigDecimal(result).setScale(3, RoundingMode.HALF_DOWN);

        return value.doubleValue();
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade de posição entre duas arestas retorna um valor
     * entre 0 e 1
     *
     * @return posição
     */
//*************************************************************************************************************         
    public double position(Edge edge1, Edge edge2) {
        setCoordinates(edge1, edge2);
        double Cp = position();
        return Cp;
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade de visibilidade entre duas arestas retorna um
     * valor entre 0 e 1
     *
     * @return visibilidade
     */
//*************************************************************************************************************     
    private double visibility() {

        Point.Double p1 = new Point.Double(xP, yP);
        Point.Double p2 = new Point.Double(xPc, yPc);
        Point.Double p3 = new Point.Double(xQ, yQ);
        Point.Double p4 = new Point.Double(xQc, yQc);

        Point.Double Pm = new Point.Double((p2.x + p1.x) / 2.0, (p2.y + p1.y) / 2.0);
        Point.Double I0 = Geometric.projectPointOnLine(p3, p1, p2);
        Point.Double I1 = Geometric.projectPointOnLine(p4, p1, p2);
        Point.Double Im = new Point.Double((I1.x + I0.x) / 2.0, (I1.y + I0.y) / 2.0);
        double VPQ = Math.max(0, (1 - ((2 * Geometric.euclideanDistance(Pm, Im)) / Geometric.euclideanDistance(I0, I1))));

        Pm = new Point.Double((p4.x + p3.x) / 2.0, (p4.y + p3.y) / 2.0);
        I0 = Geometric.projectPointOnLine(p1, p3, p4);
        I1 = Geometric.projectPointOnLine(p2, p3, p4);
        Im = new Point.Double((I1.x + I0.x) / 2.0, (I1.y + I0.y) / 2.0);
        double VQP = Math.max(0, (1 - ((2 * Geometric.euclideanDistance(Pm, Im)) / Geometric.euclideanDistance(I0, I1))));

        double result = Math.min(VPQ, VQP);

        if (result == 0) {
            result = 0.0015;
        }

        BigDecimal value = new BigDecimal(result).setScale(4, RoundingMode.HALF_DOWN);

        return value.doubleValue();
    }

    //*************************************************************************************************************
    /**
     * Calcula a compatibilidade de visibilidade entre duas arestas retorna um
     * valor entre 0 e 1
     *
     * @return visibilidade
     */
//*************************************************************************************************************     
    public double visibility(Edge edge1, Edge edge2) {
        setCoordinates(edge1, edge2);
        double Cv = visibility();
        return Cv;
    }
 
    //*************************************************************************************************************
    /**
     * Calcula o escalonamento da medida de compatibilidade
     *
     * @return compatibility
     */
//*************************************************************************************************************     
    private double scaling(double compatibility) {
        double threshold = threshold();
        return (1 + ((compatibility - threshold) / threshold));
    }

    //*************************************************************************************************************
    /**
     * Calcula o threshold total baseado em todas as medidas de compatibilidade
     *
     * @return threshold
     */
//*************************************************************************************************************     
    public double threshold() {
        double threshold = 1;

        if (Flags.isANGULAR()) {
            threshold *= Flags.COMPATIBILITY_MAX_ANGULAR_THRESHOLD();
        }
        if (Flags.isSCALE()) {
            threshold *= Flags.COMPATIBILITY_MAX_SCALE_THRESHOLD();
        }
        if (Flags.isPOSITION()) {
            threshold *= Flags.COMPATIBILITY_MAX_POSITION_THRESHOLD();
        }
        if (Flags.isVISIBILITY()) {
            threshold *= Flags.COMPATIBILITY_MAX_VISIBILITY_THRESHOLD();
        }
        return threshold;
    }

    //*************************************************************************************************************
    /**
     * Seta as coordenadas dos pontos da arestas
     *
     * @param edge1 - aresta 1
     * @param edge1 - aresta 2
     */
//*************************************************************************************************************        
    private void setCoordinates(Edge edge1, Edge edge2) {

        double x, y, x1, y1, x2, y2;
        Node sameNode = new Node();

        if (edge1.getStartNode() == edge2.getStartNode()) {
            sameNode = edge1.getStartNode();
        }
        if (edge1.getStartNode() == edge2.getEndNode()) {
            sameNode = edge1.getStartNode();
        }
        if (edge1.getEndNode() == edge2.getStartNode()) {
            sameNode = edge1.getEndNode();
        }
        if (edge1.getEndNode() == edge2.getEndNode()) {
            sameNode = edge1.getEndNode();
        }

        if (sameNode != edge1.getStartNode()) {
            x1 = edge1.getStartNode().getX();
            y1 = edge1.getStartNode().getY();
        } else {
            x1 = edge1.getEndNode().getX();
            y1 = edge1.getEndNode().getY();
        }

        if (sameNode != edge2.getStartNode()) {
            x2 = edge2.getStartNode().getX();
            y2 = edge2.getStartNode().getY();
        } else {
            x2 = edge2.getEndNode().getX();
            y2 = edge2.getEndNode().getY();
        }

        x = sameNode.getX();
        y = sameNode.getY();

        this.xP = x1;
        this.yP = y1;
        this.xPc = x;
        this.yPc = y;

        this.xQ = x2;
        this.yQ = y2;
        this.xQc = x;
        this.yQc = y;
    }
}
