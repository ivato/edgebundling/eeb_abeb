package edge.bundling.statistics;

public class GlobalStatistic {

    private double sumGlobalFitness = 0.0;
    private double quantityGlobalIndividuals = 0.0;
    private Double bestGlobalFitness = null;
    private Double worstGlobalFitness = null;


    public double getSumGlobalFitness() {
        return sumGlobalFitness;
    }

    public double getQuantityGlobalIndividuals() {
        return quantityGlobalIndividuals;
    }

    public Double getBestGlobalFitness() {
        return bestGlobalFitness;
    }

    public Double getWorstGlobalFitness() {
        return worstGlobalFitness;
    }

    public void setSumGlobalFitness(double sumGlobalFitness) {
        this.sumGlobalFitness = sumGlobalFitness;
    }

    public void setQuantityGlobalIndividuals(double quantityGlobalIndividuals) {
        this.quantityGlobalIndividuals = quantityGlobalIndividuals;
    }

    public void setBestGlobalFitness(Double bestGlobalFitness) {
        this.bestGlobalFitness = bestGlobalFitness;
    }

    public void setWorstGlobalFitness(Double worstGlobalFitness) {
        this.worstGlobalFitness = worstGlobalFitness;
    }
}
