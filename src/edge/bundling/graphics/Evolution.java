package edge.bundling.graphics;

import edge.bundling.methods.genetic.Individual;
import edge.bundling.methods.genetic.Population;
import java.util.ListIterator;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class Evolution extends JFrame {

    Population population = new Population();

    public Evolution(String title, Population population) {

        super(title);
        this.population = population;

        DefaultCategoryDataset dataset = createDataset(population);

        JFreeChart lineChart = ChartFactory.createLineChart(
                "Evolution per Generation",
                "Generation",
                "Fitness",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);

        ChartPanel chartPanel = new ChartPanel(lineChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(1120, 800));
        setContentPane(chartPanel);

    }

    private DefaultCategoryDataset createDataset(Population population) {
        int generation = 0;
        double[] fitness = new double[population.getSize()];
        int i = 0;

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        ListIterator<Individual> listIterator = population.getIndividuals().listIterator();
        while (listIterator.hasNext()) {
            Individual individual = new Individual();
            individual = (Individual) listIterator.next();
            fitness[i] = individual.getFitness();
            i++;
        }

        for (int j = (fitness.length - 1); j >= 0; j--) {
            generation++;
            dataset.addValue(fitness[j], "Fitness", String.valueOf(generation));
        }

        return dataset;
    }
}
