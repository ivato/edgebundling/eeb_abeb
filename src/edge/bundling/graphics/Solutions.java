package edge.bundling.graphics;

import edge.bundling.methods.genetic.Individual;
import edge.bundling.methods.genetic.Population;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Shape;
import java.io.FileNotFoundException;
import java.util.ListIterator;

import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.util.ShapeUtilities;

public class Solutions extends JFrame {

    Population initialPopulation = new Population();

    public Solutions(String title, Population initialPopulation, Population finalPopulation, Individual bestIndividual) throws FileNotFoundException {

        super(title);
        this.initialPopulation = initialPopulation;

        XYDataset dataset = createDataset(initialPopulation, finalPopulation, bestIndividual);

        JFreeChart chart = ChartFactory.createScatterPlot("Solutions Generated",
                "Number of Bundles",
                "Compatibility",
                dataset,
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );

        Shape shape = ShapeUtilities.createDiamond(3);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(new Color(255, 228, 196));

        plot.setDomainPannable(true);
        plot.setDomainZeroBaselineVisible(true);
        plot.setDomainGridlineStroke(new BasicStroke(0.0f));
        plot.setDomainGridlinePaint(Color.blue);
        plot.setDomainMinorGridlineStroke(new BasicStroke(0.0f));
        plot.setDomainMinorGridlinesVisible(true);

        plot.setRangePannable(true);
        plot.setRangeZeroBaselineVisible(true);
        plot.setRangeGridlinePaint(Color.blue);
        plot.setRangeGridlineStroke(new BasicStroke(0.0f));
        plot.setRangeMinorGridlineStroke(new BasicStroke(0.0f));
        plot.setRangeMinorGridlinesVisible(true);

        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
        renderer.setSeriesOutlinePaint(0, Color.black);
        renderer.setUseOutlinePaint(true);
        renderer.setSeriesShape(0, shape);

        NumberAxis domainAxis = (NumberAxis) plot.getDomainAxis();
        domainAxis.setAutoRangeIncludesZero(false);
        domainAxis.setTickMarkInsideLength(2.0f);
        domainAxis.setTickMarkOutsideLength(2.0f);
        domainAxis.setMinorTickCount(2);
        domainAxis.setMinorTickMarksVisible(true);

        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setTickMarkInsideLength(2.0f);
        rangeAxis.setTickMarkOutsideLength(2.0f);
        rangeAxis.setMinorTickCount(2);
        rangeAxis.setMinorTickMarksVisible(true);

        ChartPanel panel = new ChartPanel(chart);
        setContentPane(panel);

    }

    private XYDataset createDataset(Population initialPopulation, Population finalPopulation, Individual bestIndividual) {

        XYSeriesCollection dataset = new XYSeriesCollection();
        XYSeries series1 = new XYSeries("Initial Population");
        XYSeries series2 = new XYSeries("Final Population");
        XYSeries series3 = new XYSeries("Best Solution");

        int numberOfBundles;
        double compatibility;

        ListIterator<Individual> listIterator = initialPopulation.getIndividuals().listIterator();
        while (listIterator.hasNext()) {
            Individual individual = new Individual();
            individual = (Individual) listIterator.next();
            numberOfBundles = individual.getNumberOfBundles();
            compatibility = individual.getCompatibility();

            series1.add(numberOfBundles, compatibility);
        }

        listIterator = finalPopulation.getIndividuals().listIterator();
        while (listIterator.hasNext()) {
            Individual individual = new Individual();
            individual = (Individual) listIterator.next();
            numberOfBundles = individual.getNumberOfBundles();
            compatibility = individual.getCompatibility();

            series2.add(numberOfBundles, compatibility);
        }

        numberOfBundles = bestIndividual.getNumberOfBundles();
        compatibility = bestIndividual.getCompatibility();
        series3.add(numberOfBundles, compatibility);

        dataset.addSeries(series3);
        dataset.addSeries(series1);
        dataset.addSeries(series2);

        return dataset;
    }
}
