package edge.bundling.graphics;

import edge.bundling.methods.genetic.Individual;
import edge.bundling.methods.genetic.Population;
import edge.bundling.util.Flags;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Shape;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ListIterator;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.util.ShapeUtilities;

public class Generator {

    Population initialPopulation = new Population();
    Population population = new Population();
    int testNumber = 0;

    public Generator(int testNumber) {
        this.testNumber = testNumber;
    }

    public void evolution(Population population, String angle) throws FileNotFoundException, IOException {

        this.population = population;

        DefaultCategoryDataset dataset = createDataset(population);

        JFreeChart chart = ChartFactory.createLineChart(
                "Evolution per Generation",
                "Generation",
                "Fitness",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);

        OutputStream arquivo = new FileOutputStream(angle + "TESTE" + testNumber + " - " + Flags.NAMEFILE() + " - EVOLUTION.png");
        ChartUtilities.writeChartAsPNG(arquivo, chart, 1120, 800);

    }

    public void solutions(Population initialPopulation, Population finalPopulation, Individual bestIndividual, String angle) throws FileNotFoundException, IOException {

        this.initialPopulation = initialPopulation;

        XYDataset dataset = createDataset(initialPopulation, finalPopulation, bestIndividual);

        JFreeChart chart = ChartFactory.createScatterPlot("Solutions Generated",
                "Number of Bundles",
                "Compatibility",
                dataset,
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );

        Shape shape = ShapeUtilities.createDiamond(3);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(new Color(255, 228, 196));

        plot.setDomainPannable(true);
        plot.setDomainZeroBaselineVisible(true);
        plot.setDomainGridlineStroke(new BasicStroke(0.0f));
        plot.setDomainGridlinePaint(Color.blue);
        plot.setDomainMinorGridlineStroke(new BasicStroke(0.0f));
        plot.setDomainMinorGridlinesVisible(true);

        plot.setRangePannable(true);
        plot.setRangeZeroBaselineVisible(true);
        plot.setRangeGridlinePaint(Color.blue);
        plot.setRangeGridlineStroke(new BasicStroke(0.0f));
        plot.setRangeMinorGridlineStroke(new BasicStroke(0.0f));
        plot.setRangeMinorGridlinesVisible(true);

        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
        renderer.setSeriesOutlinePaint(0, Color.black);
        renderer.setUseOutlinePaint(true);
        renderer.setSeriesShape(0, shape);

        NumberAxis domainAxis = (NumberAxis) plot.getDomainAxis();
        domainAxis.setAutoRangeIncludesZero(false);
        domainAxis.setTickMarkInsideLength(2.0f);
        domainAxis.setTickMarkOutsideLength(2.0f);
        domainAxis.setMinorTickCount(2);
        domainAxis.setMinorTickMarksVisible(true);

        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setTickMarkInsideLength(2.0f);
        rangeAxis.setTickMarkOutsideLength(2.0f);
        rangeAxis.setMinorTickCount(2);
        rangeAxis.setMinorTickMarksVisible(true);

        OutputStream arquivo = new FileOutputStream(angle + "TESTE" + testNumber + " - " + Flags.NAMEFILE() + " - SOLUTIONS.png");
        ChartUtilities.writeChartAsPNG(arquivo, chart, 1120, 800);
    }

    private DefaultCategoryDataset createDataset(Population population) {
        int generation = 0;
        double[] fitness = new double[population.getSize()];
        int i = 0;

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        ListIterator<Individual> listIterator = population.getIndividuals().listIterator();
        while (listIterator.hasNext()) {
            Individual individual = new Individual();
            individual = (Individual) listIterator.next();
            fitness[i] = individual.getFitness();
            i++;
        }

        for (int j = (fitness.length - 1); j >= 0; j--) {
            generation++;
            dataset.addValue(fitness[j], "Fitness", String.valueOf(generation));
        }

        return dataset;
    }

    private XYDataset createDataset(Population initialPopulation, Population finalPopulation, Individual bestIndividual) {

        XYSeriesCollection dataset = new XYSeriesCollection();
        XYSeries series1 = new XYSeries("Initial Population");
        XYSeries series2 = new XYSeries("Final Population");
        XYSeries series3 = new XYSeries("Best Solution");

        int numberOfBundles;
        double compatibility;

        ListIterator<Individual> listIterator = initialPopulation.getIndividuals().listIterator();
        while (listIterator.hasNext()) {
            Individual individual = new Individual();
            individual = (Individual) listIterator.next();
            numberOfBundles = individual.getNumberOfBundles();
            compatibility = individual.getCompatibility();

            series1.add(numberOfBundles, compatibility);
        }

        listIterator = finalPopulation.getIndividuals().listIterator();
        while (listIterator.hasNext()) {
            Individual individual = new Individual();
            individual = (Individual) listIterator.next();
            numberOfBundles = individual.getNumberOfBundles();
            compatibility = individual.getCompatibility();

            series2.add(numberOfBundles, compatibility);
        }

        numberOfBundles = bestIndividual.getNumberOfBundles();
        compatibility = bestIndividual.getCompatibility();
        series3.add(numberOfBundles, compatibility);

        dataset.addSeries(series3);
        dataset.addSeries(series1);
        dataset.addSeries(series2);

        return dataset;
    }
}
