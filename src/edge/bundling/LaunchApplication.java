package edge.bundling;

import javax.swing.JFrame;

public class LaunchApplication {
    public static void main(String[] args){
        MainWindow frm = new MainWindow();
        frm.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frm.setVisible(true);
    }
}
