
package edge.bundling.methods.cover;

public class NodeCover {
    
    private int id;
    private int numberOfEdges = 0;


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public int getNumberOfEdges() {
        return numberOfEdges;
    }

    public void setNumberOfEdges(int numberOfEdges) {
        this.numberOfEdges = numberOfEdges;
    }
    
    
}
