package edge.bundling.methods.cover;

import edge.bundling.graph.Edge;
import edge.bundling.graph.Graph;
import edge.bundling.graph.Node;
import edge.bundling.util.Flags;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Cover {

    private List coverList = new ArrayList();

    
    
    private List orderedNodes(List listOfNodes, List<NodeCover> list) {

        List orderedList = new ArrayList();
        Map<Integer, Integer> listNodes = new HashMap<Integer, Integer>();

        for (Object idNode : listOfNodes) {
            NodeCover n = (NodeCover) list.get(Integer.parseInt(idNode.toString()));
            listNodes.put(n.getId(), n.getNumberOfEdges());
        }

        List<Map.Entry<Integer, Integer>> entryListNodes = new ArrayList<Map.Entry<Integer, Integer>>(listNodes.entrySet());
        Collections.sort(entryListNodes, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> integerEntry, Map.Entry<Integer, Integer> integerEntry2) {
                return integerEntry2.getValue().compareTo(integerEntry.getValue());
            }
        });

        for (Map.Entry<Integer, Integer> mapping : entryListNodes) {
            int indexNode = mapping.getKey();
            orderedList.add(indexNode);
        }
        return orderedList;
    }

    private boolean allNodesVisited(List<NodeCover> list) {
        Iterator<NodeCover> nodeCoverList = list.iterator();
        while (nodeCoverList.hasNext()) {
            NodeCover n = (NodeCover) nodeCoverList.next();
            if (n.getNumberOfEdges() != 0) {
                return true;
            }

        }
        return false;
    }
    
    
//*************************************************************************************************************
    /// <summary>
    /// Calcula os vértices da cobertura
    /// </summary>
    /// <param name="graph">Grafo no qual será executado algoritmo de cobertura</param>
//*************************************************************************************************************   

    public void vertexCoverSet(Graph graph) throws Exception {

        int idOfNode, degreeNumber, idOfNode2, degreeNumber2;

        int[] nodePicked = new int[graph.getSizeNode()]; // Mantem uma cópia dos vértices do grafo
        int[] degree = new int[graph.getSizeNode()];
        int[] edgeVisited = new int[graph.getSizeEdge()];  // Determina se a aresta foi ou não usada em um feixe

        try {

            //Monta uma lista ordenada dos vértices com base no grau de cada
            //Os vértices da cobertura virão antes
            //Ordena as listas com base no grau dos vértices - ordem decrescente
            List<NodeCover> list = new ArrayList<NodeCover>();
            List listOfOrderedNodes2 = new ArrayList();

            Iterator<Node> nodeList = graph.getNodeList().iterator();
            while (nodeList.hasNext()) {
                Node n = (Node) nodeList.next();
                degree[n.getId()] = n.getNumberOfEdges();
                nodePicked[n.getId()] = 0;

                NodeCover nodeCover = new NodeCover();
                nodeCover.setId(n.getId());
                nodeCover.setNumberOfEdges(n.getNumberOfEdges());
                list.add(nodeCover);
                listOfOrderedNodes2.add(nodeCover.getId());
            }

            //Ordena a lista
            listOfOrderedNodes2 = orderedNodes(listOfOrderedNodes2, list);

            
            // Inicializa todas as arestas como não visitadas
            Iterator<Edge> edgeList = graph.getEdgeList().iterator();
            while (edgeList.hasNext()) {
                Edge e = (Edge) edgeList.next();
                edgeVisited[e.getId()] = 0;
            }

            Iterator iterator;
            for (int u = 0; u < graph.getSizeNode(); u++) {

                if (!allNodesVisited(list)) {
                    break;
                }
                
                //Recupera um vértice da lista de vértices ordenados
                idOfNode = (int) listOfOrderedNodes2.remove(0);
                
                //Se este vertice tiver aresta adjacente
                boolean flag = false;

                //Recupera todas as arestas adjacentes do vértice escolhido
                LinkedList<Edge> adjEdgeList = graph.getNodeList().get(idOfNode).getAdjEdgeList();
                iterator = adjEdgeList.iterator();

                while (iterator.hasNext()) {
                    Edge e = (Edge) iterator.next();
                    int edgeIndex = e.getId();

                    //Se a aresta selecionada não estiver em nenhum feixe a escolhe para fazer
                    //parte do feixe e marca como usada
                    if (edgeVisited[edgeIndex] == 0) {
                        edgeVisited[edgeIndex] = 1;
                        flag = true;

                        Node n1 = graph.getNodeList().get(idOfNode);
                        Node n2 = graph.getNodeList().get(e.getEndNode().getId());
                        if (n2.getId() == n1.getId()) {
                            n2 = graph.getNodeList().get(e.getStartNode().getId());
                        }
                        
                        //Atualiza para cada vértice extremo das arestas a quantidade de arestas 
                        //disponíveis (que ainda não foram usadas), ou seja, o grau do vértice diminui
                        degree[n2.getId()] = degree[n2.getId()] - 1;
                        list.get(n2.getId()).setNumberOfEdges(degree[n2.getId()]);

                        degree[n1.getId()] = degree[n1.getId()] - 1;
                        list.get(n1.getId()).setNumberOfEdges(degree[n1.getId()]);
                    }
                }

                //Ordena novamente a lista
                listOfOrderedNodes2 = orderedNodes(listOfOrderedNodes2, list);

                //Insere o vértice centro na cobertura
                if (flag) {
                    coverList.add(idOfNode);
                    graph.getNodeList().get(idOfNode).setCover(true);
                }
            }

            Flags.COVERLIST(coverList);

        } catch (Exception e) {
        }
    }

    
    public List getCoverList() {
        return coverList;
    }   
}
