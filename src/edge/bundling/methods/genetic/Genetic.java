package edge.bundling.methods.genetic;

import edge.bundling.statistics.GlobalStatistic;
import edge.bundling.statistics.LocalStatistic;
import edge.bundling.graph.Graph;
import edge.bundling.graph.GraphBundledComparable;
import edge.bundling.graphics.Generator;
import edge.bundling.util.Flags;
import edge.bundling.util.Log;
import edge.bundling.util.Message;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public final class Genetic {

    private Population currentPopulation = new Population();
    private Population bestIndividualsPopulation = new Population();
    private Population initialPopulation = new Population();
    private Graph graph;
    private Individual bestIndividual = new Individual();
    private Individual worstIndividual = new Individual();
    private String statistics = "";
    private int diversityStep = 1;
    private String angle = "";

    private Log logEvolutionIndividual = new Log();
    private Log logEvolution = new Log();
    private String parameters = "";
    private int testNumber;

    public Genetic() {

    }

    public Genetic(Graph graph, int t, String a) throws Exception {

       // System.out.println("Executando processo evolucionário...");
       

        this.angle = a;
        this.testNumber = t;

        this.currentPopulation = new Population();
        this.graph = graph;

        if (Flags.isLOG()) {
            //parameters = getParamenters(testNumber);
            Flags.STATISTICS(true);
            logEvolution.create(this.angle + " - " + Flags.NAMEFILE() + " - DataOfExperiments.txt", true);
            logEvolution.start();
        }

        this.currentPopulation.initialize(graph);
        this.initialPopulation = this.currentPopulation.clone();
        
        
        parameters = getParamenters(testNumber);
    }

    /*public Genetic(Graph graph, Population initialPopulation, int t, String a) {
       // System.out.println("Executando processo evolucionário...");

        this.angle = a;
        this.testNumber = t;

        this.currentPopulation = new Population();
        this.graph = graph;

        if (Flags.isLOG()) {
            //parameters = getParamenters(testNumber);
            Flags.STATISTICS(true);
            logEvolution.create(this.angle + " - " + Flags.NAMEFILE() + " - DataOfExperiments.txt", true);
            logEvolution.start();
        }

        this.currentPopulation = initialPopulation.clone();
        this.initialPopulation = initialPopulation.clone();
        this.currentPopulation.setGlobal();
        this.initialPopulation.setGlobal();
        
        parameters = getParamenters(testNumber);
    }*/

    //*************************************************************************************************************
    /**
     * Inicia a execução
     *
     * @param graph - Grafo no qual será executado algoritmo genético
     */
//*************************************************************************************************************     
    public void run(Graph graph) {
        try {

            this.run();

            if (Flags.isLOG()) {
                logEvolution.finish(parameters, Message.generateDataLog(bestIndividualsPopulation.getConvergenceGeneration(), currentPopulation.getSize(), bestIndividualsPopulation.getSize(), bestIndividualsPopulation.getBestSolution()));
                logEvolution.close();

                String name = this.angle + "TESTE" + testNumber + " - " + Flags.NAMEFILE() + " - BEST SOLUTION.txt";
                logEvolutionIndividual.create(name, false);
                logEvolutionIndividual.start();
                Individual individual = getBestIndividualOfBestIndividualsPopulation();
                String bundles = individual.showBundles();
                logEvolutionIndividual.save(bundles);
                logEvolutionIndividual.close();

                Generator gG = new Generator(testNumber);
                gG.evolution(bestIndividualsPopulation, this.angle);
                gG.solutions(getInitialPopulation(), getCurrentPopulation(), getBestIndividualOfBestIndividualsPopulation(), this.angle);
            }
            
        } catch (Exception e) {

        }
    }
    
    //*************************************************************************************************************
    /**
     * Executa o processo evolucionário
     *
     * @param graph - Grafo no qual será executado algoritmo genético
     */
//*************************************************************************************************************     
    private void run() throws Exception {

        int generations = 1;
        String temp;
        String msgLog = "";
        Log log = new Log();

        //Guarda o melhor indivíduo da geração icorrente
        //bestIndividualsPopulation.add(currentPopulation.getBestSolution().cloneBundlesAndAttributs());
        addBestIndividualsPopulation();

        if (Flags.isSTATISTICS()) {
            log.create(this.angle + " - " + Flags.NAMEFILE() + " - StatisticsOfEachGeneration.txt", true);
            statistics += showStatistics(generations);
        }

        ArrayList<Individual> parents = new ArrayList();
        ArrayList<Individual> children = new ArrayList();

        Selection selection = new Selection(2);
        Crossover crossing = new Crossover();
        Mutation mutation = new Mutation();
        Survival survival = new Survival();

        int sizeOfCurrentPopulation = currentPopulation.getSize();
         
        while (!isTerminationCondition(generations)) {

            generations++;

           //System.out.println("Rescuing");
            ////Recupera o melhor indivíduo da população anterior
            currentPopulation.savePreviousBestIndividual(currentPopulation.getBestSolution().clone());

            for (int i = 0; i < (sizeOfCurrentPopulation - 1); i++) {
                 
                //System.out.print("S, ");
                parents = selection.selection(currentPopulation, i);

                //System.out.print("C, ");
                children = crossing.crossover(graph.getEdgeList(), parents);
                
                //System.out.print("E, ");
                children.get(0).evaluate(currentPopulation);
                children.get(1).evaluate(currentPopulation);
                
                //System.out.println("Arestas soltas \t" + children.get(0).contMakeFeasibleArestasSoltas);
                //System.out.println("Arestas duplicadas \t" + children.get(0).contMakeFeasibleArestasDuplicadas);
                
               //System.out.print("M, ");
                mutation.mutation(children.get(0));
                children.get(0).evaluate(currentPopulation);
                mutation.mutation(children.get(1));
                children.get(1).evaluate(currentPopulation);
                
                //System.out.println("U, " + generations + " - " + diversityStep);
                survival.survival(currentPopulation, children, parents, generations);
            }

            
            //System.out.println("Geração " + generations + " via crossover fez viável \t" + crossing.contMakeFeasibleCrossover);       
            //crossing.contMakeFeasibleCrossover = 0;
            
            //System.out.print("\nAdding, ");
            //Insere o melhor indivíduo da população anterior na população corrente
            currentPopulation.addPreviousBestIndividual();

            //System.out.print("Saving, ");
            //Guarda o melhor indivíduo da geração corrente
            //bestIndividualsPopulation.add(currentPopulation.getBestSolution().cloneBundlesAndAttributs());
            addBestIndividualsPopulation();

            //System.out.println("Statiscing");
            if (Flags.isSTATISTICS()) {
               statistics += showStatistics(generations);
            }
            
            //System.out.println(generations + " - " + diversityStep);
        }
        
        Flags.MAX_GENERATION_FINAL(generations);

        if (Flags.isSTATISTICS()) {
            statistics += Message.statistics(getBestIndividualsPopulation().getBestSolution());

            log.save(statistics);
            log.close();
        }

        if (Flags.isAUTOMATIC_SAVING()) {
            Log populationSrt = new Log();
            populationSrt.create("FinalPopulation.txt");
            msgLog = currentPopulation.showPopulation();
            populationSrt.save(msgLog);
            populationSrt.close();

            populationSrt = new Log();
            populationSrt.create("BestPopulation.txt");
            msgLog = getBestIndividualsPopulation().showPopulation();
            populationSrt.save(msgLog);
            populationSrt.close();
        }
    }

    private void addBestIndividualsPopulation(){
        bestIndividualsPopulation.add(currentPopulation.getBestSolution().clone());
    }
    
    //*************************************************************************************************************
    /**
     * Verifica a condição de parada
     *
     * @param generations - número de gerações
     * @return boolean
     */
//*************************************************************************************************************     
    public boolean isTerminationCondition(int generations) {

        
        GraphBundledComparable gc = new GraphBundledComparable();       
        
        if (generations > 16500){
            return true;
        }
        
        
        //Continua o processo evolucionário por um número fixo de gerações
        if (Flags.isTERMINATION_BY_GENERATION()) {
            bestIndividualsPopulation.setConvergenceGeneration(getBestIndividualOfBestIndividualsPopulation().getGeneration());
            
            int limit = (int) (Flags.MAX_GENERATION());
            if (generations < limit) {
                return false;
            }
        }
        
        //Continua o processo evolucionário enquanto ocorrem mudanças
        if (Flags.isTERMINATION_BY_DIVERSITY()) {
            bestIndividualsPopulation.setConvergenceGeneration(getBestIndividualOfBestIndividualsPopulation().getGeneration());
            
            int test;
            test = gc.compare(currentPopulation.getBestSolution(),currentPopulation.getPreviousBestIndividual());
 
            //if (test != 0) {
            //Se melhorou
            if (test == -1) {
                diversityStep = 1;
                return false;
            } else {
                //Se é igual ou pior
                diversityStep++;
                if (diversityStep > Flags.MAX_GENERATION_TO_WAIT()) {
                    return true;
                } else {
                    return false;
                }
            }
        }


        //Continua o processo evolucionário enquanto a melhor solução esta melhorando
        /*if (Flags.isTERMINATION_BY_DIVERSITY()) {
            bestIndividualsPopulation.setConvergenceGeneration(getBestIndividualOfBestIndividualsPopulation().getGeneration());
            if (!currentPopulation.convergence(generations)) {
                diversityStep = 0;
                return false;
            } else {
                diversityStep++;
                if (diversityStep > Flags.MAX_GENERATION()) {
                    return true;
                } else {
                    return false;
                }
            }
        }*/

        return true;
    }

    //*************************************************************************************************************
    /**
     * Retorna os dados da melhor solução da população corrente
     *
     * @return best - dados da melhor solução
     */
//*************************************************************************************************************     
    public String showBestSolutionCurrentPopulation() {
        String best = "";

        best += "Geração de convergência: " + bestIndividualsPopulation.getConvergenceGeneration() + "\n";
        best += "Quantidade de indivíduos na população corrente: " + currentPopulation.getSize() + "\n";
        best += "Quantidade de indivíduos na população de melhores soluções: " + bestIndividualsPopulation.getSize() + "\n";

        Individual bestSolution = currentPopulation.getBestSolution();
        best += Message.individialData(bestSolution, false, "");
        return best;
    }

    //*************************************************************************************************************
    /**
     * Retorna os dados da melhor solução final
     *
     * @return best - dados da melhor solução
     */
//*************************************************************************************************************     
    public String showBestSolution() {
        String best = "";

        best += "Geração de convergência: " + bestIndividualsPopulation.getConvergenceGeneration() + "\n";
        best += "Quantidade de indivíduos na população corrente: " + currentPopulation.getSize() + "\n";
        best += "Quantidade de indivíduos na população de melhores soluções: " + bestIndividualsPopulation.getSize() + "\n";

        Individual bestSolution = getBestIndividualOfBestIndividualsPopulation();
        best += Message.individialData(bestSolution, false, "");
        return best;
    }

    //*************************************************************************************************************
    /**
     * Retorna os dados da melhor solução sem os feixes
     *
     * @return best - dados da melhor solução
     */
//*************************************************************************************************************     
    private String showStatistics(int generations) {

        String best = "";
        GlobalStatistic globalData = currentPopulation.getGlobalData();
        LocalStatistic localData = new LocalStatistic();
        localData.setSumLocalFitness(currentPopulation.getSumLocalFitness());
        localData.setQuantityLocalIndividuals(currentPopulation.getIndividuals().size());

        Individual bestSolution = currentPopulation.getBestSolution();
        Individual worstSolution = currentPopulation.getWorstSolution();
        best += Message.statistics(generations, bestSolution, worstSolution, localData, globalData, false);
        bestIndividual = bestSolution.clone();
        worstIndividual = worstSolution.clone();

        return best;
    }

    public String getParamenters(int testNumber) {
        String log = "";
        log += testNumber + "\t";

        log += graph.getSizeNode() + "\t";

        log += graph.getSizeEdge() + "\t";

        log += Flags.NAMEFILE() + "\t";

        if (Flags.isEB_STAR()) {
            log += "S\t";
        } else {
            log += "N\t";
        }

        if (Flags.isANGULAR()) {
            log += " A ";
        }
        if (Flags.isSCALE()) {
            log += " S ";
        }
        if (Flags.isPOSITION()) {
            log += " P ";
        }
        if (Flags.isVISIBILITY()) {
            log += " V ";
        }
        log += "\t";

        if (Flags.isOBJECTIVE()) {
            log += "O\t";
        }

        if (Flags.isCONSTRAINT()) {
            log += "C\t";
        }

        log += Flags.TAM_POPULATION() + "\t";
        log += this.initialPopulation.getIndividuals().size() + "\t";
        log += Flags.MAX_GENERATION_TO_WAIT() + "\t";
        log += Flags.CROSSOVER_RATIO() + "\t";

        if (Flags.isONE_POINT()) {
            log += "P\t";
        }

        if (Flags.isUNIFORM()) {
            log += "U\t";
        }

        log += Flags.MUTATION_RATIO() + "\t";

        if (Flags.isMUTATION_JOIN()) {
            log += " 1 ";
        }

        if (Flags.isMUTATION_SPLIT()) {
            log += " 2 ";
        }

        if (Flags.isMUTATION_MOVE()) {
            log += " 3 ";
        }

        if (Flags.isMUTATION_MERGE()) {
            log += " 4 ";
        }

        if (Flags.isMUTATION_REMOVE()) {
            log += " 5 ";
        }

        log += "\t";

        if (Flags.isTERMINATION_BY_GENERATION()) {
            log += "G\t";
        }

        if (Flags.isTERMINATION_BY_DIVERSITY()) {
            log += "D\t";
        }

        if (Flags.isROULETTE_WHEEL()) {
            log += "RW\t";
        }
        if (Flags.isRANKING()) {
            log += "RK\t";
        }
        if (Flags.isRANDOM()) {
            log += "RD\t";
        }
        if (Flags.isTOURNAMENT()) {
            log += "T\t";
        }

        log += Flags.SELECTIVE_PRESSURE() + "\t";

        return log;
    }
    
    
    public void setBestIndividualsPopulation(Population bestIndividualsPopulation) {
        this.bestIndividualsPopulation = bestIndividualsPopulation;
    }
    
   public Individual getBestIndividualOfCurrentPopulation() {
        return currentPopulation.getBestSolution();
    }
        
    public Individual getBestIndividualOfInitialPopulation() {
        return initialPopulation.getBestSolution();
    }
   
    public Individual getBestIndividualOfBestIndividualsPopulation() {
        return bestIndividualsPopulation.getBestSolution();
    }
    
    public Population getCurrentPopulation() {
        return currentPopulation;
    }
 
    public Population getInitialPopulation() {
        return initialPopulation;
    }
    
    public Population getBestIndividualsPopulation() {
        return bestIndividualsPopulation;
    }

}
