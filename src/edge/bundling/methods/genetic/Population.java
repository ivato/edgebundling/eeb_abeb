package edge.bundling.methods.genetic;

import edge.bundling.graph.Bundle;
import edge.bundling.graph.GraphBundledComparable;
import edge.bundling.statistics.GlobalStatistic;
import edge.bundling.graph.Graph;
import edge.bundling.methods.cover.Cover;
import edge.bundling.util.Flags;
import edge.bundling.util.Log;
import edge.bundling.util.Message;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Random;

public class Population {

    //private LinkedList fitnessList = new LinkedList();
    private ArrayList<Individual> individualList = new ArrayList();

    private double sumLocalFitness = 0.0;
    private GlobalStatistic globalData = new GlobalStatistic();

    private int bestFitnessIndividualIndex;
    private int worstFitnessIndividualIndex;
    private Individual previousBestIndividual = new Individual();
    private int convergenceGeneration = 0;

    private double maxFitness = 0;
    private double maxCompatibility = 0;
    private double maxBundles = 0;
    private double minCompatibility = 0;
    private double minBundles = 0;
    private double minFitness = 0;

    private int limitOfTypePopulation;
    
    public int duplicado = 0;
    public int arrumado = 0;

    //*************************************************************************************************************
    /**
     * Gera uma nova população
     *
     * @param graph - grafo base para criar a população
     */
//*************************************************************************************************************                            
    public void initialize(Graph graph) {

       // DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
       // Date dateInitial = new Date();
       // System.out.println("Início NoCover " + dateFormat.format(dateInitial));
        
        this.initializeWithNoCover(graph);  
        //System.out.println("No cover: " + this.getSize());
        
       // dateInitial = new Date();
       // System.out.println("Fim NoCover e Inicio WithCover " + dateFormat.format(dateInitial));
        
        this.initializeWithCover(graph);
        //System.out.println("With cover: " + this.getSize());
        
        //dateInitial = new Date();
       // System.out.println("Fim WithCover e Inicio SetGlobal " + dateFormat.format(dateInitial));
        
       
        //System.out.println(this.individualList.size());
        
        setGlobal();
        
        //dateInitial = new Date();
        //System.out.println("Fim inicializacao " + dateFormat.format(dateInitial));
       // System.out.println("Duplicado " + duplicado);
        //System.out.println("Populacao gerada");
    }

    //*************************************************************************************************************
    /**
     * Seta o melhor e o pior fitness global da população
     *
     */
//*************************************************************************************************************  
    public void setGlobal() {
        globalData.setBestGlobalFitness((Double) this.getBestSolution().getFitness());
        globalData.setWorstGlobalFitness((Double) this.getWorstSolution().getFitness());
    }

    //*************************************************************************************************************
    /**
     * Responsável por criar a nova população a partir de um grafo sem usar
     * cobertura
     *
     * @param graph - grafo base para criar a população
     *
     * @return individual - lista de indivíduos
     */
//*************************************************************************************************************                            
    private void initializeWithNoCover(Graph graph) {
        Individual individual;
       
        Mutation mutation = new Mutation();

        String msgLog = "";
        Log log = new Log();
        if (Flags.isAUTOMATIC_SAVING()) {
            log.create("InitialPopulationNoCover.txt");
        }

        try {
            if (Flags.isAUTOMATIC_SAVING()) {
                msgLog += "WithNoCoverAndRandom\n";
            }

            //Representa a quantidade de indivídios a serem criados
            //Como o algoritmo não permite indivíduos repetidos podem não ser gereados indivíduos na quantidade estipulada na flag
            //Outro aspecto é que o tamanho do grafo influencia a quantidade de indivíduos, pode não existir a quantidade de combinações
            //possível de vértices o que gerará uma quantidade menor de indivíduos do que o estipulado na flag         
            limitOfTypePopulation = 0;
            while ((this.getSize() < (Flags.TAM_POPULATION()/2)) && (limitOfTypePopulation < (Flags.TAM_POPULATION()*10 ))){
                limitOfTypePopulation++;
                
                individual = new Individual(false);
                individual.createBundleWithoutCoverAndRandom(graph);
                individual.evaluate(this);
                
                //Verifica se a solução com mesmo fitness já existe
                if (!checkIfAlreadySaved(individual)){
                    this.add(individual);
                    if (Flags.isAUTOMATIC_SAVING()) {
                        msgLog += Message.individialDataFile(individual, false, "");
                    }
                } else {
                    duplicado++;
                    //Se uma solução com mesmo fitness já existe
                    //Insere uma versão mutada da melhor solução da população
                    Individual individualMutated = new Individual();
                    individualMutated = this.getBestSolution().clone();
                    mutation.joinMutation(individualMutated);
                   
                    individualMutated.evaluate(this);
                    
                    if (!checkIfAlreadySaved(individualMutated)){
                        arrumado++;
                        this.add(individualMutated);
                        if (Flags.isAUTOMATIC_SAVING()) {
                            msgLog += Message.individialDataFile(individualMutated, true, "");
                        }
                    }else{
                        duplicado++;
                    }
                }
            }

            if (Flags.isAUTOMATIC_SAVING()) {
                msgLog += "Poputation Fitness                    : " + getSumLocalFitness() + "\n\n";
                log.save(msgLog);
                log.close();
            }

        } catch (Exception e) {
            System.out.println("Erro na inicialização: initializeWithNoCover()");
        }
    }

    //*************************************************************************************************************
    /**
     * Responsável por criar a nova população a partir de uma cobertura
     *
     * @param graph - grafo base para criar a população
     *
     * @return individual - lista de indivíduos
     */
//*************************************************************************************************************    
    private void initializeWithCover(Graph graph) {
        Individual individual;

        Mutation mutation = new Mutation();

        String msgLog = "";
        Log log = new Log();
        if (Flags.isAUTOMATIC_SAVING()) {
            log.create("InitialPopulationWithCover.txt");
        }

        try {
            if (Flags.isAUTOMATIC_SAVING()) {
                msgLog += "WithSimilarityAndRandom\n";
            }

            limitOfTypePopulation = 0;
            while ((this.getSize() < Flags.TAM_POPULATION()) && (limitOfTypePopulation < (Flags.TAM_POPULATION()*10 ))){
                limitOfTypePopulation++;
                
                individual = new Individual(false);
                individual.createBundleWithCoverAndRandom(graph);
                individual.evaluate(this);
           
                //Verifica se a solução com mesmo fitness já existe
                if (!checkIfAlreadySaved(individual)){
                    this.add(individual);                  
                    if (Flags.isAUTOMATIC_SAVING()) {
                        msgLog += Message.individialDataFile(individual, true, "");
                    }
                } else {
                     duplicado++;
                    //Se uma solução com mesmo fitness já existe
                    //Insere uma versão mutada da melhor solução da população
                    Individual individualMutated = new Individual();
                    individualMutated = this.getBestSolution().clone();
                    mutation.joinMutation(individualMutated);
                    individualMutated.evaluate(this);

                    if (!checkIfAlreadySaved(individualMutated)){
                        arrumado++;
                        this.add(individualMutated);
                        if (Flags.isAUTOMATIC_SAVING()) {
                            msgLog += Message.individialDataFile(individualMutated, true, "");
                        }
                    }else{
                         duplicado++;
                    }
                }
            }

            if (Flags.isAUTOMATIC_SAVING()) {
                msgLog += "Poputation Fitness                    : " + getSumLocalFitness() + "\n\n";
                log.save(msgLog);
                log.close();
            }

        } catch (Exception e) {
            System.out.println("Erro na inicialização: initializeWithCover()");
        }
    }

    //*************************************************************************************************************
    /**
     * Responsável por checar se uma solução o fitness passado já existe na
     * população
     *
     * @param individual - indivíduo a ser testado
     *
     * @return boolean
     */
//*************************************************************************************************************  
    private boolean checkIfAlreadySaved(Individual individual) {    
        ListIterator<Individual> listIterator = this.getIndividuals().listIterator();
        while (listIterator.hasNext()) {
            Individual individualSaved = (Individual) listIterator.next();
            if (individualSaved.getBundleList().size() != individual.getBundleList().size()){
                continue;
            }else{
               if (individualSaved.getFitness() != individual.getFitness()){
                    continue;
                }else{
                  if (individualSaved.checksum() != individual.checksum()){
                      continue;
                  }else{
                      return true;
                  }
               }
            }
        }      
        return false;
    }

    //*************************************************************************************************************
    /**
     * Atualiza a compatibilidade, fitness e número de feixes máximos e mínimos
     * da população
     *
     * @param individual - indivíduo a ser comparado
     *
     */
//*************************************************************************************************************  
    public void setMaxMinValues(Individual individual) {
        if (this.getSize() > 0) {
            if (individual.getCompatibility() >= getMaxCompatibility()) {
                maxCompatibility = individual.getCompatibility();
            }

            if (individual.getCompatibility() < getMinCompatibility()) {
                minCompatibility = individual.getCompatibility();
            }

            if (individual.getNumberOfBundles() >= getMaxBundles()) {
                maxBundles = individual.getNumberOfBundles();
            }

            if (individual.getNumberOfBundles() < getMinBundles()) {
                minBundles = individual.getNumberOfBundles();
            }

            if (individual.getFitness() >= getMaxFitness()) {
                setMaxFitness(individual.getFitness());
            }

            if (individual.getFitness() < getMinFitness()) {
                setMinFitness(individual.getFitness());
            }
        } else {
            this.maxCompatibility = individual.getCompatibility();
            this.maxBundles = individual.getNumberOfBundles();
            this.minCompatibility = individual.getCompatibility();
            this.minBundles = individual.getNumberOfBundles();
            this.setMaxFitness(individual.getFitness());
            this.setMinFitness(individual.getFitness());
        }
    }

    //*************************************************************************************************************
    /**
     * Responsável por adicionar um novo indivíduo na população
     *
     * @param individual - indivíduo a ser adicionado
     */
//*************************************************************************************************************                           
    public void add(Individual individual) {
        
        Collections.shuffle(individual.getBundleList());
        
        individualList.add(individual);
        setSumLocalFitness(sumLocalFitness + individual.getFitness());

        globalData.setSumGlobalFitness(globalData.getSumGlobalFitness() + individual.getFitness());
        globalData.setQuantityGlobalIndividuals(globalData.getQuantityGlobalIndividuals() + 1);
        Double temp = globalData.getBestGlobalFitness();
        if ((temp != null) && (globalData.getBestGlobalFitness() < individual.getFitness())) {
            globalData.setBestGlobalFitness((Double) individual.getFitness());
        }
        temp = globalData.getWorstGlobalFitness();
        if ((temp != null) && (globalData.getWorstGlobalFitness() > individual.getFitness())) {
            globalData.setWorstGlobalFitness((Double) individual.getFitness());
        }
    }

    //*************************************************************************************************************
    /**
     * Responsável por remover um novo indivíduo na população
     *
     * @param individual - indivíduo a ser removido
     */
//*************************************************************************************************************                           
    public boolean remove(Individual individual) {
        if (individualList.contains(individual)) {
            setSumLocalFitness(sumLocalFitness - individual.getFitness());
            individualList.remove(individual);
            return true;
        }
        return false;
    }

    //*************************************************************************************************************
    /**
     * Remove um indivíduo aleatório e insere o melhor da população anterior caso não tenha havido melhora
     *
     */
//*************************************************************************************************************                           
    public void addPreviousBestIndividual() {
        //Ordena os indivíduos da população
        Collections.sort(individualList, new GraphBundledComparable());

        //Recupera o melhor indivíduo da população 
        Individual bestIndividual = individualList.get(0);
        
        //Remove um indivíduo aleatório e insere o melhor da população anterior
        //if (bestIndividual.getFitness() != previousBestIndividual.getFitness()) {
        if (bestIndividual.getFitness() < previousBestIndividual.getFitness()) {
            //Recupera um indivíduo aleatório
            Random rndSeed = new Random(System.currentTimeMillis());
            int seed = rndSeed.nextInt(18400791);
            Random rnd = new Random(seed);
            int index = (int) rnd.nextInt(individualList.size() - 1);
        
            Individual randomIndividual = individualList.get(index);
            this.remove(randomIndividual);
            this.add(previousBestIndividual);
        }
    }

    //*************************************************************************************************************
    /**
     * Calcula o ranking dos indivíduos baseado no fitness
     */
//*************************************************************************************************************    
    public void ranking() {
        //Reinicia o ranking 
        for (int i = 0; i < this.getSize(); i++) {
            this.getIndividual(i).setFitnessRank(-1);
        }

        //Calcula o novo ranking dos indivíduso
        for (int i = 0; i < this.getSize(); i++) {
            this.getIndividual(i).setFitnessRank(this.getFitnessRank(this.getIndividual(i).getFitness()));
        }

        //Quando o ranking do melhor e pior indivíduo da população
        for (int i = 0; i < this.getSize(); i++) {
            if (this.getIndividual(i).getFitnessRank() == 0) {
                this.bestFitnessIndividualIndex = i;
            }
            if (this.getIndividual(i).getFitnessRank() == this.getSize() - 1) {
                this.worstFitnessIndividualIndex = i;
            }
        }
    }

    //*************************************************************************************************************
    /**
     * Verifica se a população já convergiu
     *
     * @return boolean
     */
//*************************************************************************************************************    
    public boolean convergence(int generations) {
        if (Flags.isTERMINATION_BY_DIVERSITY()) {
            if (previousBestIndividual.getFitness() < this.getBestSolution().getFitness()) {
                this.convergenceGeneration = generations;
                return false;
            }
        }

        return true;
    }

//*************************************************************************************************************
    /**
     * Devolve o rannking de um valor de fitness
     *
     * @param fitness - fitness a ser pesquisado
     * @return ranking - raking do fitness
     */
    //*************************************************************************************************************    
    public int getFitnessRank(double fitness) {

        int ranking = -1;
        boolean firstOccur = true;

        for (int i = 0; i < this.getSize(); i++) {
            if (fitness > this.getIndividual(i).getFitness()) {
                ranking++;
            } else if (fitness == this.getIndividual(i).getFitness()) {

                //Assegurar ranking diferente para indivíduos com mesmo fitness
                if (this.getIndividual(i).getFitnessRank() > -1) {
                    ranking++;
                } else if (this.getIndividual(i).getFitnessRank() == -1) {
                    if (firstOccur) {
                        ranking++;
                        firstOccur = false;
                    }
                }
            }
        }
        return (ranking);
    }
   
//*************************************************************************************************************
    /**
     * Responsável por devolver o indivíduo com melhor fitness
     *
     * @param edgeList - lista de arestas do grafo
     * @return bestIndividual - melhor indivíduo da população
     */
//*************************************************************************************************************    
    public Individual getBestSolution() {
        Collections.sort(individualList, new GraphBundledComparable());
        Individual bestIndividual = individualList.get(0);
        return bestIndividual;
    }

    //*************************************************************************************************************
    /**
     * Responsável por devolver o indivíduo com pior fitness
     *
     * @param edgeList - lista de arestas do grafo
     * @return bestIndividual - melhor indivíduo da população
     */
//*************************************************************************************************************    
    public Individual getWorstSolution() {
        Collections.sort(individualList, new GraphBundledComparable());
        Individual worstIndividual = individualList.get(individualList.size() - 1);
        return worstIndividual;
    }

    public Population clone() {
        Population clone = new Population();

        ListIterator<Individual> listIterator = this.getIndividuals().listIterator();
        while (listIterator.hasNext()) {
            Individual individual = (Individual) listIterator.next().clone();
            clone.add(individual);
        }

        //ListIterator listIterator2 = this.fitnessList.listIterator();
        //while (listIterator2.hasNext()) {
        //    cloneBundlesAndAttributs.fitnessList.add((double) listIterator2.next());
        //}

        clone.setSumLocalFitness(this.sumLocalFitness);
        clone.maxFitness = this.maxFitness;
        clone.maxCompatibility = this.maxCompatibility;
        clone.maxBundles = this.maxBundles;
        clone.minCompatibility = this.minCompatibility;
        clone.minBundles = this.minBundles;
        clone.minFitness = this.minFitness;
        clone.limitOfTypePopulation = this.limitOfTypePopulation;
        clone.bestFitnessIndividualIndex = this.bestFitnessIndividualIndex;
        clone.worstFitnessIndividualIndex = this.worstFitnessIndividualIndex;
        clone.previousBestIndividual = this.previousBestIndividual.clone();
        clone.convergenceGeneration = this.convergenceGeneration;
        //clone.globalData = new GlobalStatistic();
        //clone.localData = new LocalStatistic();
        return clone;

    }

    //*************************************************************************************************************
    /**
     * Mostra a população, gerando também um arquivo texto
     *
     * @return listWithPopulation - lista com a população
     */
//*************************************************************************************************************        
    public String showPopulation() {
        int k = 1;
        String listWithPopulation = "\n";
        ListIterator<Individual> listIterator = this.individualList.listIterator();
        while (listIterator.hasNext()) {
            Individual individual = listIterator.next();
            listWithPopulation += "Indivíduo " + k + "\n";
            listWithPopulation += individual.showBundles();
            listWithPopulation += "Fitness: " + individual.getFitness() + "\n";
            listWithPopulation += "Size of population: " + this.getSize() + "\n\n";
            k++;
        }
        listWithPopulation += "\n";
        return listWithPopulation;
    }

    /**
     * @return individualList
     */
    public ArrayList<Individual> getIndividuals() {
        return individualList;
    }

    /**
     * @return the sumLocalFitness
     */
    public double getSumLocalFitness() {
        return sumLocalFitness;
    }

    /**
     * @return the globalData
     */
    public GlobalStatistic getGlobalData() {
        return globalData;
    }

    /**
     * @return the size
     */
    public int getSize() {
        return individualList.size();
    }

    /**
     * @return the individual
     */
    public Individual getIndividual(int i) {
        return individualList.get(i);
    }

    /**
     * @return the maxCompatibility
     */
    public double getMaxCompatibility() {
        return maxCompatibility;
    }

    /**
     * @return the maxBundles
     */
    public double getMaxBundles() {
        return maxBundles;
    }

    /**
     * @return the minCompatibility
     */
    public double getMinCompatibility() {
        return minCompatibility;
    }

    /**
     * @return the minBundles
     */
    public double getMinBundles() {
        return minBundles;
    }

    /**
     * @return the maxFitness
     */
    public double getMaxFitness() {
        return maxFitness;
    }

    /**
     * @param maxFitness the maxFitness to set
     */
    public void setMaxFitness(double maxFitness) {
        this.maxFitness = maxFitness;
    }

    /**
     * @return the minFitness
     */
    public double getMinFitness() {
        return minFitness;
    }

    /**
     * @param minFitness the minFitness to set
     */
    public void setMinFitness(double minFitness) {
        this.minFitness = minFitness;
    }

    /**
     * @param sumLocalFitness the sumLocalFitness to set
     */
    public void setSumLocalFitness(double sumLocalFitness) {
        this.sumLocalFitness = sumLocalFitness;
    }

    /**
     * @return the previousBestIndividual
     */
    public Individual getPreviousBestIndividual() {
        return previousBestIndividual;
    }

    /**
     * @param previousBestIndividual the previousBestIndividual to set
     */
    public void savePreviousBestIndividual(Individual previousBestIndividual) {
        this.previousBestIndividual = previousBestIndividual;
    }

    /**
     * @return the convergenceGeneration
     */
    public int getConvergenceGeneration() {
        return convergenceGeneration;
    }

    /**
     * @param convergenceGeneration the convergenceGeneration to set
     */
    public void setConvergenceGeneration(int convergenceGeneration) {
        this.convergenceGeneration = convergenceGeneration;
    }

}
