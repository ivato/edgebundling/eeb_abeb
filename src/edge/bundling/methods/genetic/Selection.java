package edge.bundling.methods.genetic;

import edge.bundling.util.Flags;
import edge.bundling.util.Log;
import edge.bundling.util.Message;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import edge.bundling.util.Utils;

public class Selection {

    //RandomGenerator randomNumber = null;
    private int poolsize;
    Random rndSeed;

    public Selection(int val) {
        super();
        // randomNumber = new RandomGenerator();
        this.poolsize = val;
    }

    public ArrayList<Individual> selection(Population population, int i) {
        rndSeed = new Random(System.currentTimeMillis() * i);

        Log log = new Log();
        String msgLog = "";
        if (Flags.isAUTOMATIC_SAVING()) {
            log.create("EvolutionProcess.txt", true);
        }

        ArrayList<Individual> individuals = null;
        try {
            if (Flags.isRANKING()) {
                individuals = rankingSelection(population, poolsize);
                if (Flags.isAUTOMATIC_SAVING()) {
                    msgLog += "\nSELEÇÃO: ranking";
                }
            } else if (Flags.isROULETTE_WHEEL()) {
                individuals = rouletteWheelSelection(population, poolsize);
                if (Flags.isAUTOMATIC_SAVING()) {
                    msgLog += "\nSELEÇÃO: rouletteWheel";
                }
            } else if (Flags.isTOURNAMENT()) {
                individuals = tournamentSelection(population, poolsize);
                if (Flags.isAUTOMATIC_SAVING()) {
                    msgLog += "\nSELEÇÃO: tournament";
                }
            } else if (Flags.isRANDOM()) {
                individuals = randomSelection(population, poolsize);
                if (Flags.isAUTOMATIC_SAVING()) {
                    msgLog += "\nSELEÇÃO: random";
                }
            }
        } catch (Exception e) {
            System.out.println("Erro na seleção.");
        }

        if (Flags.isAUTOMATIC_SAVING()) {
            msgLog += Message.individialDataFile(individuals.get(0), false, "Pai 1");
            msgLog += Message.individialDataFile(individuals.get(1), false, "Pai 2");

            log.save(msgLog);
            log.close();
        }

        return individuals;
    }

    //*************************************************************************************************************
    /**
     * Executa a seleção por ranking em um problema de maximização Usa o
     * fitnessRank para seleção
     *
     * @param population - população de onde serão selecionados os indivíduos
     * @param poolSize - número de indivíduos a serem selecionados
     * @return individualList - lista com os indivíduos
     */
//*************************************************************************************************************     
    public ArrayList<Individual> rankingSelection(Population population, int poolSize) {
        int populationDim = population.getSize();
        ArrayList<Individual> individuals = new ArrayList<Individual>();

        double[] individualFitness = new double[populationDim];

        //Atualiza o ranking dos indivíduos
        population.ranking();

        double selectivePressure = Flags.SELECTIVE_PRESSURE();

        //Linear ranking
        for (int i = 0; i < populationDim; i++) {
            int rankPosition = population.getIndividual(i).getFitnessRank();
            double A = (double) 2 * (selectivePressure - 1);
            double B = (double) (rankPosition - 1) / (populationDim - 1);
            individualFitness[i] = 2 - selectivePressure + (A * B);

        }

        /* Executa o core da seleção por Roleta */
        int[] indexSelectedIndividuals = runRouletteWheel(individualFitness, poolSize);

        for (int i = 0; i < poolSize; i++) {
            Individual newIndividual = population.getIndividual(indexSelectedIndividuals[i]);
            individuals.add(newIndividual);
        }

        return individuals;
    }

//*************************************************************************************************************
    /**
     * Executa a seleção por roleta em um problema de maximização Usa o fitness
     * para seleção
     *
     * @param population - população de onde serão selecionados os indivíduos
     * @param poolSize - número de indivíduos a serem selecionados
     * @return individualList - lista com os indivíduos
     */
    //*************************************************************************************************************     
    public ArrayList<Individual> rouletteWheelSelection(Population population, int poolSize) {
        int populationDim = population.getSize();
        ArrayList<Individual> individuals = new ArrayList<Individual>();

        double[] individualFitness = new double[populationDim];

        for (int i = 0; i < populationDim; i++) {
            individualFitness[i] = population.getIndividual(i).getFitness();
        }

        /* Escalona o fitness para um problema de maximização */
        double maxFitness = population.getBestSolution().getFitness();
        Scaling.scalingForMaximization(individualFitness, maxFitness);

        /* Executa o core da seleção por Roleta */
        int[] indexSelectedIndividuals = runRouletteWheel(individualFitness, poolSize);

        for (int i = 0; i < poolSize; i++) {
            Individual newIndividual = population.getIndividual(indexSelectedIndividuals[i]);
            individuals.add(newIndividual);
        }

        return individuals;
    }

//*************************************************************************************************************     
    /**
     * Executa o procedimento de simulação da roleta rodando
     *
     * @param fitnessOfIndividuals - fitness escalonado
     * @param poolSize - número de indivíduos a serem selecionados
     * @return indice dos indivídios selecionados
     */
    //************************************************************************************************************* 
    public int[] runRouletteWheel(double[] fitnessOfIndividuals, int poolSize) {
        double sumOfFitness = 0;
        int populationDim = fitnessOfIndividuals.length;
        double[] rndPoints = new double[poolSize];

        //int a = RandomNumber.getRandomInt(19700621, 59700621);
        int a = Utils.getRandomInt(19700621, 59700621);

        int seed = rndSeed.nextInt(a);
        Random rnd = new Random(seed);

        //Guarda o índice dos indivíduos selecionados
        int[] indexSelectedIndividuals = new int[poolSize];

        //Soma o fitness de todos os indivíduos
        for (int i = 0; i < populationDim; i++) {
            sumOfFitness += fitnessOfIndividuals[i];
        }

        //Seleciona um valor aleatório para cada indivíduo
        for (int i = 0; i < poolSize; i++) {
            rndPoints[i] = (double) rnd.nextDouble() * sumOfFitness;
        }

        Arrays.sort(rndPoints);

        //Roda a roleta
        int selPos = 0;
        int indPos = 0;
        double accumS = 0;

        /* Shuffle the array */
        //Permutation permutation = new Permutation();
        int[] perm = Utils.getPermutation(poolSize);

        try {
            while (selPos < poolSize) {
                //Se o ponto rndPoints[selPos] esta dentro do seguimento corrente 
                // de indivíduos[indPos] 
                if (accumS + fitnessOfIndividuals[indPos] > rndPoints[selPos]) {
                    indexSelectedIndividuals[perm[selPos] - 1] = indPos;
                    selPos++;
                } else {
                    accumS += fitnessOfIndividuals[indPos];
                    indPos++;
                }
            }
        } catch (Exception e) {
            System.out.println("Current scaled fitnesses: ");
            for (int i = 0; i < populationDim; i++) {
                System.out.print(fitnessOfIndividuals[i] + ", ");
            }
            System.out.println();
            System.out.println("IndPos: " + indPos + ", SelPos: " + selPos);
            System.out.println("ERROR in RouletteWheel: " + e.getMessage());
            System.exit(0);
        }

        return indexSelectedIndividuals;
    }

//*************************************************************************************************************
    /**
     * Executa a seleção por torneio em um problema de maximização Usa o fitness
     * para seleção sem escalonamento
     *
     * @param population - população de onde serão selecionados os indivíduos
     * @param poolSize - número de indivíduos a serem selecionados
     * @return individualList - lista com os indivíduos
     */
//*************************************************************************************************************     
    public ArrayList<Individual> tournamentSelection(Population population, int poolSize) {
        int populationDim = population.getSize();
        ArrayList<Individual> individuals = new ArrayList<Individual>();

        double[] individualFitness = new double[populationDim];

        for (int i = 0; i < populationDim; i++) {
            individualFitness[i] = population.getIndividual(i).getFitness();
        }

        //Executa o torneio
        int[] indexSelectedIndividuals = runTournament(individualFitness, poolSize);

        for (int i = 0; i < poolSize; i++) {
            Individual newIndividual = population.getIndividual(indexSelectedIndividuals[i]);
            individuals.add(newIndividual);
        }

        return individuals;
    }

//*************************************************************************************************************     
    /**
     * Executa o procedimento de simulação o torneio
     *
     * @param fitnessOfIndividuals - fitness escalonado
     * @param poolSize - número de indivíduos a serem selecionados
     * @return indice dos indivídios selecionados
     */
    //************************************************************************************************************* 
    public int[] runTournament(double[] fitnessOfIndividuals, int poolSize) {
        int populationDim = fitnessOfIndividuals.length;

        int tourSize = Flags.TOURNAMENT_TOUR();

        int[] indexIndividualsSelected = new int[poolSize];
        double[] fitnessCompetitors = new double[tourSize];
        int[] indexes = new int[tourSize];

        int seed = rndSeed.nextInt(18400791);
        Random rnd = new Random(seed);

        for (int i = 0; i < poolSize; i++) {

            // Escolhe tourSize competidores 
            for (int j = 0; j < tourSize; j++) {
                int index = (int) rnd.nextInt(populationDim);
                indexes[j] = index;
                fitnessCompetitors[j] = fitnessOfIndividuals[index];
            }

            //Seleciona o maior vencedor
            Utils.bubbleSort(fitnessCompetitors, indexes);
            indexIndividualsSelected[i] = indexes[0];
        }
        return indexIndividualsSelected;
    }

//*************************************************************************************************************
    /**
     * Executa a seleção aleatória em um problema de maximização Usa o fitness
     * para seleção sem escalonamento
     *
     * @param population - população de onde serão selecionados os indivíduos
     * @param poolSize - número de indivíduos a serem selecionados
     * @return individualList - lista com os indivíduos
     */
//*************************************************************************************************************     
    public ArrayList<Individual> randomSelection(Population population, int poolSize) {
        int populationDim = population.getSize();
        ArrayList<Individual> individuals = new ArrayList<Individual>();

        int seed = rndSeed.nextInt(18400791);
        Random rnd = new Random(seed);

        for (int i = 0; i < poolSize; i++) {
            int index = (int) rnd.nextInt(populationDim);
            Individual newIndividual = population.getIndividual(index);
            individuals.add(newIndividual);
        }
        return individuals;
    }

//*************************************************************************************************************
    /**
     * Seleciona os k melhores indivíduos Usa o fitness para seleção sem
     * escalonamento
     *
     * @param population - população de onde serão selecionados os indivíduos
     * @param poolSize - número de indivíduos a serem selecionados
     * @return individualList - lista com os indivíduos
     */
//*************************************************************************************************************     
    public int[] kBestSelection(Population population, int poolSize) {
        int populationDim = population.getSize();
        ArrayList<Individual> individuals = new ArrayList<Individual>();

        double[] individualFitness = new double[populationDim];

        for (int i = 0; i < populationDim; i++) {
            individualFitness[i] = population.getIndividual(i).getFitness();
        }

        /* Escalona o fitness para um problema de maximização */
        double maxFitness = population.getBestSolution().getFitness();
        Scaling.scalingForMaximization(individualFitness, maxFitness);

        //Indice do dos indivíduos selecionados 
        int[] indexIndividualsSelected = new int[poolSize];
        int[] ordIndex = new int[populationDim];
        for (int i = 0; i < populationDim; i++) {
            ordIndex[i] = i;
        }

        // The higest, the better. Sort in descending order (highest to lowest) 
        Utils.bubbleSort(individualFitness, ordIndex);

        //Permutation permGen = new Permutation();
        int[] perm = Utils.getPermutation(poolSize);
        for (int i = 0; i < poolSize; i++) {
            indexIndividualsSelected[perm[i] - 1] = ordIndex[i];
        }
        return indexIndividualsSelected;
    }

    //*************************************************************************************************************
    /**
     * Seleciona dois indivíduos da população
     *
     * @param populacao - populacao de onde serão selecionados os indivíduos
     * @return individualList - lista com dois indivíduos
     */
//*************************************************************************************************************     
    public ArrayList<Individual> roulette(Population population) {

        ArrayList<Individual> individualList = new ArrayList();

        Individual individual1 = runRoulette(population);
        individualList.add(individual1);

        Individual individual2;
        int i = 1;
        do {
            individual2 = runRoulette(population);
            i++;
        } while ((individual1 == individual2) && (i <= Flags.ROULETTE_INTERACTION()));

        if (individual1.getFitness() == individual2.getFitness()) {

            //Random generator = new Random();
            int seed = rndSeed.nextInt(19400531);
            Random rnd = new Random(seed);
            int index = (int) rnd.nextInt(population.getIndividuals().size() - 1);

            individual2 = (Individual) population.getIndividuals().get(index);
        }

        individualList.add(individual2);

        return individualList;
    }

    //*************************************************************************************************************
    /**
     * Roda a roleta e seleciona um individuo
     *
     * @param populacao - populacao de onde serão selecionados os indivíduos
     * @return individual - indivíduo selecionado
     */
//*************************************************************************************************************     
    private Individual runRoulette(Population population) {

        ListIterator<Individual> listIterator = population.getIndividuals().listIterator();

        int seed = rndSeed.nextInt(24400531);
        Random rnd = new Random(seed);

        double populationFitness = population.getSumLocalFitness();
        double rouletteWheelPosition = (double) rnd.nextDouble() * populationFitness;

        double spinWheel = 0;
        while (listIterator.hasNext()) {

            Individual individual = listIterator.next();

            spinWheel += individual.getFitness();
            if (spinWheel >= rouletteWheelPosition) {
                return individual;
            }
        }
        return population.getIndividuals().get(population.getIndividuals().size() - 1);
    }

    //*************************************************************************************************************
    /**
     * Retorna dois indivíduos usando o método do torneio
     *
     * @param populacao - populacao de onde serão selecionados os indivíduos
     * @return individual - indivíduo selecionado
     */
//*************************************************************************************************************     
    protected ArrayList<Individual> runTournament(Population population, int i_tournamentSize) {
        ArrayList<Individual> parents = new ArrayList<Individual>();
        List gladiators = new ArrayList(i_tournamentSize);

        int seed = rndSeed.nextInt(14800531);
        Random rnd = new Random(seed);

        //Pick a random spot within the population
        int startIndex = (int) Math.floor(rnd.nextDouble() * ((Flags.TAM_POPULATION() - i_tournamentSize) - 0 + 1)) + 0;

        //Get as many individual as specified by tournament size
        for (int i = startIndex; i < (startIndex + i_tournamentSize); i++) {
            gladiators.add(i - startIndex, population.getIndividuals().get(i));
        }

        //Sort the sub-population by fitness
        Collections.sort(gladiators);

        //Return the best two
        parents.add((Individual) gladiators.get(0));
        parents.add((Individual) gladiators.get(1));

        return parents;
    }
}
