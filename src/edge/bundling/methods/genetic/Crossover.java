package edge.bundling.methods.genetic;

import edge.bundling.graph.Bundle;
import edge.bundling.graph.Edge;
import edge.bundling.graph.Node;
import edge.bundling.util.Flags;
import edge.bundling.util.Log;
import edge.bundling.util.Message;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Random;

public class Crossover {

    //public int contMakeFeasibleCrossover = 0;
    
    //*************************************************************************************************************
    /**
     * Efetua o cruzamento em dois indivíduos
     *
     * @param edgeList - lista de arestas do grafo
     * @param individualList - array com os indivíduos a serem cruzados
     * @return newIndividuals - lista com os dois indivíduos resultantes
     */
//*************************************************************************************************************                           
    public ArrayList<Individual> crossover(ArrayList<Edge> edgeList, ArrayList<Individual> individualList) {
        ArrayList<Individual> sons = new ArrayList<Individual>();
        Individual father = new Individual();
        Individual mother = new Individual();
        
        try {
            int sizeEdgeGraph = edgeList.size();

            Log log = new Log();
            String msgLog = "";
            if (Flags.isAUTOMATIC_SAVING()) {
                log.create("EvolutionProcess.txt", true);
            }


            Individual son = new Individual();
            Individual daughter = new Individual();

            father = individualList.get(0).clone();
            mother = individualList.get(1).clone();

            if (Flags.isAUTOMATIC_SAVING()) {
                msgLog += "\nCRUZAMENTO";
            }

            Random rndSeed = new Random(System.currentTimeMillis());
            int seed = rndSeed.nextInt(19700621);
            Random rnd = new Random(seed);
            float num = (float) rnd.nextFloat();

            if (Flags.CROSSOVER_RATIO() > num) {
                if (Flags.isONE_POINT()) {
                    
                    rnd = new Random(System.currentTimeMillis());
                    int pointChoice = (int) rnd.nextInt(3);
                    
                    if (pointChoice <= 1){
                        sons = onePointCrossover(father, mother, edgeList, sizeEdgeGraph);
                    }else{
                        sons = twoPointCrossover(father, mother, edgeList, sizeEdgeGraph);
                    }
                            
                    
                    if (Flags.isAUTOMATIC_SAVING()) {
                        msgLog += "\n...com cruzamento um ponto...\n";
                    }
                } else {

                    if (Flags.isUNIFORM()) {
                        sons = uniformCrossover(father, mother, sizeEdgeGraph, edgeList);
                        if (Flags.isAUTOMATIC_SAVING()) {
                            msgLog += "\n...com cruzamento Uniforme...\n";
                        }
                    }
                }

            } else {
                if (Flags.isAUTOMATIC_SAVING()) {
                    msgLog += "...sem cruzamento...\n";
                }
                sons.add(individualList.get(0));
                sons.add(individualList.get(1));
            }

            if (Flags.isAUTOMATIC_SAVING()) {
                msgLog += Message.individialDataFile(sons.get(0), false, "Filho 1");
                msgLog += Message.individialDataFile(sons.get(1), false, "Filho 2");

                log.save(msgLog);
                log.close();
            }
        } catch (Exception e) {
            System.out.println("Erro no cruzamento");
            //System.out.println("Parent 1:");
            //System.out.println(father.showBundles());
            //System.out.println("Parent 2:");
            //System.out.println(mother.showBundles());
        }
        return sons;
    }

    //*************************************************************************************************************
    /**
     * Efetua o cruzamento em dois indivíduos usando a abordagem de um ponto
     *
     * @param parent1 - indivíduo 1 a ser cruzado
     * @param parent2 - indivíduo 2 a ser cruzado
     * @param sizeEdgeGraph - Quatidade de arestas dos grafos
     * @param edgeList - lista de arestas do grafo
     * @return sons - os dois indivíduos gerados
     */
//*************************************************************************************************************                           
    private ArrayList<Individual> onePointCrossover(Individual parent1, Individual parent2, ArrayList<Edge> edgeList, int sizeEdgeGraph) {

        ArrayList<Individual> sons = new ArrayList<Individual>();
        Individual son = new Individual();
        Individual daughter = new Individual();

        son = makeCrossing(parent1, parent2, edgeList, sizeEdgeGraph); //Une primeira parte do 1 e segunda do 2
        daughter = makeCrossing(parent2, parent1, edgeList, sizeEdgeGraph); //Une primeira parte do 2 e segunda do 1

        sons.add(son);
        sons.add(daughter);

        return sons;
    }

    
    
    private ArrayList<Individual> twoPointCrossover(Individual parent1, Individual parent2, ArrayList<Edge> edgeList, int sizeEdgeGraph) {

        ArrayList<Individual> sons = new ArrayList<Individual>();
        Individual son = new Individual();
        Individual daughter = new Individual();

        son = makeCrossingTwoPoint(parent1, parent2, edgeList, sizeEdgeGraph); //Une primeira parte do 1 e segunda do 2
        daughter = makeCrossingTwoPoint(parent2, parent1, edgeList, sizeEdgeGraph); //Une primeira parte do 2 e segunda do 1

        sons.add(son);
        sons.add(daughter);

        return sons;
    }
    
    //*************************************************************************************************************
    /**
     * Auxilia a execução do cruzamento de um ponto criando um indivíduo com as
     * partes do pai 1 e pai 2 nesta ordem
     *
     * @param parent1 - indivíduo 1 a ser cruzado
     * @param parent2 - indivíduo 2 a ser cruzado
     * @param sizeEdgeGraph - Quatidade de arestas dos grafos
     * @param edgeList - lista de arestas do grafo
     * @return son - filho gerado
     */
//*************************************************************************************************************                           
    private Individual makeCrossing(Individual parent1, Individual parent2, ArrayList<Edge> edgeList, int sizeEdgeGraph) {

        Individual son = new Individual();
        int[] visited = new int[sizeEdgeGraph + 1];
        int id;

        
        son.setEdgeList(parent1.getEdgeList());
        son.setNodeList(parent1.getNodeList());
        son.setDirected(parent1.getDirected());
        son.setSizeEdge(parent1.getSizeEdge());
        son.setSizeNode(parent1.getSizeNode());
        son.setCoverList(parent1.getCoverList());
        
        //Determina o ponto de corte conforme a quantidade de feixes que os indivíduos     
        int qty1 = parent1.getBundleList().size();
        int qty2 = parent2.getBundleList().size();
        int qty = parent2.getBundleList().size();
        int cutOff;
        
        if (qty1 > qty2) {
            cutOff = (int) qty2 / 2;
        } else {
            cutOff = (int) qty1 / 2;
        }

        //Preenche o vetor de controle responsável por monitorar quais arestas já 
        //pertence a algum feixe, -1 se ainda não esta em nenhum feixe e o id do feixe 
        //caso contrário
        for (int i = 0; i <= sizeEdgeGraph; i++) {
            visited[i] = 0;
        }
        
        //Até o ponto de corte recupera os feixes do primeiro indivíduo e adiciona ao novo
        for (int i = 0; i <= cutOff; i++) {
                    
            Bundle bundle = new Bundle();
            bundle = parent1.getBundle(i).clone();
            son.add(bundle);
             
            //Atualiza o vetor de controle marcado as arestas já usadas
            ListIterator<Edge> listIterator = parent1.getBundle(i).getEdges().listIterator();
            while (listIterator.hasNext()) {
                id = listIterator.next().getId();
                visited[id] = 1;
                visited[visited.length-1] += 1;
            }
        }
         
        //Depois do ponto de corte recupera os feixes do segundo indivíduo e adiciona ao novo
        int idBundle = cutOff + 1;
        for (int i = (cutOff + 1); i < qty; i++) {

            Bundle bundle = new Bundle();
            bundle = parent2.getBundle(i).clone();
            bundle.setId(idBundle);

            //Laço que verificará se as arestas já foram usadas
            //as arestas já usadas serão retiradas deste feixe
            ListIterator<Edge> listIterator = parent2.getBundle(i).getEdges().listIterator();
            while (listIterator.hasNext()) {

                Edge edge = new Edge();
                edge = listIterator.next();
                id = edge.getId();

                //Se a aresta já foi usada em um outro feixe já adicionado ao novo indivíduo
                //retira esta aresta do feixe que será adicionado
                if (visited[id] != 0) {
                    bundle.remove(edge);
                } else {
                    //Marca a aresta no vetor de controle
                    visited[id] = 1;
                    visited[visited.length-1] += 1;
                }
            }           
            
            //Se o feixe não ficou vazio o adiciona ao novo indivído
            if (bundle.getEdges().size() > 0) {              
                Node node = bundle.getCenterNode();
                son.add(bundle);
                idBundle++;
            }
        }
        
        if (visited[visited.length-1] != sizeEdgeGraph){           
            son.makeFeasible();
            //contMakeFeasibleCrossover++;
        }
        
        return son;
    }
    
    
    
     private Individual makeCrossingTwoPoint(Individual parent1, Individual parent2, ArrayList<Edge> edgeList, int sizeEdgeGraph) {

        Individual son = new Individual();
        int[] visited = new int[sizeEdgeGraph + 1];
        int id;

        
        son.setEdgeList(parent1.getEdgeList());
        son.setNodeList(parent1.getNodeList());
        son.setDirected(parent1.getDirected());
        son.setSizeEdge(parent1.getSizeEdge());
        son.setSizeNode(parent1.getSizeNode());
        son.setCoverList(parent1.getCoverList());
        
        //Determina o ponto de corte conforme a quantidade de feixes que os indivíduos     
        int qty1 = parent1.getBundleList().size();
        int qty2 = parent2.getBundleList().size();
        int qty = parent2.getBundleList().size();
        int cutOff, cutOff2;
        
        if (qty1 > qty2) {
            cutOff  = (int) qty2 / 3;
            cutOff2 = (int) cutOff * 2;
        } else {
            cutOff = (int) qty1 / 3;
            cutOff2 = (int) cutOff * 2;
        }

        //Preenche o vetor de controle responsável por monitorar quais arestas já 
        //pertence a algum feixe, -1 se ainda não esta em nenhum feixe e o id do feixe 
        //caso contrário
        for (int i = 0; i <= sizeEdgeGraph; i++) {
            visited[i] = 0;
        }
            
        //Até o ponto de corte recupera os feixes do primeiro indivíduo e adiciona ao novo
        for (int i = 0; i <= cutOff; i++) {
                    
            Bundle bundle = new Bundle();
            bundle = parent1.getBundle(i).clone();
            son.add(bundle);
             
            //Atualiza o vetor de controle marcado as arestas já usadas
            ListIterator<Edge> listIterator = parent1.getBundle(i).getEdges().listIterator();
            while (listIterator.hasNext()) {
                id = listIterator.next().getId();
                visited[id] = 1;
                visited[visited.length-1] += 1;
            }
        }
        
        //Depois do ponto de corte recupera os feixes do segundo indivíduo e adiciona ao novo
        int idBundle = cutOff + 1;
        for (int i = (cutOff + 1); i <= cutOff2; i++) {

            Bundle bundle = new Bundle();
            bundle = parent2.getBundle(i).clone();
            bundle.setId(idBundle);

            //Laço que verificará se as arestas já foram usadas
            //as arestas já usadas serão retiradas deste feixe
            ListIterator<Edge> listIterator = parent2.getBundle(i).getEdges().listIterator();
            while (listIterator.hasNext()) {

                Edge edge = new Edge();
                edge = listIterator.next();
                id = edge.getId();

                //Se a aresta já foi usada em um outro feixe já adicionado ao novo indivíduo
                //retira esta aresta do feixe que será adicionado
                if (visited[id] != 0) {
                    bundle.remove(edge);
                } else {
                    //Marca a aresta no vetor de controle
                    visited[id] = 1;
                    visited[visited.length-1] += 1;
                }
            }           
            
            //Se o feixe não ficou vazio o adiciona ao novo indivído
            if (bundle.getEdges().size() > 0) {              
                Node node = bundle.getCenterNode();
                son.add(bundle);
                idBundle++;
            }
        }
         
        idBundle = cutOff2 + 1;
        for (int i = (cutOff2 + 1); i < qty1; i++) {

            Bundle bundle = new Bundle();
            bundle = parent1.getBundle(i).clone();
            bundle.setId(idBundle);

            //Laço que verificará se as arestas já foram usadas
            //as arestas já usadas serão retiradas deste feixe
            ListIterator<Edge> listIterator = parent1.getBundle(i).getEdges().listIterator();
            while (listIterator.hasNext()) {

                Edge edge = new Edge();
                edge = listIterator.next();
                id = edge.getId();

                //Se a aresta já foi usada em um outro feixe já adicionado ao novo indivíduo
                //retira esta aresta do feixe que será adicionado
                if (visited[id] != 0) {
                    bundle.remove(edge);
                } else {
                    //Marca a aresta no vetor de controle
                    visited[id] = 1;
                    visited[visited.length-1] += 1;
                }
            }           
            
            //Se o feixe não ficou vazio o adiciona ao novo indivído
            if (bundle.getEdges().size() > 0) {              
                Node node = bundle.getCenterNode();
                son.add(bundle);
                idBundle++;
            }
        }
        
        if (visited[visited.length-1] != sizeEdgeGraph){
            son.makeFeasible();
            //contMakeFeasibleCrossover++;
        }
        
        return son;
    }
    
    
    
    //*************************************************************************************************************
    /**
     * Efetua o cruzamento em dois indivíduos usando a abordagem uniforme
     *
     * @param individual1 - indivíduo 1 a ser cruzado
     * @param individual2 - indivíduo 2 a ser cruzado
     * @param sizeEdgeGraph - Quatidade de arestas dos grafos
     * @param edgeList - lista de arestas do grafo
     * @return sons - os dois indivíduos gerados
     */
//*************************************************************************************************************                           
    private ArrayList<Individual> uniformCrossover(Individual parent1, Individual parent2, int sizeEdgeGraph, ArrayList<Edge> edgeList) {
        ArrayList<Individual> sons = new ArrayList<Individual>();
        boolean[] mask;
        try {

            int size = 0;
            Individual son = new Individual();
            Individual daughter = new Individual();
            Bundle bundleParent1;
            Bundle bundleParent2;
            int bundleIndexSon = 0;
            int bundleIndexDaugther = 0;

            //Encontra o tamanho do menor indivíduo
            if (parent1.getNumberOfBundles() < parent2.getNumberOfBundles()) {
                size = parent1.getNumberOfBundles();
            } else {
                size = parent2.getNumberOfBundles();
            }

            Random rndSeed = new Random(System.currentTimeMillis());

            float num;
            mask = new boolean[size];
            for (int i = 0; i < size; i++) {

                int seed = rndSeed.nextInt(19700621);
                Random rnd = new Random(seed);
                num = (float) rnd.nextFloat();

                if (Flags.CROSSOVER_RATIO() > num) {
                    mask[i] = true;
                } else {
                    mask[i] = false;
                }
            }

            //Insere os feixes em cada indivíduo conforme probabilidade e tamanho do menor indivíduo
            for (int i = 0; i < size; i++) {
                bundleParent1 = new Bundle();
                bundleParent2 = new Bundle();
                bundleParent1 = parent1.getBundle(i).clone();
                bundleParent2 = parent2.getBundle(i).clone();
 
                if (mask[i] == true) {
                    bundleParent2.setId(bundleIndexSon);
                    bundleIndexSon++;
                    son.add(bundleParent2);
                    son.getCoverList().add(bundleParent2.getCenterNode().getId());

                    bundleParent1.setId(bundleIndexDaugther);
                    bundleIndexDaugther++;
                    daughter.add(bundleParent1);
                    daughter.getCoverList().add(bundleParent1.getCenterNode().getId());

                } else {
                    bundleParent1.setId(bundleIndexSon);
                    bundleIndexSon++;
                    son.add(bundleParent1);
                    son.getCoverList().add(bundleParent1.getCenterNode().getId());

                    bundleParent2.setId(bundleIndexDaugther);
                    bundleIndexDaugther++;
                    daughter.add(bundleParent2);
                    daughter.getCoverList().add(bundleParent2.getCenterNode().getId());
                }
            }

            //Insere os feixes que faltam no indivíduo maior
            if (parent1.getNumberOfBundles() > parent2.getNumberOfBundles()) {
                for (int i = size; i < parent1.getNumberOfBundles(); i++) {
                    bundleParent1 = new Bundle();
                    bundleParent1 = parent1.getBundle(i).clone();
                    bundleParent1.setId(bundleIndexSon);
                    bundleIndexSon++;
                    son.add(bundleParent1);
                    son.getCoverList().add(bundleParent1.getCenterNode().getId());
                }
            } else {
                for (int i = size; i < parent2.getNumberOfBundles(); i++) {
                    bundleParent2 = new Bundle();
                    bundleParent2 = parent2.getBundle(i).clone();
                    bundleParent2.setId(bundleIndexDaugther);
                    bundleIndexDaugther++;
                    daughter.add(bundleParent2);
                    daughter.getCoverList().add(bundleParent2.getCenterNode().getId());
                }
            }

            son.setDirected(parent1.getDirected());
            daughter.setDirected(parent1.getDirected());
            son.setSizeEdge(parent1.getSizeEdge());
            son.setSizeNode(parent1.getSizeNode());
            daughter.setSizeEdge(parent1.getSizeEdge());
            daughter.setSizeNode(parent1.getSizeNode());

            //son.makeFeasible(sizeEdgeGraph, edgeList);
            //daughter.makeFeasible(sizeEdgeGraph, edgeList);

            son.makeFeasible();
            daughter.makeFeasible();
            
            sons.add(son);
            sons.add(daughter);

        } catch (Exception e) {
            System.out.print("ERRO" + e.getMessage());
        }
        return sons;
    }

}
