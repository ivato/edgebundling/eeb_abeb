package edge.bundling.controller;


import edge.bundling.graph.Bundle;
import edge.bundling.graph.Edge;
import edge.bundling.view.rendering.LinearDrawPanelGraph;
import edge.bundling.view.rendering.LinearDrawPanelGraphBundled;
import edge.bundling.view.components.ComponentParametersPanel;
import edge.bundling.view.components.ComponentToolBar;
import edge.bundling.graph.Graph;
import edge.bundling.graph.GraphBundled;
import edge.bundling.graphics.Evolution;
import edge.bundling.graphics.Solutions;
import edge.bundling.methods.force.Force;
import edge.bundling.methods.force.ForceParams;
import edge.bundling.methods.genetic.Genetic;
import edge.bundling.methods.genetic.Population;
import edge.bundling.util.Flags;
import edge.bundling.util.GraphData;
import edge.bundling.util.Utils;
import edge.bundling.view.ViewBestSolutionData;
import edge.bundling.view.io.GraphReader;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.swing.ImageIcon;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.RootPaneContainer;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import java.awt.Rectangle;
import java.io.FileOutputStream;
import org.jdom.Document;

public class ControllerMainWindow {

    private JPanel mainWindow;
    private JScrollPane drawScroll;

    private ComponentToolBar toolBarPanel;
    private ComponentParametersPanel paramPanel;
    private LinearDrawPanelGraph drawPanelGraph;
    private LinearDrawPanelGraphBundled drawPanelGraphBundled;

    private Graph graph = new Graph(false);
    private GraphBundled graphBundled = new GraphBundled(false);

    private Genetic genetic;
    private Force force;

    private int algorithmSelected = 0;

    public ControllerMainWindow(JPanel mainWindow, JScrollPane drawScroll, ComponentToolBar toolBarPanel, ComponentParametersPanel paramPanel, LinearDrawPanelGraph drawPanelGraph, LinearDrawPanelGraphBundled drawPanelGraphBundled) {
        this.mainWindow = mainWindow;
        this.drawScroll = drawScroll;
        this.toolBarPanel = toolBarPanel;
        this.paramPanel = paramPanel;
        this.drawPanelGraph = drawPanelGraph;
        this.drawPanelGraphBundled = drawPanelGraphBundled;
        this.toolBarPanel.registerController(this);
        this.paramPanel.registerController(this);
        this.drawPanelGraph.registerController(this);
        this.drawPanelGraphBundled.registerController(this);
    }

   public void setDrawPanelGraphSize(Dimension size) {
      drawPanelGraph.setPreferredSize(size);
      drawPanelGraphBundled.setPreferredSize(size);
   }
    
   public void revalidateViewport() {
      drawScroll.getViewport().revalidate();
      drawScroll.getViewport().repaint();
   }
       
    public void initController() {
        toolBarPanel.getjBOpenGraph().addActionListener(e -> openFile());
        toolBarPanel.getjBSaveImage().addActionListener(e -> saveFile());
        toolBarPanel.getjBOpenSolution().addActionListener(e -> openFileSolution());
        toolBarPanel.getjBZoomIn().addActionListener(e -> zoomIn());
        toolBarPanel.getjBZoomOut().addActionListener(e -> zoomOut());
        toolBarPanel.getjBZoomInNode().addActionListener(e -> zoomInNode());
        toolBarPanel.getjBZoomOutNode().addActionListener(e -> zoomOutNode());
        toolBarPanel.getjBColorNode().addActionListener(e -> colorNode());
        toolBarPanel.getjBColorLabelNode().addActionListener(e -> colorLabelNode());
        toolBarPanel.getjCBLabels().addActionListener(e -> showLabels());
        toolBarPanel.getjCBCover().addActionListener(e -> showCover());
        toolBarPanel.getjTBSound().addActionListener(e -> setSound());
        toolBarPanel.getjCBAlgorithms().addActionListener(e -> selectAlgorithm());
        toolBarPanel.getjBRunAlgorithm().addActionListener(e -> runAlgorithm());
        toolBarPanel.getjBGraphic().addActionListener(e -> showSolutionGraphic());
        toolBarPanel.getjBEvolution().addActionListener(e -> showEvolutionGraphic());
        toolBarPanel.getjTBSaveBestSolutionFile().addActionListener(e -> saveBestSolutionFile());
        toolBarPanel.getjTBDataTableFiles().addActionListener(e -> saveDataTableFile());
        toolBarPanel.getjTBStatisticsFiles().addActionListener(e -> saveStatisticsFile());
        toolBarPanel.getjCBLog().addActionListener(e -> turnLogOnOut());

        paramPanel.getjCBMinAngular().addActionListener(e -> selectMinAngular());
        paramPanel.getjCBMinScale().addActionListener(e -> selectMinScale());
        paramPanel.getjCBMinPosition().addActionListener(e -> selectMinPosition());

        paramPanel.getjCBMaxAngular().addActionListener(e -> selectMaxAngular());
        paramPanel.getjCBMaxScale().addActionListener(e -> selectMaxScale());
        paramPanel.getjCBMaxPosition().addActionListener(e -> selectMaxPosition());

        paramPanel.getjSMaxAngular().addChangeListener(e -> selectMaxAngularValue());
        paramPanel.getjSMaxScale().addChangeListener(e -> selectMaxScaleValue());
        paramPanel.getjSMaxPosition().addChangeListener(e -> selectMaxPositionValue());

        paramPanel.getjSMinAngular().addChangeListener(e -> selectMinAngularValue());
        paramPanel.getjSMinScale().addChangeListener(e -> selectMinScaleValue());
        paramPanel.getjSMinPosition().addChangeListener(e -> selectMinPositionValue());

        paramPanel.getjRBGeneration().addActionListener(e -> selectGeneratiom());
        paramPanel.getjRBDiversity().addActionListener(e -> selectDiversity());
        paramPanel.getjRObjective().addActionListener(e -> selectObjective());
        paramPanel.getjRConstraint().addActionListener(e -> selectConstraint());
        paramPanel.getjCBMutation1().addActionListener(e -> selectMutation1());
        paramPanel.getjCBMutation2().addActionListener(e -> selectMutation2());
        paramPanel.getjCBMutation3().addActionListener(e -> selectMutation3());
        paramPanel.getjCBMutation4().addActionListener(e -> selectMutation4());
        paramPanel.getjCBMutation5().addActionListener(e -> selectMutation5());
        paramPanel.getjRBRoulette().addActionListener(e -> selectRoulette());
        paramPanel.getjRBRanking().addActionListener(e -> selectselectRanking());
        paramPanel.getjRBTournament().addActionListener(e -> selectTournament());
        paramPanel.getjRBRandom().addActionListener(e -> selectRandom());
        paramPanel.getjBColorSource().addActionListener(e -> selectSource());
        paramPanel.getjBColorTarget().addActionListener(e -> selectTarget());
        paramPanel.getjBEdge().addActionListener(e -> selectEdges());
        paramPanel.getjSStifness().addChangeListener(e -> selectStifnessValue());
        paramPanel.getjBReDraw().addActionListener(e -> selectRedrawGraph());
        paramPanel.getjCBPED().addActionListener(e -> selectPED());
        paramPanel.getjCBPEDSOURCE().addActionListener(e -> selectPEDSOURCE());
        paramPanel.getjCBPEDTARGET().addActionListener(e -> selectPEDTARGET());
        toolBarPanel.getjBBestSolution().addActionListener(e -> showDataBestSolution());

        drawPanelGraph.addMouseWheelListener(e -> zoomInOut(e));
        drawPanelGraphBundled.addMouseWheelListener(e -> zoomInOut(e));
    }

    private void openFile() {
        try {
            File xmlFile;
            GraphReader reader;
            JFileChooser fileChooser = new JFileChooser();
            int r = fileChooser.showOpenDialog(this.mainWindow);
            if (r == JFileChooser.APPROVE_OPTION) {
                String path = fileChooser.getSelectedFile().getAbsolutePath();
                String name = fileChooser.getSelectedFile().getName();
                int i = name.lastIndexOf('.');
                if (i > 0 && i < name.length() - 1) {
                    String extension = name.substring(i + 1).toLowerCase();
                    if (extension.equals("xml") || extension.equals("graphml")) {
                        String nameGraph = name;
                        nameGraph = nameGraph.substring(0, i);
                        Flags.NAMEFILE(nameGraph);
                        reader = new GraphReader();
                        xmlFile = new File(path);
                        graph = reader.importGraph(xmlFile, graphBundled);
                    }
                }
                GraphData.initializeData(graph);
                renderingGraph(0);
                initParamentersForOpenFile(0);
            }
        } catch (Exception exception) {
            System.out.println("Não foi possível abrir o arquivo.");
        }
    }

    private void saveFile() {
        try {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Salvar imagem...");
            fileChooser.setFileFilter(new FileNameExtensionFilter("*.png", "png"));
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.setSelectedFile(new File(Flags.NAMEFILE() + ".png"));
            int opcao = fileChooser.showSaveDialog(this.mainWindow);
            if (opcao == JFileChooser.APPROVE_OPTION) {

                File file = fileChooser.getSelectedFile();
                BufferedImage imageBuffer = null;
                if (drawPanelGraph.isActive()) {

                    imageBuffer = new BufferedImage(950, 687, BufferedImage.TYPE_INT_ARGB);
                    Graphics2D graphics = (Graphics2D) imageBuffer.getGraphics();
                    graphics.setColor(Color.WHITE);
                    drawPanelGraph.paintComponent(graphics);
                    graphics.dispose();
                    
                    
                       /* Dimension original = drawPanelGraph.getSize();
                        int width = drawPanelGraph.getWidth() * 4;
                        int height = drawPanelGraph.getHeight() * 4;
                        drawPanelGraph.setSize(width, height);
                        drawPanelGraph.doLayout();
                        imageBuffer =new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
                        Graphics2D g2d = (Graphics2D) imageBuffer.createGraphics();
                        g2d.setStroke(new BasicStroke(0.5f)); 
                        g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
                        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                        g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
                        g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
                        g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
                        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
                        g2d.setTransform(AffineTransform.getScaleInstance(4, 4));
                        drawPanelGraph.print(g2d);
                        g2d.dispose();
                        drawPanelGraph.setSize(original);
                        drawPanelGraph.doLayout();*/
                    
                } else {
                    if (drawPanelGraphBundled.isActive()) {
                        
                        imageBuffer = new BufferedImage(950, 687, BufferedImage.TYPE_INT_ARGB);
                        Graphics2D graphics = (Graphics2D) imageBuffer.getGraphics();
                        graphics.setColor(Color.WHITE);
                        drawPanelGraphBundled.paintComponent(graphics);
                        graphics.dispose();
                        
      
                        
       
                       /* int width = drawPanelGraphBundled.getWidth() ;
                        int height = drawPanelGraphBundled.getHeight() ;
                        
                        imageBuffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                        
                        Graphics2D g2d = (Graphics2D) imageBuffer.createGraphics();
                        g2d.setStroke(new BasicStroke(0.5f)); 
                        g2d.setBackground(Color.WHITE);
                        g2d.setPaint(Color.WHITE);
                        
                        g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
                        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                        g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
                        g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
                        g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
                        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
                        
                        drawPanelGraphBundled.print(g2d);
                        g2d.dispose();*/
                        
                    }
                }

               ImageIO.write(imageBuffer, "png", new File(file.getAbsolutePath()));
            }
        } catch (Exception e1) {
            System.out.println("Não foi possível salvar o arquivo.");
        }
    }

    private void openFileSolution() {
        try {
            JFileChooser fileChooser = new JFileChooser();
            int r = fileChooser.showOpenDialog(this.mainWindow);
            if (r == JFileChooser.APPROVE_OPTION) {

                String path = fileChooser.getSelectedFile().getAbsolutePath();
                String name = fileChooser.getSelectedFile().getName();

                int i = name.lastIndexOf('.');
                if (i > 0 && i < name.length() - 1) {
                    String extension = name.substring(i + 1).toLowerCase();
                    if (extension.equals("txt")) {

                        FileReader arq = new FileReader(path);
                        BufferedReader lerArq = new BufferedReader(arq);
                        String linha = lerArq.readLine();

                        int flag = 0;

                        Bundle bundle = new Bundle();
                        graphBundled.getBundleList().clear();

                        while (linha != null) {
                            int k = linha.indexOf(':');
                            int bundleIndex = Integer.parseInt(linha.substring(0, k));
                            String bundleString = linha.substring(k + 2, linha.length());

                            bundle = new Bundle();
                            bundle.setId(bundleIndex);

                            while (true) {
                                int x = bundleString.indexOf('-');
                                if (x != -1) {
                                    String edge = bundleString.substring(0, x);
                                    StringBuilder text = new StringBuilder(bundleString);
                                    text.replace(0, x + 2, "");
                                    Edge e1 = new Edge();
                                    e1 = graph.getEdgeList().get(Integer.parseInt(edge.trim()));
                                    bundle.add(e1);
                                    bundleString = text.toString();
                                } else {
                                    Edge e1 = new Edge();
                                    e1 = graph.getEdgeList().get(Integer.parseInt(bundleString.trim()));
                                    bundle.add(e1);
                                    break;
                                }
                            }
                            bundle.centerNode();
                            //bundle.setPositionPoint();
                            bundle.compatibility();
                            graphBundled.add(bundle);
                            linha = lerArq.readLine();
                        }
                        arq.close();
                        if (validateParametersForce()) {
                            initGlobalVariables();
                            renderingGraph(1);
                            this.toolBarPanel.getjBBestSolution().setEnabled(false);
                            this.toolBarPanel.getjBRunAlgorithm().setEnabled(false);
                            this.toolBarPanel.getjBGraphic().setEnabled(false);
                            this.toolBarPanel.getjBEvolution().setEnabled(false);
                        }
                    }
                }

            }
        } catch (Exception exception) {
            System.out.println("Não foi possível abrir o arquivo.");
        }
    }

    private void zoomIn() {
        
        Flags.PREVIOUSSCALE(Flags.SCALE());
        Flags.SCALE(Flags.SCALE() + 5);
         
        int value = (int) (Flags.SCALE());      
        int w = (int) (Flags.XSCREENSIZE() * value);
        int h = (int) (Flags.YSCREENSIZE() * value);       
        Dimension size = new Dimension(w, h);
        setDrawPanelGraphSize(size); 
        revalidateViewport(); 
        
        if (drawPanelGraph.isActive()) {
            graph.ajustPosition();
            renderingGraph(0);
        } else {
            if (drawPanelGraphBundled.isActive()) {
                graphBundled.ajustPosition();
                renderingGraph(1);
            }
        }
        ajustLimitZoom();
    }

    private void zoomOut() {
        Flags.PREVIOUSSCALE(Flags.SCALE());
        Flags.SCALE(Flags.SCALE() - 5);
        
        
        int value = (int) (Flags.SCALE());      
        int w = (int) (Flags.XSCREENSIZE() * value);
        int h = (int) (Flags.YSCREENSIZE() * value);       
        Dimension size = new Dimension(w, h);
        setDrawPanelGraphSize(size); 
        revalidateViewport(); 
        
        if (drawPanelGraph.isActive()) {
            graph.ajustPosition();
            renderingGraph(0);
        } else {
            if (drawPanelGraphBundled.isActive()) {
                graphBundled.ajustPosition();
                renderingGraph(1);
            }
        }

        ajustLimitZoom();
    }

    public void zoomInOut(MouseWheelEvent e) {

        int notches = e.getWheelRotation();
        if (notches < 0) {
            if (Flags.SCALE() < (Flags.SCALEDEFAULT() + 60)) {
                zoomIn();
            }
        } else {
            if (Flags.SCALE() > 5) {
                zoomOut();
            }
        }

    }

    private void ajustLimitZoom() {
        toolBarPanel.getjBZoomIn().setEnabled(!(Flags.SCALE() == (Flags.SCALEDEFAULT() + 60)));
        toolBarPanel.getjBZoomOut().setEnabled(!(Flags.SCALE() == 5));
    }

    private void zoomInNode() {
        if (drawPanelGraph.isActive()) {
            drawPanelGraph.zoomInNode();
            renderingGraph(0);
        } else {
            if (drawPanelGraphBundled.isActive()) {
                drawPanelGraphBundled.zoomInNode();
                renderingGraph(1);
            }
        }
    }

    private void zoomOutNode() {
        if (drawPanelGraph.isActive()) {
            drawPanelGraph.zoomOutNode();
            renderingGraph(0);
        } else {
            if (drawPanelGraphBundled.isActive()) {
                drawPanelGraphBundled.zoomOutNode();
                renderingGraph(1);
            }
        }
    }

    
    private void colorNode(){
        Flags.COLORNODE(JColorChooser.showDialog(this.mainWindow, "Choose the color", Flags.COLORNODE()));
        if (Flags.COLORNODE() == null) {
            Flags.COLORNODE(Flags.DEFAULTCOLORNODE());
        }
        toolBarPanel.getjPColorNode().setBackground(Flags.COLORNODE());
        
        if (drawPanelGraph.isActive()) {
            renderingGraph(0);
        } else {
            if (drawPanelGraphBundled.isActive()) {
                renderingGraph(1);
            }
        }
     
    }
    
    
     private void colorLabelNode(){
        Flags.COLORLABELNODE(JColorChooser.showDialog(this.mainWindow, "Choose the color", Flags.COLORLABELNODE()));
        if (Flags.COLORLABELNODE() == null) {
            Flags.COLORLABELNODE(Flags.DEFAULTCOLORLABELNODE());
        }
        toolBarPanel.getjPColorLabelNode().setBackground(Flags.COLORLABELNODE());
        
        
        if (drawPanelGraph.isActive()) {
            renderingGraph(0);
        } else {
            if (drawPanelGraphBundled.isActive()) {
                renderingGraph(1);
            }
        }
    }
    
    private void showLabels() {
        if (toolBarPanel.getjCBLabels().isSelected()) {
            Flags.LABEL(true);
        } else {
            Flags.LABEL(false);
        }

        if (drawPanelGraph.isActive()) {
            renderingGraph(0);
        } else {
            if (drawPanelGraphBundled.isActive()) {
                renderingGraph(1);
            }
        }
    }

    private void showCover() {
        if (toolBarPanel.getjCBCover().isSelected()) {
            Flags.COVER(true);
        } else {
            Flags.COVER(false);
        }

        if (drawPanelGraph.isActive()) {
            renderingGraph(0);
        } else {
            if (drawPanelGraphBundled.isActive()) {
                renderingGraph(1);
            }
        }
    }

    private void setSound() {
        if (toolBarPanel.getjTBSound().isSelected()) {
            Flags.SOUND(true);
            toolBarPanel.getjTBSound().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/sound.png")));
            toolBarPanel.getjTBSound().setBorder(null);
        } else {
            toolBarPanel.getjTBSound().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/sound_mute.png")));
            toolBarPanel.getjTBSound().setBorder(null);
            Flags.SOUND(false);
        }
    }

    public void resetGraph() {
        if (graph.getMovedNodes() == 0) {
            graphBundled.resetEdgesSubdivigion();
        } else {
            graphBundled.resetNodesPosition();
        }
    }

    public void enableParamenterForEvolution() {
        paramPanel.disableMovingPanel();
        paramPanel.enableEvolutionaryPanel();
        toolBarPanel.getjBGraphic().setEnabled(true);
        toolBarPanel.getjBEvolution().setEnabled(true);
        paramPanel.getjBReDraw().setEnabled(true);
        paramPanel.getjCBPED().setEnabled(true);
        checkPED();
        toolBarPanel.getjBBestSolution().setEnabled(true);
    }

    public void disableParamenterForEvolution() {
        paramPanel.enableForcePanel();
        paramPanel.getjBReDraw().setEnabled(false);
        paramPanel.getjCBPED().setEnabled(false);
        paramPanel.getjCBPEDSOURCE().setEnabled(false);
        paramPanel.getjCBPEDTARGET().setEnabled(false);
        paramPanel.getjLPEDTRANSPARENT().setEnabled(false);
        paramPanel.getjTPEDTRANSPARENT().setEnabled(false);
        toolBarPanel.getjBBestSolution().setEnabled(false);
    }

    public void disableParamenterForForce() {
        paramPanel.disableMovingPanel();
        paramPanel.disableEvolutionaryPanel();
        paramPanel.enableForcePanel();
        //paramPanel.getjPSource().setBackground(new java.awt.Color(240, 240, 240));
        paramPanel.getjPTarget().setBackground(new java.awt.Color(240, 240, 240));
        paramPanel.getjPEdges().setBackground(new java.awt.Color(240, 240, 240));
        //paramPanel.getjPSource().setEnabled(false);
        paramPanel.getjPTarget().setEnabled(false);
        paramPanel.getjPEdges().setEnabled(false);
        //paramPanel.getjBColorSource().setEnabled(false);
        paramPanel.getjBColorTarget().setEnabled(false);
        paramPanel.getjBEdge().setEnabled(false);
        paramPanel.getjBReDraw().setEnabled(false);
        paramPanel.getjCBPED().setEnabled(false);
        paramPanel.getjCBPEDSOURCE().setEnabled(false);
        paramPanel.getjCBPEDTARGET().setEnabled(false);
        paramPanel.getjLPEDTRANSPARENT().setEnabled(false);
        paramPanel.getjTPEDTRANSPARENT().setEnabled(false);
        toolBarPanel.getjBBestSolution().setEnabled(false);
        toolBarPanel.getjBGraphic().setEnabled(false);
        toolBarPanel.getjBEvolution().setEnabled(false);
    }

    public void enableParamenterForHybrid() {
        paramPanel.enableEvolutionaryPanel();
        paramPanel.getjBReDraw().setEnabled(true);
        paramPanel.getjCBPED().setEnabled(true);
        checkPED();
        toolBarPanel.getjBBestSolution().setEnabled(true);
    }

    public void disableParamenterForHybrid() {
        paramPanel.enableMovingPanel();
        paramPanel.enableForcePanel();
        paramPanel.getjBReDraw().setEnabled(false);
        paramPanel.getjCBPED().setEnabled(false);
        paramPanel.getjCBPEDSOURCE().setEnabled(false);
        paramPanel.getjCBPEDTARGET().setEnabled(false);
        toolBarPanel.getjBBestSolution().setEnabled(false);
        toolBarPanel.getjBGraphic().setEnabled(false);
        toolBarPanel.getjBEvolution().setEnabled(false);
    }

    private void checkSound() {
        try {
            if (Flags.isSOUND()) {
                String gongFile = "sound.wav";
                InputStream in = new FileInputStream(gongFile);
                AudioStream audioStream = new AudioStream(in);
                AudioPlayer.player.start(audioStream);
            }
        } catch (IOException e) {
            System.out.println("Na abertura do arquivo de som");
        }
    }

    private void selectAlgorithm() {
        algorithmSelected = (int) toolBarPanel.getjCBAlgorithms().getSelectedIndex();

        if (algorithmSelected == 0) { //Evolucionário
            drawPanelGraphBundled.setForce(false);
            enableParamenterForEvolution();
            disableParamenterForEvolution();
            initGlobalVariablesForEvolutionary();
        } else {
            if (algorithmSelected == 1) { //Algoritmo de força
                drawPanelGraphBundled.setForce(true);
                disableParamenterForForce();
                initGlobalVariablesForEvolutionary();
            } else {
                if (algorithmSelected == 2) { //Algoritmo Híbrido
                    drawPanelGraphBundled.setForce(false);
                    enableParamenterForEvolution();
                    disableParamenterForHybrid();
                }
            }
        }
    }

    private void file(String name){
         try {
            File xmlFile;
            GraphReader reader;

            File arquivo = new File(name);
            int i = name.lastIndexOf('.');
            if (i > 0 && i < name.length() - 1) {
                String extension = name.substring(i + 1).toLowerCase();
                if (extension.equals("xml") || extension.equals("graphml")) {
                    String nameGraph = name;
                    nameGraph = nameGraph.substring(0, i);
                    Flags.NAMEFILE(nameGraph);
                    reader = new GraphReader();
                    xmlFile = new File(name);
                    graph = reader.importGraph(xmlFile, graphBundled);
                }
            }
            GraphData.initializeData(graph);
            renderingGraph(0);
            initParamentersForOpenFile(0);
            
        } catch (Exception exception) {
           System.out.println("Não foi possível abrir o arquivo.");
        }
    
    }
    
    
    private void runAlgorithm() {          
        if (Flags.isLOG()) {
            for (int i = 10; i<=10; i++){
                  //if (i == 1){file("10 - Sintetico (20 - 28).graphml"); }
                  //if (i == 2){file("06 - ZacaryKarateClub (34 - 78).graphml"); }
                  //if (i == 3){file("01 - PlanarGraphGD2015 (66 - 101).graphml"); }
                  //if (i == 4){file("09 - DolphinSocialNetwork (62 - 160).graphml");} 
                  //if (i == 5){file("03 - MovieLens (160 - 161).graphml");}
                  //if (i == 6){file("02 - LesMiserables (77-254).graphml");}
                  //if (i == 7){file("08 - BooksAboutUSPolitics (115 - 401).graphml");}
                  //if (i == 8){file("05 - WordAdjacencies (112 - 425).graphml"); }                    
                  //if (i == 9){file("11 - Flare (220 - 709).graphml");}
                  if (i == 10){file("04 - Airlines (235 - 1297).graphml");}
                  execute();          
             }
        }else{
            execute();      
        }
    }


    public void execute(){
            if (graph.getSizeNode() != 0) {
                if (algorithmSelected == 0) {
                    if (validateParametersEvolution()) {
                        resetGraph();
                        initGlobalVariables();
                        initGlobalVariablesForEvolutionary();
                        executeEvolutionay();
                        renderingGraph(1);
                        checkSound();      
                    }
                } 
            } else {
                JOptionPane.showMessageDialog(null, "Nenhum grafo selecionado.");
            }    
    }

    public void executeEvolutionay() {
        try {
            
     
             if (Flags.isLOG()) {
                 
                Flags.SOUND(true);

                String angle = "30";
                for (int i = 1; i <= 15; i++) {
                    angle = "30";
                    Flags.COMPATIBILITY_MAX_ANGULAR_THRESHOLD(0.833);
                    genetic = new Genetic(graph, i, angle);
                    genetic.run(graph);
                   
                    angle = "45";
                    Flags.COMPATIBILITY_MAX_ANGULAR_THRESHOLD(0.750);
                    genetic = new Genetic(graph, i, angle);
                    genetic.run(graph);
                   
                    
                    angle = "70";
                    Flags.COMPATIBILITY_MAX_ANGULAR_THRESHOLD(0.611);
                    genetic = new Genetic(graph, i, angle);
                    genetic.run(graph);
                }
               


            }else{
              
                mainWindow.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                               
                genetic = new Genetic(graph, 0, "");
                genetic.run(graph);  
                      
                mainWindow.setCursor(Cursor.getDefaultCursor());

            }

            graphBundled.getBundleList().clear();
            graphBundled.setBundleList(genetic.getBestIndividualOfBestIndividualsPopulation().getBundleList());

            enableParamenterForEvolution();

        } catch (Exception e) {
            System.out.println("Erro ocorrido durante a evolução.");
        }
    }


    private void showSolutionGraphic() {
        if (genetic != null) {

            Population initialPopulation = genetic.getInitialPopulation();
            Population finalPopulation = genetic.getCurrentPopulation();
            Population bestIndividualPerGenerationPopulation = genetic.getBestIndividualsPopulation();

            SwingUtilities.invokeLater(() -> {
                Solutions solutions;
                try {
                    solutions = new Solutions("Graphic of Solutions", initialPopulation, finalPopulation, bestIndividualPerGenerationPopulation.getBestSolution());
                    solutions.setSize(800, 400);
                    solutions.setLocationRelativeTo(null);
                    solutions.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
                    solutions.setVisible(true);
                } catch (FileNotFoundException ex) {
                    // Logger.getLogger(MainWindow2.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        }
    }

    private void showEvolutionGraphic() {
        if (genetic != null) {

            Population bestIndividualPerGenerationPopulation = genetic.getBestIndividualsPopulation();

            SwingUtilities.invokeLater(() -> {
                Evolution evolution = new Evolution("Evolution", bestIndividualPerGenerationPopulation);
                evolution.setSize(1120, 800);
                evolution.setLocationRelativeTo(null);
                evolution.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
                evolution.setVisible(true);
            });
        }
    }

    private void saveBestSolutionFile() {
        if (toolBarPanel.getjTBSaveBestSolutionFile().isSelected()) {
            Flags.AUTOMATIC_SAVING(true);
            toolBarPanel.getjTBSaveBestSolutionFile().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/table_save1.png")));
            toolBarPanel.getjTBSaveBestSolutionFile().setBorder(null);

        } else {
            toolBarPanel.getjTBSaveBestSolutionFile().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/table_save.png")));
            toolBarPanel.getjTBSaveBestSolutionFile().setBorder(null);
            Flags.AUTOMATIC_SAVING(false);
        }
    }

    private void saveDataTableFile() {
        if (toolBarPanel.getjTBDataTableFiles().isSelected()) {
            toolBarPanel.getjTBDataTableFiles().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/database_gear1.png")));
            toolBarPanel.getjTBDataTableFiles().setBorder(null);
            Flags.DATATABLE(true);
        } else {
            toolBarPanel.getjTBDataTableFiles().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/database_gear.png")));
            toolBarPanel.getjTBDataTableFiles().setBorder(null);
            Flags.DATATABLE(false);
        }
    }

    private void saveStatisticsFile() {
        if (toolBarPanel.getjTBStatisticsFiles().isSelected()) {
            toolBarPanel.getjTBStatisticsFiles().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/chart_pie_add1.png")));
            toolBarPanel.getjTBStatisticsFiles().setBorder(null);
            Flags.STATISTICS(true);
        } else {
            toolBarPanel.getjTBStatisticsFiles().setIcon(new javax.swing.ImageIcon(getClass().getResource("/edge/bundling/icons/chart_pie_add.png")));
            toolBarPanel.getjTBStatisticsFiles().setBorder(null);
            Flags.STATISTICS(false);
        }
    }

    private void showDataBestSolution() {
        if (genetic != null) {
            SwingUtilities.invokeLater(() -> {
                Population bestIndividualPerGenerationPopulation = genetic.getBestIndividualsPopulation();
                ViewBestSolutionData bestSolution = new ViewBestSolutionData(genetic.showBestSolution());
                bestSolution.setSize(600, 600);
                bestSolution.setLocationRelativeTo(null);
                bestSolution.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
                bestSolution.setVisible(true);
            });
        }
    }

    private void turnLogOnOut() {
        if (toolBarPanel.getjCBLog().isSelected()) {
            Flags.LOG(true);
        } else {
            Flags.LOG(false);
        }
    }

    private void selectMinAngular() {

        if (paramPanel.getjCBMinAngular().isSelected()) {

            Flags.ANGULAR(true);
            paramPanel.getjSMinAngular().setEnabled(true);
            paramPanel.getjLMinAngular().setEnabled(true);

            paramPanel.getjCBMaxAngular().setSelected(true);
            paramPanel.getjSMaxAngular().setEnabled(true);
            paramPanel.getjLMaxAngular().setEnabled(true);
            paramPanel.getjTPenalty().setEnabled(true);
            paramPanel.getjLPenalty().setEnabled(true);

        } else {

            Flags.ANGULAR(false);
            paramPanel.getjSMinAngular().setEnabled(false);
            paramPanel.getjLMinAngular().setEnabled(false);

            paramPanel.getjCBMaxAngular().setSelected(false);
            paramPanel.getjSMaxAngular().setEnabled(false);
            paramPanel.getjLMaxAngular().setEnabled(false);

        }

    }

    
    private boolean testAllMetricsNotSelected(){
        if (!paramPanel.getjCBMaxAngular().isSelected()) {
            if (!paramPanel.getjCBMaxScale().isSelected()) {
                if (!paramPanel.getjCBMaxPosition().isSelected()) {
                    return true;
                }
            }
        }       
        return false;
    }
    
    private void selectMinScale() {
        if (paramPanel.getjCBMinScale().isSelected()) {
            Flags.SCALE(true);
            paramPanel.getjSMinScale().setEnabled(true);
            paramPanel.getjLMinScale().setEnabled(true);

            paramPanel.getjCBMaxScale().setSelected(true);
            paramPanel.getjSMaxScale().setEnabled(true);
            paramPanel.getjLMaxScale().setEnabled(true);
            
            paramPanel.getjTPenalty().setEnabled(true);
            paramPanel.getjLPenalty().setEnabled(true);

        } else {
            Flags.SCALE(false);
            paramPanel.getjSMinScale().setEnabled(false);
            paramPanel.getjLMinScale().setEnabled(false);

            paramPanel.getjCBMaxScale().setSelected(false);
            paramPanel.getjSMaxScale().setEnabled(false);
            paramPanel.getjLMaxScale().setEnabled(false);

        }
    }

    private void selectMinPosition() {
        if (paramPanel.getjCBMinPosition().isSelected()) {
            Flags.POSITION(true);
            paramPanel.getjSMinPosition().setEnabled(true);
            paramPanel.getjLMinPosition().setEnabled(true);

            paramPanel.getjCBMaxPosition().setSelected(true);
            paramPanel.getjSMaxPosition().setEnabled(true);
            paramPanel.getjLMaxPosition().setEnabled(true);
            
            paramPanel.getjTPenalty().setEnabled(true);
            paramPanel.getjLPenalty().setEnabled(true);

        } else {
            Flags.POSITION(false);
            paramPanel.getjSMinPosition().setEnabled(false);
            paramPanel.getjLMinPosition().setEnabled(false);

            paramPanel.getjCBMaxPosition().setSelected(false);
            paramPanel.getjSMaxPosition().setEnabled(false);
            paramPanel.getjLMaxPosition().setEnabled(false);

        }
    }

    private void selectMaxAngular() {
        if (paramPanel.getjCBMaxAngular().isSelected()) {

            Flags.ANGULAR(true);
            paramPanel.getjSMaxAngular().setEnabled(true);
            paramPanel.getjLMaxAngular().setEnabled(true);
            
            paramPanel.getjTPenalty().setEnabled(true);
            paramPanel.getjLPenalty().setEnabled(true);

            if (algorithmSelected == 2) {
                paramPanel.getjCBMinAngular().setSelected(true);
                paramPanel.getjSMinAngular().setEnabled(true);
                paramPanel.getjLMinAngular().setEnabled(true);
            }
        } else {

            Flags.ANGULAR(false);
            paramPanel.getjSMaxAngular().setEnabled(false);
            paramPanel.getjLMaxAngular().setEnabled(false);
            
            if (testAllMetricsNotSelected()){
                paramPanel.getjTPenalty().setEnabled(false);
                paramPanel.getjLPenalty().setEnabled(false);
            }

            if (algorithmSelected == 2) {
                paramPanel.getjCBMinAngular().setSelected(false);
                paramPanel.getjSMinAngular().setEnabled(false);
                paramPanel.getjLMinAngular().setEnabled(false);
            }
        }
    }

    private void selectMaxScale() {
        if (paramPanel.getjCBMaxScale().isSelected()) {
            Flags.SCALE(true);
            paramPanel.getjSMaxScale().setEnabled(true);
            paramPanel.getjLMaxScale().setEnabled(true);
            
            paramPanel.getjTPenalty().setEnabled(true);
            paramPanel.getjLPenalty().setEnabled(true);
            
            if (algorithmSelected == 2) {
                paramPanel.getjCBMinScale().setSelected(true);
                paramPanel.getjSMinScale().setEnabled(true);
                paramPanel.getjLMinScale().setEnabled(true);
            }
        } else {
            Flags.SCALE(false);
            paramPanel.getjSMaxScale().setEnabled(false);
            paramPanel.getjLMaxScale().setEnabled(false);
            
            if (testAllMetricsNotSelected()){
                paramPanel.getjTPenalty().setEnabled(false);
                paramPanel.getjLPenalty().setEnabled(false);
            }
            
            if (algorithmSelected == 2) {
                paramPanel.getjCBMinScale().setSelected(false);
                paramPanel.getjSMinScale().setEnabled(false);
                paramPanel.getjLMinScale().setEnabled(false);
            }
        }
    }

    private void selectMaxPosition() {
        if (paramPanel.getjCBMaxPosition().isSelected()) {
            Flags.POSITION(true);
            paramPanel.getjSMaxPosition().setEnabled(true);
            paramPanel.getjLMaxPosition().setEnabled(true);
            
            paramPanel.getjTPenalty().setEnabled(true);
            paramPanel.getjLPenalty().setEnabled(true);
            
            if (algorithmSelected == 2) {
                paramPanel.getjCBMinPosition().setSelected(true);
                paramPanel.getjSMinPosition().setEnabled(true);
                paramPanel.getjLMinPosition().setEnabled(true);
            }
        } else {
            Flags.POSITION(false);
            paramPanel.getjSMaxPosition().setEnabled(false);
            paramPanel.getjLMaxPosition().setEnabled(false);
            
            if (testAllMetricsNotSelected()){
                paramPanel.getjTPenalty().setEnabled(false);
                paramPanel.getjLPenalty().setEnabled(false);
            }
            
            if (algorithmSelected == 2) {
                paramPanel.getjCBMinPosition().setSelected(false);
                paramPanel.getjSMinPosition().setEnabled(false);
                paramPanel.getjLMinPosition().setEnabled(false);
            }
        }
    }

    private void selectMaxAngularValue() {
        double value = (double) paramPanel.getjSMaxAngular().getValue() / 100;
        Flags.COMPATIBILITY_MAX_ANGULAR_THRESHOLD(value);
        paramPanel.getjLMaxAngular().setText(String.valueOf(value));
    }

    private void selectMaxScaleValue() {
        double value = (double) paramPanel.getjSMaxScale().getValue() / 100;
        Flags.COMPATIBILITY_MAX_SCALE_THRESHOLD(value);
        paramPanel.getjLMaxScale().setText(String.valueOf(value));
    }

    private void selectMaxPositionValue() {
        double value = (double) paramPanel.getjSMaxPosition().getValue() / 100;
        Flags.COMPATIBILITY_MAX_POSITION_THRESHOLD(value);
        paramPanel.getjLMaxPosition().setText(String.valueOf(value));
    }

    private void selectMinAngularValue() {
        double value = (double) paramPanel.getjSMinAngular().getValue() / 100;
        Flags.COMPATIBILITY_MIN_ANGULAR_THRESHOLD(value);
        paramPanel.getjLMinAngular().setText(String.valueOf(value));
    }

    private void selectMinScaleValue() {
        double value = (double) paramPanel.getjSMinScale().getValue() / 100;
        Flags.COMPATIBILITY_MIN_SCALE_THRESHOLD(value);
        paramPanel.getjLMinScale().setText(String.valueOf(value));
    }

    private void selectMinPositionValue() {
        double value = (double) paramPanel.getjSMinPosition().getValue() / 100;
        Flags.COMPATIBILITY_MIN_POSITION_THRESHOLD(value);
        paramPanel.getjLMinPosition().setText(String.valueOf(value));
    }

    private void selectGeneratiom() {
        Flags.TERMINATION_BY_GENERATION(true);
        Flags.TERMINATION_BY_DIVERSITY(false);
    }

    private void selectDiversity() {
        Flags.TERMINATION_BY_GENERATION(false);
        Flags.TERMINATION_BY_DIVERSITY(true);
    }

    private void selectObjective() {
        if (paramPanel.getjRObjective().isSelected()) {
            Flags.OBJECTIVE(true);
            Flags.CONSTRAINT(false);
        }
    }

    private void selectConstraint() {
        if (paramPanel.getjRConstraint().isSelected()) {
            Flags.OBJECTIVE(false);
            Flags.CONSTRAINT(true);
        }
    }

    private void selectMutation1() {
        if (paramPanel.getjCBMutation1().isSelected()) {
            Flags.MUTATION_ONE(true);
        } else {
            Flags.MUTATION_ONE(false);
        }
    }

    private void selectMutation2() {
        if (paramPanel.getjCBMutation2().isSelected()) {
            Flags.MUTATION_TWO(true);
        } else {
            Flags.MUTATION_TWO(false);
        }
    }

    private void selectMutation3() {
        if (paramPanel.getjCBMutation3().isSelected()) {
            Flags.MUTATION_THREE(true);
        } else {
            Flags.MUTATION_THREE(false);
        }
    }

    private void selectMutation4() {
        if (paramPanel.getjCBMutation4().isSelected()) {
            Flags.MUTATION_FOUR(true);
        } else {
            Flags.MUTATION_FOUR(false);
        }
    }

    private void selectMutation5() {
        if (paramPanel.getjCBMutation5().isSelected()) {
            Flags.MUTATION_FIVE(true);
        } else {
            Flags.MUTATION_FIVE(false);
        }
    }

    private void selectRoulette() {
        Flags.ROULETTE_WHEEL(true);
        Flags.TOURNAMENT(false);
        Flags.RANKING(false);
        Flags.RANDOM(false);
        paramPanel.getjTSelectivePressure().setEnabled(false);
        paramPanel.getjTSelectivePressure().setBackground(new java.awt.Color(255, 255, 204));
    }

    private void selectselectRanking() {
        Flags.ROULETTE_WHEEL(false);
        Flags.TOURNAMENT(false);
        Flags.RANKING(true);
        Flags.RANDOM(false);
        paramPanel.getjTSelectivePressure().setEnabled(true);
        paramPanel.getjTSelectivePressure().setBackground(new java.awt.Color(255, 255, 255));
    }

    private void selectTournament() {
        Flags.ROULETTE_WHEEL(false);
        Flags.TOURNAMENT(true);
        Flags.RANKING(false);
        Flags.RANDOM(false);
        paramPanel.getjTSelectivePressure().setEnabled(false);
        paramPanel.getjTSelectivePressure().setBackground(new java.awt.Color(255, 255, 204));
    }

    private void selectRandom() {
        Flags.TOURNAMENT(false);
        Flags.RANKING(false);
        Flags.RANDOM(true);
        paramPanel.getjTSelectivePressure().setEnabled(false);
        paramPanel.getjTSelectivePressure().setBackground(new java.awt.Color(255, 255, 204));
    }

    private void selectSource() {
        Flags.COLORSOURCE(JColorChooser.showDialog(this.mainWindow, "Choose the color", Flags.COLORSOURCE()));
        if (Flags.COLORSOURCE() == null) {
            Flags.COLORSOURCE(Flags.DEFAULTCOLORSOURCE());
        }
        paramPanel.getjPSource().setBackground(Flags.COLORSOURCE());
    }

    private void selectTarget() {
        Flags.COLORTARGET(JColorChooser.showDialog(this.mainWindow, "Choose the color", Flags.COLORTARGET()));
        if (Flags.COLORTARGET() == null) {
            Flags.COLORTARGET(Flags.DEFAULTCOLORTARGET());
        }
        paramPanel.getjPTarget().setBackground(Flags.COLORTARGET());
    }

    private void selectEdges() {
        Flags.COLOREDGE(JColorChooser.showDialog(this.mainWindow, "Choose the color", Flags.COLOREDGE()));
        if (Flags.COLOREDGE() == null) {
            Flags.COLOREDGE(Flags.DEFAULTCOLOREDGE());
        }
        paramPanel.getjPEdges().setBackground(Flags.COLOREDGE());
    }

    private void selectStifnessValue() {
        ForceParams.STIFFINESS((int) this.paramPanel.getjSStifness().getValue());
    }

    
    private void checkPED(){
        if(paramPanel.getjCBPED().isSelected()){
                paramPanel.getjCBPEDSOURCE().setEnabled(true);
                paramPanel.getjCBPEDTARGET().setEnabled(true);
                paramPanel.getjLPEDTRANSPARENT().setEnabled(true);
                paramPanel.getjTPEDTRANSPARENT().setEnabled(true);
                Flags.setPEDTRANSPARENT(Integer.parseInt(paramPanel.getjTPEDTRANSPARENT().getText()));
        }else{
                paramPanel.getjCBPEDSOURCE().setEnabled(false);
                paramPanel.getjCBPEDTARGET().setEnabled(false);
                paramPanel.getjLPEDTRANSPARENT().setEnabled(false);
                paramPanel.getjTPEDTRANSPARENT().setEnabled(false);
        }
    }
    
    
    private void selectPED(){
        Flags.setPEDTRANSPARENT(10);
        paramPanel.getjTPEDTRANSPARENT().setText("10");
        
        if (validateParametersForce()) {
            
            checkPED();
            
            initGlobalVariables();
            renderingGraph(1);
        }
    }
    
    private void selectPEDSOURCE(){
        if (validateParametersForce()) {
            
            if(paramPanel.getjCBPEDSOURCE().isSelected()){
                paramPanel.getjCBPEDTARGET().setSelected(false);
            }
            
            initGlobalVariables();
            renderingGraph(1);
        }
    }
        
    private void selectPEDTARGET(){
        if (validateParametersForce()) {
            
            if(paramPanel.getjCBPEDTARGET().isSelected()){
                paramPanel.getjCBPEDSOURCE().setSelected(false);
            }
                        
            initGlobalVariables();
            renderingGraph(1);
        }
    }
    
    private void selectRedrawGraph() {
  
        if (validateParametersForce()) {
             
            initGlobalVariables();
            renderingGraph(1);
        }
    }
    
    private void initGlobalVariablesForEvolutionary() {
        Flags.COMPATIBILITY_MIN_ANGULAR_THRESHOLD(1);
        Flags.COMPATIBILITY_MIN_SCALE_THRESHOLD(1);
        Flags.COMPATIBILITY_MIN_POSITION_THRESHOLD(1);

        paramPanel.getjLMinAngular().setText(String.valueOf(Flags.COMPATIBILITY_MIN_ANGULAR_THRESHOLD()));
        paramPanel.getjLMinScale().setText(String.valueOf(Flags.COMPATIBILITY_MIN_SCALE_THRESHOLD()));
        paramPanel.getjLMinPosition().setText(String.valueOf(Flags.COMPATIBILITY_MIN_POSITION_THRESHOLD()));
        
        paramPanel.getjSMinAngular().setValue(100);
        paramPanel.getjSMinScale().setValue(100);
        paramPanel.getjSMinPosition().setValue(100);
        
        
     }
     
    private void initGlobalVariablesForHybrid() {
        Flags.ANGULAR(true);
        Flags.SCALE(true);
        Flags.POSITION(false);
        
        Flags.COMPATIBILITY_MIN_ANGULAR_THRESHOLD(1);
        Flags.COMPATIBILITY_MIN_SCALE_THRESHOLD(1);
        Flags.COMPATIBILITY_MIN_POSITION_THRESHOLD(1);

        paramPanel.getjLMinAngular().setText(String.valueOf(Flags.COMPATIBILITY_MIN_ANGULAR_THRESHOLD()));
        paramPanel.getjLMinScale().setText(String.valueOf(Flags.COMPATIBILITY_MIN_SCALE_THRESHOLD()));
        paramPanel.getjLMinPosition().setText(String.valueOf(Flags.COMPATIBILITY_MIN_POSITION_THRESHOLD()));
        
        paramPanel.getjSMinAngular().setValue(100);
        paramPanel.getjSMinScale().setValue(100);
        paramPanel.getjSMinPosition().setValue(100);
     }

    private void initGlobalVariables() {

        Flags.STEPCONVEXHUL(Integer.parseInt(this.paramPanel.getjTStepConvexHull().getText()));
        Flags.TAM_POPULATION(Integer.parseInt(this.paramPanel.getjTTamPopulation().getText()));
        Flags.MAX_GENERATION(Integer.parseInt(this.paramPanel.getjTMaxGeneration().getText()));
        Flags.MAX_GENERATION_TO_WAIT(Integer.parseInt(this.paramPanel.getjTMaxGeneration().getText()));
        Flags.CROSSOVER_RATIO(Float.parseFloat(this.paramPanel.getjTCrossoverRatio().getText()));
        Flags.MUTATION_RATIO(Float.parseFloat(this.paramPanel.getjTMutationRatio().getText()));
        Flags.SELECTIVE_PRESSURE(Integer.parseInt(this.paramPanel.getjTSelectivePressure().getText()));
        Flags.PED(this.paramPanel.getjCBPED().isSelected());
        Flags.PEDSOURCE(this.paramPanel.getjCBPEDSOURCE().isSelected());
        Flags.PEDTARGET(this.paramPanel.getjCBPEDTARGET().isSelected());
        Flags.setPEDTRANSPARENT(Integer.parseInt(paramPanel.getjTPEDTRANSPARENT().getText()));
        
        ForceParams.INTERACTIONS(Integer.parseInt(this.paramPanel.getjTIteractions().getText()));
        ForceParams.CYCLES(Integer.parseInt(this.paramPanel.getjTCycles().getText()));
        ForceParams.STEPSIZE(Double.parseDouble(this.paramPanel.getjTStepSize().getText()));
        
        

    }

    private void renderingGraph(int graphIndex) {
        if (graphIndex == 0) {
            this.drawPanelGraph.setActive(true);
            this.drawPanelGraphBundled.setActive(false);
            this.drawPanelGraph.validate();
            this.drawPanelGraph.repaint();
            this.drawScroll.setViewportView(this.drawPanelGraph);
        }
        if (graphIndex == 1) {
            this.drawPanelGraphBundled.setActive(true);
            this.drawPanelGraph.setActive(false);
            this.drawPanelGraphBundled.validate();
            this.drawPanelGraphBundled.repaint();
            this.drawScroll.setViewportView(this.drawPanelGraphBundled);
        }
        
        this.paramPanel.getjCBMaxAngular().setSelected(Flags.isANGULAR());
        this.paramPanel.getjCBMaxPosition().setSelected(Flags.isPOSITION());
        this.paramPanel.getjCBMaxScale().setSelected(Flags.isSCALE());
        this.paramPanel.getjSMaxAngular().setEnabled(Flags.isANGULAR());
        this.paramPanel.getjSMaxScale().setEnabled(Flags.isSCALE());
        this.paramPanel.getjSMaxPosition().setEnabled(Flags.isPOSITION());
        this.paramPanel.getjLMaxAngular().setEnabled(Flags.isANGULAR());
        this.paramPanel.getjLMaxScale().setEnabled(Flags.isSCALE());
        this.paramPanel.getjLMaxPosition().setEnabled(Flags.isPOSITION());
        
           
        this.paramPanel.getjCBMinAngular().setSelected(Flags.isANGULAR());
        this.paramPanel.getjCBMinPosition().setSelected(Flags.isPOSITION());
        this.paramPanel.getjCBMinScale().setSelected(Flags.isSCALE());
        this.paramPanel.getjSMinAngular().setEnabled(Flags.isANGULAR());
        this.paramPanel.getjSMinScale().setEnabled(Flags.isSCALE());
        this.paramPanel.getjSMinPosition().setEnabled(Flags.isPOSITION());
        this.paramPanel.getjLMinAngular().setEnabled(Flags.isANGULAR());
        this.paramPanel.getjLMinScale().setEnabled(Flags.isSCALE());
        this.paramPanel.getjLMinPosition().setEnabled(Flags.isPOSITION());
    }

    private void initParamentersForOpenFile(int graphIndex) {
        if (graphIndex == 0) {
            toolBarPanel.getjBSaveImage().setEnabled(true);
            toolBarPanel.getjCBLabels().setSelected(Flags.isLABEL());
            toolBarPanel.getjCBCover().setSelected(Flags.isCOVER());
            toolBarPanel.getjTBSound().setEnabled(true);
            toolBarPanel.getjBRunAlgorithm().setEnabled(true);
            toolBarPanel.getjBZoomIn().setEnabled(true);
            toolBarPanel.getjBZoomOut().setEnabled(true);
            toolBarPanel.getjBZoomInNode().setEnabled(true);
            toolBarPanel.getjBZoomOutNode().setEnabled(true);
            toolBarPanel.getjPColorNode().setEnabled(true);
            toolBarPanel.getjBColorNode().setEnabled(true);
            toolBarPanel.getjPColorLabelNode().setEnabled(true);
            toolBarPanel.getjBColorLabelNode().setEnabled(true);
            toolBarPanel.getjCBLabels().setEnabled(true);
            toolBarPanel.getjCBCover().setEnabled(true);
            toolBarPanel.getjBRunAlgorithm().setEnabled(true);
            toolBarPanel.getjBGraphic().setEnabled(false);
            toolBarPanel.getjBEvolution().setEnabled(false);
            toolBarPanel.getjBOpenSolution().setEnabled(true);
            this.paramPanel.getjBReDraw().setEnabled(false);
            paramPanel.getjCBPED().setEnabled(false);
            paramPanel.getjCBPEDSOURCE().setEnabled(false);
            paramPanel.getjCBPEDTARGET().setEnabled(false);
            toolBarPanel.getjBBestSolution().setEnabled(false);
        }
    }

    private boolean validateParametersEvolution() {
        
        if (this.paramPanel.getjTPenalty().getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Penalty number must be informed (Ex: -3)");
            return false;
        }
        
        if (this.paramPanel.getjTTamPopulation().getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Population number must be informed (Ex: 100)");
            return false;
        }
        if (this.paramPanel.getjTMaxGeneration().getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Generation number must be informed (Ex: 10)");
            return false;
        }
        if (this.paramPanel.getjTCrossoverRatio().getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Crossover Rate must be informed (Ex: 0.7)");
            return false;
        }
        if (this.paramPanel.getjTMutationRatio().getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Mutation Rate must be informed (Ex: 0.1)");
            return false;
        }

        String value = this.paramPanel.getjTPenalty().getText();
        if (!Utils.isNumericNegative(value)) {
            JOptionPane.showMessageDialog(null, "Penalty number must be a negative integer (Ex: -3)");
            return false;
        }
        
        value = this.paramPanel.getjTTamPopulation().getText();
        if (!Utils.isNumeric(value)) {
            JOptionPane.showMessageDialog(null, "Population number must be a positive integer (Ex: 100)");
            return false;
        }
        value = this.paramPanel.getjTMaxGeneration().getText();
        if (!Utils.isNumeric(value)) {
            JOptionPane.showMessageDialog(null, "Generation number must be a positive integer (Ex: 10)");
            return false;
        }

        value = this.paramPanel.getjTCrossoverRatio().getText();
        if (!Utils.isReal(value)) {
            JOptionPane.showMessageDialog(null, "Crossover Rate must be a positive real (Ex: 0.7)");
            return false;
        }
        value = this.paramPanel.getjTMutationRatio().getText();
        if (!Utils.isReal(value)) {
            JOptionPane.showMessageDialog(null, "Mutation Rate must be a positive real (Ex: 0.1)");
            return false;
        }

        int valueInt = (int) Integer.parseInt(this.paramPanel.getjTTamPopulation().getText());
        if (valueInt <= 0) {
            JOptionPane.showMessageDialog(null, "Population number must be greater than 0 (Ex: 100)");
            return false;
        }
        valueInt = (int) Integer.parseInt(this.paramPanel.getjTMaxGeneration().getText());
        if (valueInt <= 0) {
            JOptionPane.showMessageDialog(null, "Generation number must be greater than 0 (Ex: 10)");
            return false;
        }

        double valueDouble = (double) Double.parseDouble(this.paramPanel.getjTCrossoverRatio().getText());
        if ((valueDouble < 0) || (valueDouble > 1)) {
            JOptionPane.showMessageDialog(null, "Crossover Rate must be between 0 and 1 (Ex: 0.7)");
            return false;
        }
        valueDouble = (double) Double.parseDouble(this.paramPanel.getjTMutationRatio().getText());
        if ((valueDouble < 0) || (valueDouble > 1)) {
            JOptionPane.showMessageDialog(null, "Mutation Rate must be between 0 and 1 (Ex: 0.1)");
            return false;
        }

        if (this.paramPanel.getjRBRanking().isSelected()) {
            if (this.paramPanel.getjTSelectivePressure().getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Selective Pressure number must be informed (Ex: 2)");
                return false;
            }

            if (!Utils.isReal(this.paramPanel.getjTSelectivePressure().getText())) {
                JOptionPane.showMessageDialog(null, "Selective Pressure must be a positive integer (Ex: 2)");
                return false;
            }

            double valuePressure = (double) Double.parseDouble(this.paramPanel.getjTSelectivePressure().getText());
            if ((valuePressure < 1) || (valuePressure > 2)) {
                JOptionPane.showMessageDialog(null, "Selective Pressure must be between 1 and 2 ");
                return false;
            }
        }

        if (!validateParametersForce()) {
            return false;
        }
        return true;
    }

    private boolean validateParametersMoving() {
        if (this.paramPanel.getjTStepConvexHull().getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Step must be informed (Ex: 6)");
            return false;
        }
        String value = this.paramPanel.getjTStepConvexHull().getText();
        if (!Utils.isNumeric(value)) {
            JOptionPane.showMessageDialog(null, "Step number must be a positive integer (Ex: 100)");
            return false;
        }
        int valueInt = (int) Integer.parseInt(this.paramPanel.getjTStepConvexHull().getText());
        if (valueInt <= 0) {
            JOptionPane.showMessageDialog(null, "Step number must be greater than 0 (Ex: 100)");
            return false;
        }
        return true;
    }

    private boolean validateParametersForce() {
        
        if (paramPanel.getjCBPED().isSelected()){
            if (this.paramPanel.getjTPEDTRANSPARENT().getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Transparency must be informed (Ex: 6)");
                return false;
            }
            String value = this.paramPanel.getjTPEDTRANSPARENT().getText();
            if (!Utils.isNumeric(value)) {
                JOptionPane.showMessageDialog(null, "Transparency number must be a positive integer (Ex: 6)");
                return false;
            }
            
            int valueInt = (int) Integer.parseInt(this.paramPanel.getjTPEDTRANSPARENT().getText());
            if (valueInt < 0) {
                JOptionPane.showMessageDialog(null, "Transparency number must be equal or greater than 0 (Ex: 6)");
                return false;
            }else{
               if (valueInt > 255) {
                JOptionPane.showMessageDialog(null, "Transparency number must be equal or smaller than 255 (Ex: 6)");
                return false;
                } 
            }
        }
        
        if (this.paramPanel.getjTCycles().getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Cycles must be informed (Ex: 6)");
            return false;
        }

        if (this.paramPanel.getjTIteractions().getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Iteractions must be informed (Ex: 50)");
            return false;
        }

        if (this.paramPanel.getjTStepSize().getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Step Size must be informed (Ex: 0.4)");
            return false;
        }

        String value = this.paramPanel.getjTCycles().getText();
        if (!Utils.isNumeric(value)) {
            JOptionPane.showMessageDialog(null, "Cycles number must be a positive integer (Ex: 6)");
            return false;
        }

        value = this.paramPanel.getjTIteractions().getText();
        if (!Utils.isNumeric(value)) {
            JOptionPane.showMessageDialog(null, "Iteractions number must be a positive integer (Ex: 50)");
            return false;
        }

        value = this.paramPanel.getjTStepSize().getText();
        if (!Utils.isReal(value)) {
            JOptionPane.showMessageDialog(null, "StepSize must be a positive real (Ex: 0.4)");
            return false;
        }

        int valueInt = (int) Integer.parseInt(this.paramPanel.getjTCycles().getText());
        if (valueInt < 1) {
            JOptionPane.showMessageDialog(null, "Cycles number must be equal or greater than 1 (Ex: 6)");
            return false;
        }
        valueInt = (int) Integer.parseInt(this.paramPanel.getjTIteractions().getText());
        if (valueInt < 1) {
            JOptionPane.showMessageDialog(null, "Iteractions number must be equal or greater than 1 (Ex: 50)");
        }

        return true;
    }

    public GraphBundled getGraphBundled() {
        return graphBundled;
    }

    public Graph getGraph() {
        return graph;
    }

    public void setGraph(GraphBundled graphBundled) {
        this.graphBundled = graphBundled;
    }

    public void setGraph(Graph graph) {
        this.graph = graph;
    }
}
