package edge.bundling.graph;

import edge.bundling.util.Flags;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

public class Graph {

    protected ArrayList<Node> nodeList;
    protected ArrayList<Edge> edgeList;
    protected boolean isDirected = false;
    protected int sizeEdge = 0;
    protected int sizeNode = 0;
    protected double area = 0;
    protected int movedNodes = 0;
    private double xArea;
    private double yArea;
    
    public Graph(boolean direct) {
        nodeList = new ArrayList<Node>();
        edgeList = new ArrayList<Edge>();
        isDirected = direct;
    }


    //*************************************************************************************************************
    /**
     * Reinicia a escala do grafo com base na área do desenho
     *
     */
//*************************************************************************************************************               
    public void resetScale() {
        double xArea = Math.abs(Flags.XSCREENSIZE());
        double yArea = Math.abs(Flags.YSCREENSIZE());
        double area = (double) Math.sqrt(xArea * yArea);

        double newScale = area / getArea();

        if (newScale < 5) {
            newScale = 15;
        } else {
            if ((newScale % 5) != 0) {
                newScale = newScale - (newScale % 5);
            }
        }

        Flags.SCALE((int) newScale);
        Flags.PREVIOUSSCALE((int) newScale);
        Flags.SCALEDEFAULT((int) newScale);
    }

    //*************************************************************************************************************
    /**
     * Seta a lista de vértices e arestas como nulas
     *
     */
//*************************************************************************************************************               
    public void clear() {
        if (getNodeList() != null) {
            getNodeList().clear();
            getEdgeList().clear();
        }
    }

    //*************************************************************************************************************
    /**
     * Reinicia a posição dos vértices para a posição original
     *
     */
//*************************************************************************************************************               
    public void resetNodesPosition() {
        double x1, y1;
        for (int i = 0; i < this.getNodeList().size(); i++) {
            Node n = this.getNodeList().get(i);
            x1 = getNodeList().get(i).getxOriginal();
            y1 = getNodeList().get(i).getyOriginal();
            getNodeList().get(i).setX(x1);
            getNodeList().get(i).setY(y1);
        }

        resetSubNodesList();
        setMovedNodes(0);
        area();
        resetScale();
    }

    //*************************************************************************************************************
    /**
     * Retira os subseguimentos das aresas
     *
     */
//*************************************************************************************************************               
    public void resetSubNodesList() {
        Edge edge;
        for (int i = 0; i < getEdgeList().size(); i++) {
            edge = new Edge();
            edge = getEdgeList().get(i);
            edge.resetSubNodes();
        }
    }

//*************************************************************************************************************
    /**
     * Subdivide as arestas do grafo em segmentos
     *
     */
//*************************************************************************************************************               
    public void startSubNodesList() {
        Edge edge;
        for (int i = 0; i < getEdgeList().size(); i++) {
            edge = new Edge();
            edge = getEdgeList().get(i);
            edge.startSubNodesList();
        }
    }

    //*************************************************************************************************************
    /**
     * Ordena os vértices passados como parâmetreo por grau
     *
     * @param listOfNodes - lista a ser ordenada
     */
//*************************************************************************************************************               
    public List orderedNodes(List listOfNodes) {

        List orderedList = new ArrayList();
        Map<Integer, Integer> listNodes = new HashMap<Integer, Integer>();

        for (Object idNode : listOfNodes) {
            Node n = (Node) this.nodeList.get(Integer.parseInt(idNode.toString()));
            listNodes.put(n.getId(), n.getNumberOfEdges());
        }

        List<Map.Entry<Integer, Integer>> entryListNodes = new ArrayList<Map.Entry<Integer, Integer>>(listNodes.entrySet());
        Collections.sort(entryListNodes, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> integerEntry, Map.Entry<Integer, Integer> integerEntry2) {
                return integerEntry2.getValue().compareTo(integerEntry.getValue());
            }
        });

        for (Map.Entry<Integer, Integer> mapping : entryListNodes) {
            int indexNode = mapping.getKey();
            orderedList.add(indexNode);
        }
        return orderedList;
    }

    //*************************************************************************************************************
    /**
     * Adiciona um vértice ao grafo
     *
     * @param node - vértice a ser adicionado
     */
//*************************************************************************************************************               
    public void addNode(Node node) {
        nodeList.add(node);
        this.sizeNode++;
    }

    //*************************************************************************************************************
    /**
     * Adiciona uma aresta ao grafo
     *
     * @param edge - aresta a ser adicionada
     */
//*************************************************************************************************************               
    public void addEdge(Edge edge) {

        //Verifica se os vértices adjacentes à aresta existem no grafo
        int g1Idx = nodeList.indexOf(edge.getStartNode());
        int g2Idx = nodeList.indexOf(edge.getEndNode());
        if (g1Idx < 0 || g2Idx < 0) {
            throw new NullPointerException("Node not found in the graph");
        }

        //Adiciona arestas à lista de arestas adjacentes dos vértices adjacentes da aresta	
        edge.getStartNode().addAdjEdge(edge);
        edge.getEndNode().addAdjEdge(edge);

        //Adiciona a aresta ao grafo
        getEdgeList().add(edge);
        this.sizeEdge++;

    }

    //*************************************************************************************************************
    /**
     * Recupera um vértice do grafo a partir do seu rótulo
     *
     * @param data - texto referente ao rótulo do vértice
     *
     * @return node
     */
//*************************************************************************************************************
    public Node searchNode(String data) {
        for (int i = 0; i < this.getNodeList().size(); i++) {
            Node n = this.getNodeList().get(i);
            if (n.getData().equals(data)) {
                return n;
            }
        }
        return null;
    }

    //*************************************************************************************************************
    /**
     * Ajusta a posição de todos os vértices do grafo conforme a escala do
     * usuário
     *
     */
//*************************************************************************************************************                        
    public void ajustPosition() {

        for (int i = 0; i < this.getNodeList().size(); i++) {
            Node n = this.getNodeList().get(i);
            n.ajust();
        }
        centralizePosition();
    }

    //*************************************************************************************************************
    /**
     * Centraliza dos vértices do grafo nos limites da tela
     *
     */
//*************************************************************************************************************                        
    public void centralizePosition() {
        double maxX = getNodeList().get(0).getX();
        double minX = getNodeList().get(0).getX();
        double maxY = getNodeList().get(0).getY();
        double minY = getNodeList().get(0).getY();

        for (int i = 1; i < nodeList.size(); i++) {
            if (maxX < getNodeList().get(i).getX()) {
                maxX = getNodeList().get(i).getX();
            }

            if (maxY < getNodeList().get(i).getY()) {
                maxY = getNodeList().get(i).getY();
            }

            if (minX > getNodeList().get(i).getX()) {
                minX = getNodeList().get(i).getX();
            }

            if (minY > getNodeList().get(i).getY()) {
                minY = getNodeList().get(i).getY();
            }
        }
        double middleScreenY = Flags.XSCREENSIZE() / 2;
        double middleScreenX = Flags.YSCREENSIZE() / 2;

        double middleGraphY = (minY + maxY) / 2;
        double middleGraphX = (minX + maxX) / 2;

        double diffX = middleGraphX - middleScreenX;
        double diffY = middleGraphY - middleScreenY;

        double x1, y1;

        for (int i = 0; i < nodeList.size(); i++) {
            x1 = getNodeList().get(i).getX();
            getNodeList().get(i).setX(x1 - diffX);

            y1 = getNodeList().get(i).getY();
            getNodeList().get(i).setY(y1 - diffY);
        }
    }

    //*************************************************************************************************************
    /**
     * Iguale a posição dos vértices no grafo passado como parâmetro
     *
     * @param graph - Solução de desenho de grafo
     */
//*************************************************************************************************************     
    public void setPositionNode(GraphBundled graph) {
        ListIterator<Node> listIterator = graph.nodeList.listIterator();
        while (listIterator.hasNext()) {
            Node v = (Node) listIterator.next();
            this.nodeList.get(v.getId()).setX(graph.nodeList.get(v.getId()).getX());
            this.nodeList.get(v.getId()).setY(graph.nodeList.get(v.getId()).getY());
        }
    }

    //*************************************************************************************************************
    /**
     * Calcula a área de um desenho
     *
     * @param graph - Solução de desenho de grafo
     */
//*************************************************************************************************************     
    public void area() {
        double area;

        Point.Double min = new Point.Double(0, 0);
        Point.Double max = new Point.Double(Flags.XSCREENSIZE(), Flags.YSCREENSIZE());

        bound(min, max);

        this.xArea = Math.abs(max.x - min.x);
        this.yArea = Math.abs(max.y - min.y);

        area = (double) Math.sqrt(this.getxArea() * this.getyArea());
        this.setArea(area);
    }

    //*************************************************************************************************************
    /**
     * Método auxiliar que calcula os limites do desenho
     *
     * @param graph - Solução de desenho de grafo
     */
//*************************************************************************************************************        
    private void bound(Point.Double min, Point.Double max) {
        for (int j = 0; j < this.getNodeList().size(); j++) {
            if (min.x > this.getNodeList().get(j).getX()) {
                min.x = this.getNodeList().get(j).getX();
            }

            if (max.x < this.getNodeList().get(j).getX()) {
                max.x = this.getNodeList().get(j).getX();
            }

            if (min.y > this.getNodeList().get(j).getY()) {
                min.y = this.getNodeList().get(j).getY();
            }

            if (max.y < this.getNodeList().get(j).getY()) {
                max.y = this.getNodeList().get(j).getY();
            }
        }
    }

    
    public String showNodePosition(){
        String message = "";
        
        for (int i = 0; i < nodeList.size(); i++) {
            Node v = new Node();
            v = nodeList.get(i);
            message += v.toString();
        }
                
        return message;
    }
    
    //*************************************************************************************************************
    /**
     * Gera um cloneBundlesAndAttributs do grafo
     *
     * @return graph
     */
//*************************************************************************************************************                   
    public Graph clone() {
        Graph graph = new Graph(false);
        Node v;
        Edge e;

        graph.setArea(this.getArea());
        graph.setDirected(this.getDirected());
        graph.setMovedNodes(this.getMovedNodes());

        for (int i = 0; i < nodeList.size(); i++) {
            v = new Node();
            v = nodeList.get(i);
            graph.addNode(v);
        }
        for (int i = 0; i < getEdgeList().size(); i++) {
            e = new Edge();
            e = getEdgeList().get(i);
            graph.addEdge(e);
        }
        return graph;
    }

    public GraphBundled cloneListOfNodeAndEdges() {
        GraphBundled graph = new GraphBundled(false);
        Node v;
        Edge e;

        graph.setArea(this.getArea());
        graph.setDirected(this.getDirected());
        graph.setMovedNodes(this.getMovedNodes());

        for (int i = 0; i < nodeList.size(); i++) {
            v = new Node();
            v = nodeList.get(i);
            graph.addNode(v);
        }
        for (int i = 0; i < getEdgeList().size(); i++) {
            e = new Edge();
            e = getEdgeList().get(i);
            graph.addEdge(e);
        }
        return graph;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getArea() {
        return this.area;
    }

    public boolean getDirected() {
        return isDirected;
    }

    public void setDirected(Boolean isDirected) {
        this.isDirected = isDirected;
    }

    public ArrayList<Node> getNodeList() {
        return nodeList;
    }

    public ArrayList<Edge> getEdgeList() {
        return edgeList;
    }

    public void setSizeNode(int size) {
        this.sizeNode = size;
    }

    public int getSizeNode() {
        return this.sizeNode;
    }

    public void setSizeEdge(int size) {
        this.sizeEdge = size;
    }

    public int getSizeEdge() {
        return this.sizeEdge;
    }

    public void setEdgeList(ArrayList<Edge> edgeList) {
        this.edgeList = edgeList;
    }

    public void setNodeList(ArrayList<Node> nodeList) {
        this.nodeList = nodeList;
    }

    public int getMovedNodes() {
        return movedNodes;
    }

    public void setMovedNodes(int movedNodes) {
        this.movedNodes = movedNodes;
    }

    /**
     * @return the xArea
     */
    public double getxArea() {
        return xArea;
    }

    /**
     * @return the yArea
     */
    public double getyArea() {
        return yArea;
    }
}
