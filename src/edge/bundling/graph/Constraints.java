package edge.bundling.graph;

import edge.bundling.util.Flags;
import edge.bundling.util.GraphData;
import java.util.ArrayList;
import java.util.ListIterator;

public class Constraints {

    
    public static boolean satisfyAdjacencyConstraint(Edge e, Bundle bundle) {
        try {
            ListIterator listIterator = bundle.getEdges().listIterator();
            while (listIterator.hasNext()) {
                Edge edge = new Edge();
                edge = (Edge) listIterator.next();
                if (!edge.isAdjacent(e)) {
                    return false;
                }
            }
        } catch (Exception ex) {
            System.out.println("Erro no processo de constraint");
            System.out.println(ex.getMessage());
            System.out.println(ex.getCause());
        }

        return true;
    }
    
    //***********************************************************************************************************
    /**
     * Compara uma aresta com as arestas de um feixe devolver verdadeiro se ela
     * é compatível com todas do feixe
     *
     * @param e - aresta a ser comparada
     * @param bundle - feixe de arestas
     * @return boolean
     */
//*************************************************************************************************************                  
    public static boolean satisfyConstraint(Edge e, Bundle bundle) {

        try {
            
            ListIterator listIterator = bundle.getEdges().listIterator();
            while (listIterator.hasNext()) {
                Edge edge = new Edge();
                edge = (Edge) listIterator.next();
                if (!Constraints.satisfyConstraint(e, edge)) {
                    return false;
                }
            }
        } catch (Exception ex) {
            System.out.println("Erro no processo de constraint");
            System.out.println(ex.getMessage());
            System.out.println(ex.getCause());
        }

        return true;
    }

    //***********************************************************************************************************
    /**
     * Compara uma aresta com uma lista de arestas devolver verdadeiro se ela é
     * compatível com todas da lista
     *
     * @param e - aresta a ser comparada
     * @param edgesIndex - indice das arestas a serem comparadas
     * @param edgesList - lista de arestas do grafo
     * @return boolean
     */
//*************************************************************************************************************                  
    public static boolean satisfyConstraint(Edge e, ArrayList edgesIndex, ArrayList<Edge> edgeList) {
        ListIterator listIterator = edgesIndex.listIterator();
        while (listIterator.hasNext()) {
            int value = (int) listIterator.next();
            Edge edge = new Edge();
            edge = edgeList.get(value);
            if (!Constraints.satisfyConstraint(e, edge)) {
                return false;
            }
        }
        return true;
    }

    //***********************************************************************************************************
    /**
     * Verifica se duas arestas respeitam os thresholds determinados
     *
     * @param e1 - aresta 1 a ser comparada
     * @param e2 - aresta 2 a ser comparada
     * @return boolean
     */
//*************************************************************************************************************   
    public static boolean satisfyConstraint(Edge e1, Edge e2) {
        double comp;
        boolean testCompatibility = true;

        if (Flags.isANGULAR()) {
            comp = GraphData.getCompatibilityAngular(e1.getId(), e2.getId());
            if (comp < Flags.COMPATIBILITY_MAX_ANGULAR_THRESHOLD()) {
                testCompatibility = false;
            }
            if (comp > Flags.COMPATIBILITY_MIN_ANGULAR_THRESHOLD()) {
                testCompatibility = false;
            }
        }

        if (Flags.isPOSITION()) {
            comp = GraphData.getCompatibilityPosition(e1.getId(), e2.getId());
            if (comp < Flags.COMPATIBILITY_MAX_POSITION_THRESHOLD()) {
                testCompatibility = false;
            }
        }

        if (Flags.isSCALE()) {
            comp = GraphData.getCompatibilityScale(e1.getId(), e2.getId());
            if (comp < Flags.COMPATIBILITY_MAX_SCALE_THRESHOLD()) {
                testCompatibility = false;
            }
        }

        if (Flags.isVISIBILITY()) {
            comp = GraphData.getCompatibilityVisibility(e1.getId(), e2.getId());
            if (comp < Flags.COMPATIBILITY_MAX_VISIBILITY_THRESHOLD()) {
                testCompatibility = false;
            }
        }

        return testCompatibility;
    }
}
