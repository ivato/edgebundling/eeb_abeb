package edge.bundling.graph;

import edge.bundling.util.Flags;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

public class Node {

    private int id;
    private String data;
    private double xOriginal;                   //Posição original x do vértice
    private double yOriginal;                   //Posição original y do vértice
    private double x;                           //Posição x do vértice
    private double y;                           //Posição y do vértice
    private boolean cover = false;
    
    private LinkedList<Edge> adjEdgeList;       //Lista de arestas adjacentes do vértice
    private LinkedList<Node> adjNodeList;       //Lista de vértices adjacentes do vértice
    private int numberOfEdges = 0;

    public Node() {
        this.id = 0;
        this.data = "";
        this.xOriginal = 0;
        this.yOriginal = 0;
        this.x = 0;
        this.y = 0;
        this.adjEdgeList = new LinkedList<Edge>();
        this.adjNodeList = new LinkedList<Node>();
        this.numberOfEdges = 0;
        this.cover = false;
    }

    public Node(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Node(int id, String data, double x, double y) {
        this.id = id;
        this.data = data;
        this.xOriginal = x;
        this.yOriginal = y;
        this.x = x;
        this.y = y;
        this.adjEdgeList = new LinkedList<Edge>();
        this.adjNodeList = new LinkedList<Node>();
        this.numberOfEdges = 0;
        this.cover = false;
    }

    public int getEdgesListOfANode(List adjList) {
        for (int w = 0; w < getAdjEdgeList().size(); w++) {
            adjList.add(w);
        }
        int size = adjList.size();
        return size;
    }

    public int getEdgesListOfANode(LinkedList<Edge> adjList) {
        for (int w = 0; w < getAdjEdgeList().size(); w++) {
            adjList.add(getAdjEdgeList().get(w));
        }
        int size = adjList.size();
        return size;
    }

    public Point.Double nodePositionOnEdge(Edge edge) {
        double x1 = 0, y1 = 0;

        if (this != edge.getStartNode()) {
            x1 = edge.getStartNode().getX();
            y1 = edge.getStartNode().getY();
        } else {
            x1 = edge.getEndNode().getX();
            y1 = edge.getEndNode().getY();
        }

        return (new Point.Double(x1, y1));
    }

    //*************************************************************************************************************
    /**
     * Calcula a distância entre dois vértices
     *
     * @param node - segundo vértice
     */
//*************************************************************************************************************               
    public double distance(Node other) {
        Edge edge = new Edge(this, other);
        return edge.distance();
    }

    //*************************************************************************************************************
    /**
     * Adiciona uma aresta à lista de arestas adjacentes do vértice
     *
     * @param edge - aresta a ser adicionada
     */
//*************************************************************************************************************               
    public void addAdjEdge(Edge edge) {

        if (!this.adjEdgeList.contains(edge)) {
            numberOfEdges++;
            adjEdgeList.add(edge);
            if (this != edge.getStartNode()) {
                adjNodeList.add(edge.getStartNode());
            } else {
                adjNodeList.add(edge.getEndNode());
            }
        }

    }

    //*************************************************************************************************************
    /**
     * Remove um vértice da lista de nós adjacentes do vértice
     *
     * @param node - vértice a ser removido
     */
//*************************************************************************************************************                   
    public void removeAdjNode(Node node) {
        adjNodeList.remove(node);
    }

    //*************************************************************************************************************
    /**
     * Ajusta a posição do vértice conforme a escala do usuário
     *
     */
//*************************************************************************************************************                      
    public void ajust() {
        this.x = (this.x * Flags.SCALE()) / Flags.PREVIOUSSCALE();
        this.y = (this.y * Flags.SCALE()) / Flags.PREVIOUSSCALE();

    }

    //*************************************************************************************************************
    /**
     * Retorna os pontos do feixo convexo entre os vértices adjacentes
     *
     */
//*************************************************************************************************************               
    public ArrayList<Point.Double> getConvexHullPoints() {
        ArrayList<Point.Double> points = new ArrayList<Point.Double>();
        Point.Double point = new Point.Double();

        for (Node n : getAdjNodeList()) {
            point = new Point.Double(n.getX(), n.getY());
            points.add(point);
        }
        return points;
    }

    //*************************************************************************************************************
    /**
     * Retorna a lista de vértices adjacentes ordenados por grau
     *
     */
//*************************************************************************************************************               
    public List<Map.Entry<Integer, Integer>> orderedAdjacentNodes() {
        Map<Integer, Integer> listNodes = new HashMap<Integer, Integer>();
        for (Node n : this.getAdjNodeList()) {
            listNodes.put(n.getId(), n.getNumberOfEdges());
        }
        List<Map.Entry<Integer, Integer>> entryListNodes = new ArrayList<Map.Entry<Integer, Integer>>(listNodes.entrySet());
        Collections.sort(entryListNodes, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> integerEntry, Map.Entry<Integer, Integer> integerEntry2) {
                return integerEntry2.getValue().compareTo(integerEntry.getValue());
            }
        });
        return entryListNodes;
    }

    //*************************************************************************************************************
    /**
     * Gera um clone do vértice
     *
     */
//*************************************************************************************************************               
    public Node clone() {
        Node clone = new Node();
        clone.id = this.id;
        clone.data = this.data;
        clone.x = this.x;
        clone.y = this.y;
        clone.xOriginal = this.xOriginal;
        clone.yOriginal = this.yOriginal;
        clone.numberOfEdges = this.getNumberOfEdges();
        clone.cover = this.cover;

        ListIterator<Node> listNodeIterator = this.getAdjNodeList().listIterator();
        while (listNodeIterator.hasNext()) {
            Node node = new Node();
            node = listNodeIterator.next();
            clone.adjNodeList.add(node);
        }

        ListIterator<Edge> listEdgeIterator = this.getAdjEdgeList().listIterator();
        while (listEdgeIterator.hasNext()) {
            Edge edge = new Edge();
            edge = listEdgeIterator.next();
            clone.adjEdgeList.add(edge);
        }
        return clone;
    }
    
    @Override
    public String toString(){
        
        return "(" + id + ")["+ x + "," + y + "]\n";
    }

    public int getId() {
        return id;
    }

    public String getData() {
        return data;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public LinkedList<Edge> getAdjEdgeList() {
        return adjEdgeList;
    }

    public LinkedList<Node> getAdjNodeList() {
        return adjNodeList;
    }

    public int getNumberOfEdges() {
        return numberOfEdges;
    }

    public double getxOriginal() {
        return xOriginal;
    }

    public void setxOriginal(double xOriginal) {
        this.xOriginal = xOriginal;
    }

    public double getyOriginal() {
        return yOriginal;
    }

    public void setyOriginal(double yOriginal) {
        this.yOriginal = yOriginal;
    }

    public int getXInt() {
        return (int) Math.round(x);
    }

    public int getYInt() {
        return (int) Math.round(y);
    }

    public void setCover(boolean cover) {
        this.cover = cover;
    }

    public boolean isCover() {
        return this.cover;
    }
   
}
