package edge.bundling.graph;

import edge.bundling.compatibility.Compatibility;
import edge.bundling.util.Flags;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public class Bundle {

    private int id;
    private Node centerNode = new Node();

    private double compatibility = 0.0;
    private double caCompatibility = 0.0;
    private double csCompatibility = 0.0;
    private double cpCompatibility = 0.0;
    private double cvCompatibility = 0.0;

    private int checksum = 0;
    private int penalty = 0;
    private int size = 0;
    private LinkedList<Edge> edgeList = new LinkedList();

      
    
    //*************************************************************************************************************
    /**
     * Calcula o checksum do feixe
     *
     * @return int
     */
//*************************************************************************************************************                           
    public int checksum() {
        checksum = 1;
        Iterator<Edge> iterator = edgeList.iterator();
        while (iterator.hasNext()) {
            Edge e = (Edge) iterator.next();
            checksum *= (e.getId()+1);
        }
        return checksum;
    }

    //*************************************************************************************************************
    /**
     * Adiciona uma aresta ao feixe
     *
     * @param edge - aresta a ser adicionada
     */
//*************************************************************************************************************                           
    public void add(Edge edge) {
        if (!edgeList.contains(edge)) {
            edgeList.add(edge);
            size++;
        }
    }

    //*************************************************************************************************************
    /**
     * Remove uma aresta do feixe
     *
     * @param edge - aresta a ser removida
     */
//*************************************************************************************************************                              
    public void remove(Edge edge) {

        if (edgeList.contains(edge)) {
            edgeList.remove(edge);
            size--;

            if (size > 0) {
                this.evaluate();
                //System.out.print("Yeappie");
            } else {
                //System.out.print("Yeap");
                compatibility = 0.0;
                centerNode = new Node();
                checksum = 0;
            }
        }
    }

    //*************************************************************************************************************
    /**
     * Calcula os parâmetros: vértice central e compatibilidade
     *
     */
//*************************************************************************************************************                           
    public void evaluate() {
        //Calcula o vértice central a compatibilidade e a posição do ponto de controle
        this.centerNode();
        this.compatibility();
    }

    //*************************************************************************************************************
    /**
     * Calcula o vértice comum entre as arestas do feixe
     *
     */
//*************************************************************************************************************                   
    public void centerNode() {

        ListIterator<Edge> edgeIterator = edgeList.listIterator();
        Edge edge1 = new Edge();
        Edge edge2 = new Edge();

        edge1 = (Edge) edgeIterator.next();

        if (edgeList.size() > 1) {
            edge2 = (Edge) edgeIterator.next();
            centerNode = edge1.commonNode(edge2);
        } else {
            if (edgeList.size() == 1) {
                centerNode = edge1.getStartNode();
            }
        }
    }

    //*************************************************************************************************************
    /**
     * Calcula a medida de compatibilidade do feixe
     *
     */
//*************************************************************************************************************                    
    public void compatibility() {
        Compatibility c = new Compatibility();
        this.compatibility = 0;
        this.compatibility = c.compatibility(this);  //Calcula a compatibilidade levando em consideração todas as restriçoes

        //CBEB
        /*if (Flags.isOBJECTIVE()) {
            this.caCompatibility = c.compatibilityAngular(this);
            this.csCompatibility = c.compatibilityScale(this);
            this.cpCompatibility = c.compatibilityPosition(this);
        }*/
    }

    //*************************************************************************************************************
    /**
     * Gera um clone do feixe
     *
     * @return bundle
     */
//*************************************************************************************************************               
    public Bundle clone() {
        Bundle clone = new Bundle();
        Node node = new Node();

        ListIterator<Edge> edgeIterator = edgeList.listIterator();
        while (edgeIterator.hasNext()) {
            Edge edge = new Edge();
            edge = (Edge) edgeIterator.next();
            clone.add(edge);
        }

        clone.size = size;
        clone.centerNode = centerNode;
        clone.id = this.id;
        clone.compatibility = this.compatibility;                 //Compatibilidade mínima entre cada feixe dois a dois
        clone.checksum = this.checksum;
        return clone;
    }

    //*************************************************************************************************************
    /**
     * Mostra a lista dos feixes
     *
     * @return String
     */
//*************************************************************************************************************                                   
    public String showBundle() {
        String message = "";
        message += this.getId() + ": ";
        int i = 1;

        ListIterator<Edge> listIterator = this.edgeList.listIterator();
        while (listIterator.hasNext()) {
            message += listIterator.next().getId();
            if (i != this.edgeList.size()) {
                message += " - ";
                i++;
            }
        }
        message += " [C:" + this.getCompatibility() + "] (CN:" + this.centerNode.getId() + ") {S:" + this.size + "}\n";
        return message;
    }

    //*************************************************************************************************************
    /**
     * Gera dados para o LOG
     *
     * @return String
     */
//*************************************************************************************************************                                       
    public String showBundleLog() {
        String message = "";
        message += this.getId() + ": ";
        int i = 1;

        ListIterator<Edge> listIterator = this.edgeList.listIterator();
        while (listIterator.hasNext()) {
            message += listIterator.next().getId();
            if (i != this.edgeList.size()) {
                message += " - ";
                i++;
            }
        }
        message += "\n";
        return message;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setCenterNode(Node centerNode) {
        this.centerNode = centerNode;
    }

    public Node getCenterNode() {
        return centerNode;
    }

    public LinkedList<Edge> getEdges() {
        return edgeList;
    }

    public int getSize() {
        return size;
    }

    public double getCompatibility() {
        return compatibility;
    }

    public double getCaCompatibility() {
        return caCompatibility;
    }

    public double getCsCompatibility() {
        return csCompatibility;
    }

    public double getCpCompatibility() {
        return cpCompatibility;
    }

    public double getCvCompatibility() {
        return cvCompatibility;
    }

    public int getPenalty() {
        return penalty;
    }

    public void setPenalty(int penalty) {
        this.penalty = penalty;
    }

    public int getChecksum() {
        return checksum;
    }
}
