package edge.bundling.graph;

import edge.bundling.methods.genetic.Population;
import edge.bundling.util.Flags;
import edge.bundling.util.Utils;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

public class GraphBundled extends Graph {

    protected ArrayList<Bundle> bundleList = new ArrayList();
    protected List coverList = new ArrayList();
    protected double compatibility = 0.0;

    private double aCompatibility = 0.0;
    private double pCompatibility = 0.0;
    private double sCompatibility = 0.0;

    protected int numberOfBundles = 0;
    protected int numberOfBundlesMoreThanOne = 0;
    protected int numberOfBundlesEqualOne = 0;
    protected int bundleIndex = 0;

    protected double density = 0.0;
    protected double fitness = 0.0;
    protected int fitnessRank = -1;
    private int checksum = 0;
    private int crossed = 0;
    private int withCover = 0;

    //public int contMakeFeasibleInitial = 0;
    //public int contMakeFeasibleArestasSoltas = 0;
    //public int contMakeFeasibleArestasDuplicadas = 0;

     
        
    public GraphBundled(boolean direct) {
        super(direct);
    }

    
    public boolean checksum(GraphBundled graph) {
     if (this.getBundleList().size() != graph.getBundleList().size()){
                return false;
            }else{
               if (this.getFitness() != graph.getFitness()){
                    return false;
                }else{
                  if (this.checksum() != graph.checksum()){
                      return false;
                  }
               }
            }
         return true;   
     }
   
     
    public int checksum() {
        checksum = 0;
        Iterator<Bundle> iterator = bundleList.iterator();
        while (iterator.hasNext()) {
            Bundle b = (Bundle) iterator.next();
            checksum += b.checksum();
        }

        return getChecksum();
    }

    private void resetAttributes() {
        bundleList = new ArrayList();
        coverList = new ArrayList();
        compatibility = 0.0;
        aCompatibility = 0.0;
        pCompatibility = 0.0;
        sCompatibility = 0.0;
        numberOfBundles = 0;
        numberOfBundlesMoreThanOne = 0;
        numberOfBundlesEqualOne = 0;
        bundleIndex = 0;
        density = 0.0;
        fitness = 0.0;
        fitnessRank = -1;
    }

    //*************************************************************************************************************
    /**
     * Retira os subseguimentos das aresas
     *
     */
//*************************************************************************************************************               
    public void resetEdgesSubdivigion() {
        super.resetSubNodesList();
        resetAttributes();
    }

    //*************************************************************************************************************
    /**
     * Reinicia a posição dos vértices para a posição original
     *
     */
//*************************************************************************************************************               
    public void resetNodesPosition() {
        super.resetNodesPosition();
        resetAttributes();
    }

    //*************************************************************************************************************
    /**
     * Monta feixes aleatórios sem usar a cobertura
     *
     * @param graph - grafo original
     */
//*************************************************************************************************************               
    public void createBundleWithoutCoverAndRandom(Graph graph) {

        //Vetor que controlará a montagem do feixe, possui 0 para arestas não usadas e 1 para usadas
        int nrEdges = graph.getEdgeList().size();
        int[] visited = new int[nrEdges+1];
        for (int t = 0; t < visited.length; t++) {
            visited[t] = 0;
        }
        createRandomBundles(graph, visited);
    }

    //*************************************************************************************************************
    /**
     * Monta feixes aleatórios usando critério de compatibilidade e a cobertura
     *
     * @param graph - grafo original
     */
//*************************************************************************************************************                    
    public void createBundleWithCoverAndRandom(Graph graph) throws Exception {

        this.setCoverList(Flags.COVERLIST());

        this.sizeEdge = graph.getEdgeList().size();
        this.sizeNode = graph.getNodeList().size();
        this.setEdgeList(graph.getEdgeList());
        this.setNodeList(graph.getNodeList());

        //double x = 0, y = 0, x1 = 0, y1 = 0, x2 = 0, y2 = 0;
        Point.Double point = new Point.Double(0, 0);
        double comp = 0;
        bundleIndex = 0;
        Random rndSeed = new Random(System.currentTimeMillis());
        
        int visited[] = new int[graph.getSizeEdge() + 1];
        for (int t = 0; t < visited.length; t++) {
            visited[t] = 0;
        }
        
        //Controla a quantidade de vezes que repete o processo
        for (int s = 0; s < this.sizeEdge; s++) {

            //Monta uma lista com o índice dos vértices pertencentes à cobertura
            List nodeCoverList = new ArrayList();
            for (int indexOfNode = 0; indexOfNode < coverList.size(); indexOfNode++) {
                nodeCoverList.add(indexOfNode);
            }
            int size = nodeCoverList.size();

            int k = 0;
            while (k < size) {

                k++;

                //Embaralha a lista de vértices da cobertura     
                //Recupera o índice do próximo nó da cobertura 
                Collections.shuffle(nodeCoverList);
                int indexOfVertice = (int) nodeCoverList.remove(0);

                //Decide se vai para a proxima iteração e não usa o vértice selecionado
                if (Utils.escape(rndSeed, 22745621, 5)) {
                    continue;
                }

                //Recupera o próximo nó da cobertura de acordo com o indice selecionado e monta um feixe para eele
                int i = (int) coverList.get(indexOfVertice);
                Node node = graph.getNodeList().get(i);

                if (node.getAdjEdgeList().size() != 0) {

                    //Monta o feixe recuperando uma das arestas adjacentes do nó que será a referência
                    //e as demais arestas comparadas a esta (aleatoriamente)     
                    List edgesOfNodeRandomList1 = new ArrayList();
                    int size1 = node.getEdgesListOfANode(edgesOfNodeRandomList1);

                    int t = 0;
                    Edge e1;
                    do {
                        t++;

                        //Embaralha a lista de arestas adjacentes do nó 
                        //para escolher a aresta de referência de forma aleatória    
                        //Escolhe a primeira aresta da lista de arestas do nó
                        Collections.shuffle(edgesOfNodeRandomList1);
                        int indexOfEdge = (int) edgesOfNodeRandomList1.remove(0);
                        e1 = (Edge) node.getAdjEdgeList().get(indexOfEdge);
                        
                        //Verifica se a aresta já foi usada
                        if (visited[e1.getId()] == 0) {
                            
                            //Decide se vai para a proxima iteração e não usa a aresta selecionada
                            if (Utils.escape(rndSeed, 38743921, 5)) {
                                continue;
                            }

                            //Cria um feixe e coloca a aresta neste feixe;
                            Bundle bundle = new Bundle();
                            bundle.add(e1);
                            visited[e1.getId()] = 1;
                            visited[visited.length-1] += 1;
                            
                            //Compara a aresta selecionada com as próximas adjacentes
                            //Havendo compatibilidade adiciona as outras arestas no feixe
                            List edgesOfNodeRandomList2 = new ArrayList();
                            int size2 = node.getEdgesListOfANode(edgesOfNodeRandomList2);
                            
                            int t2 = 0;
                            do {
                                t2++;

                                //Embaralha a lista de arestas adjacentes do nó 
                                //Recupera a próxima aresta 
                                Collections.shuffle(edgesOfNodeRandomList2);
                                indexOfEdge = (int) edgesOfNodeRandomList2.remove(0);

                                Edge e2 = (Edge) node.getAdjEdgeList().get(indexOfEdge);
                                if (e1 != e2) {
                                    if (visited[e2.getId()] == 0) {
                                        
                                        //Decide se vai para a proxima iteração e não usa a aresta selecionada
                                        if (Utils.escape(rndSeed, 15708201, 5)) {
                                            continue;
                                        }
                                
                                        //Caso a aresta não tenha sido usada 
                                        //e o fator de compatibilidade esteja adequado a insere no feixe  
                                        if (Constraints.satisfyConstraint(e2, bundle)) {
                                            bundle.add(e2);
                                            visited[e2.getId()] = 1;
                                            visited[visited.length-1] += 1;
                                        }
                                    }
                                }
                            } while (t2 < size2);

                            //Insere o feixe no novo grafo
                            this.add(bundle);
                            
                            if(visitedAllEdged(visited)) break;
                        }
                        if(visitedAllEdged(visited)) break;
                    } while (t < size1);
                }
                if(visitedAllEdged(visited)) break;
            }
            if(visitedAllEdged(visited)) break;
        }

        //Cria feixes aleatórios com as arestas não usadas
        if(!visitedAllEdged(visited)){
            createRandomBundles(graph, visited);
        }
        else{
            if (visited[visited.length-1] != this.sizeEdge){           
                makeFeasible();
                //contMakeFeasibleInitial++;
            }
            //makeFeasible();
        }      
        this.setWithCover(1);
    }

    //*************************************************************************************************************
    /**
     * Procura um outro feixe com o mesmo vértice central
     *
     * @param bundle1 - feixe a ser comparado
     *
     */
//*************************************************************************************************************               
    public Bundle searchBundleSameNode(Bundle bundle1) {

        Bundle bundle2 = null;
        ListIterator<Bundle> bundleIterator = this.getBundleList().listIterator();
        while (bundleIterator.hasNext()) {
            bundle2 = new Bundle();
            bundle2 = (Bundle) bundleIterator.next();
            if (bundle1 != bundle2) {
                if (bundle1.getCenterNode() == bundle2.getCenterNode()) {
                    return bundle2;
                }
            }
        }
        return null;
    }

    //*************************************************************************************************************
    /**
     * Cria feixes aleatórios
     *
     * @param graph - grafo original
     * @param visited - vetor de arestas já usadas
     */
//*************************************************************************************************************               
    public void createRandomBundles(Graph graph, int[] visited) {
        int indexOfNode;
        int idEdge = 0;
        int qtEdge = 0;
        Bundle bundle;
        
        ArrayList<Node> nodeList = graph.getNodeList();
        this.sizeEdge = graph.getEdgeList().size();
        this.sizeNode = graph.getNodeList().size();
        this.setEdgeList(graph.getEdgeList());
        this.setNodeList(graph.getNodeList());
        this.bundleIndex = 0;
        
        Random rndSeed = new Random(System.currentTimeMillis());

        //Controla a quantidade de vezes que repete o processo
        for (int k = 0; k < this.sizeEdge; k++) {

            //Array que armazena de forma ordenada o índice identificador dos nós do grafo
            //Todos os vértices são colocados nesta lista
            ArrayList indexNodeList = new ArrayList();
            for (int indexOfVertice = 0; indexOfVertice < nodeList.size(); indexOfVertice++) {
                indexNodeList.add(indexOfVertice);
            }

            
            //Para cada vértice da lista tenta-se montar um feixe com as 
            //suas arestas adjacente ainda não usadas
            for (int i = 0; i < nodeList.size(); i++) {

                //Embaralha a lista de vértices do grafo e recupera um deles aleatoriamente
                Collections.shuffle(indexNodeList);
                indexOfNode = (int) indexNodeList.remove(0);
                Node node = nodeList.get(indexOfNode);

                
                //Decide se usará ou não este vértice, caso não use reinicia o
                //laço escolhendo um novo vértice (o vértice poderá ou não
                //ser escolhido nas próximas passagens)
                if (Utils.escape(rndSeed, 25733625, 5)) {
                    continue;
                }

                bundle = new Bundle();

                //Recupera a lista de arestas adjacentes do escolhido
                LinkedList<Edge> adjList = new LinkedList<Edge>();
                qtEdge = node.getEdgesListOfANode(adjList);

                
                //Para cada aresta da lista verifica se ela pode ou não se colocada no feixe
                //liderado pelo vértice escolhido
                for (int j = 0; j < qtEdge; j++) {

                    //Embaralha a lista de arestas adjacentes e recupera uma delas
                    Collections.shuffle(adjList);
                    Edge edge = (Edge) adjList.remove(0);
                    idEdge = (int) edge.getId();
                    
                    //Se a aresta não foi usada ainda
                    if (visited[idEdge] == 0) {
                    
                        //Decide se usará ou não esta aresta no feixe, caso não use reinicia o
                        //laço escolhendo um uma nova aresta adjacente  
                        if (Utils.escape(rndSeed, 19700621, 5)) {
                            continue;
                        }
                        
                        //Se o feixe estiver vazio insere a aresta escolhida
                        //Caso contrário testa a compatibilidade da aresta antes de inserir se o sistema 
                        //Estiver setado como CONSTRAINTED, ou simplesmente insere a aresta caso contrário
                        if (bundle.getSize() == 0){ 
                                bundle.add(edge);
                                visited[idEdge] = 1;
                                visited[visited.length-1] += 1;
                        }else{
                            if (Flags.isCONSTRAINT()) {
                                if (Constraints.satisfyConstraint(edge, bundle)) {
                                        bundle.add(edge);
                                        visited[idEdge] = 1;
                                        visited[visited.length-1] += 1;
                                }
                            } else {
                                if (Constraints.satisfyAdjacencyConstraint(edge, bundle)){
                                    bundle.add(edge);
                                    visited[idEdge] = 1;
                                    visited[visited.length-1] += 1;
                                }
                            }
                        }
                    }
                    
                    //Caso já tenha usado todas as arestas do grafo para o laço 
                    //principal que controla o número de repetições
                    if(visitedAllEdged(visited)) break;
                    
                    //Neste ponto pode ser tomada a decisão de parar a montagem 
                    //do feixe e incluí-lo como esta no indivíduo
                    //Dexa-o como esta e inicia um novo feixe selecionando um novo vértice
                    if (Utils.escape(rndSeed, 30740721, 5)) {
                        break;
                    }
                }

                //Adiciona o feixe no indivíduo
                if (bundle.getEdges().size() != 0) {
                    this.add(bundle);
                }
                           
                if(visitedAllEdged(visited)) break;
            }
            
            if(visitedAllEdged(visited))  break;
        }

        //Torna o indivíduo viavél
        //As arestas não usadas formarão um feixe único cada
        //Arestas usadas mais de uma vez serão deletas e mantidas em apenas um dos feixes
        if (visited[visited.length-1] != this.sizeEdge){           
            makeFeasible();
            //contMakeFeasibleInitial++;
        }
        //makeFeasible();
    }
    
    
     //*************************************************************************************************************
    /**
     * Método auxiliar para chegar se todas as arestas foram colocadas em um
     * feixe 
     *
     * @param visited[] - lista de arestas já usadas em feixes
     */
//*************************************************************************************************************
    public boolean visitedAllEdged(int[] visited){
        if(visited[visited.length-1] == (visited.length-1))
            return true;
        return false;
    }
 
    //*************************************************************************************************************
    /**
     * Torna o indivíduo factível eliminando arestas duplicadas em vários feixes
     * e montando feixes com arestas não usadas
     *
     * @param sizeEdgeGraph - quantidade de arestas do grafo
     * @param edgeList - lista de arestas do grafo
     */
//*************************************************************************************************************  
    public void makeFeasible() {
        int[] control = new int[this.sizeEdge];
        
        //for (int i = 0; i < this.sizeEdge; i++) {
        //    control[i] = 0;
        //}
        
        //Marca todas as arestas usadas em feixes como 1
        ListIterator<Bundle> listIteratorBundle = this.getBundleList().listIterator();
        while (listIteratorBundle.hasNext()) {
            Bundle bundle = (Bundle)listIteratorBundle.next();
            
            ListIterator<Edge> listIterator = bundle.getEdges().listIterator();
            while (listIterator.hasNext()) {
                Edge edge = (Edge) listIterator.next();
                control[edge.getId()] += 1;
            }          
        }
        
        
        for (int i = 0; i < this.sizeEdge; i++) {
            //Para cada aresta marcada como 0 cria um feixe único
            if (control[i] == 0){
                Edge e = (Edge) this.getEdgeList().get(i);
                Bundle bundle = new Bundle();
                bundle.add(e);
                this.add(bundle);
                //contMakeFeasibleArestasSoltas++;
    
                //Se uma aresta foi usada mais de uma vez deixa-a apenas em um dos feixes
                if (control[i] > 1){   
                    removeRepeatedEdge(i, control[i]);
                    //contMakeFeasibleArestasDuplicadas++;
                }
            }
        }
    }
    
    
    public void removeRepeatedEdge(int idEdge, int size){
        
        while (size != 1){
            for (int i = 0; i < this.getNumberOfBundles(); i++) {
                Bundle bundle = new Bundle();
                bundle = this.getBundle(i);

                ListIterator<Edge> listIterator = bundle.getEdges().listIterator();
                while (listIterator.hasNext()) {
                    Edge edge = (Edge) listIterator.next();
                    int id = edge.getId();
                    if (id == idEdge){
                        if (bundle.getSize() == 1){
                            this.removeBundle(bundle);
                            size--;
                            break;
                        }
                        else{
                            bundle.remove(edge);
                            size--;
                            break;
                        }
                    }
                }
                if (size == 1){
                    break;
                }
            }
        }
    }

   

    //*************************************************************************************************************
    /**
     * Adiciona um feixe ao grafo
     *
     * @param bundle - feixe a ser adiciodado
     */
//*************************************************************************************************************               
    public void add(Bundle bundle) {
        bundle.setId(bundleIndex);
        bundle.evaluate();

        bundleList.add(bundle);

        if (bundle.getSize() == 1) {
            numberOfBundlesEqualOne++;
        } else {
            numberOfBundlesMoreThanOne++;
        }

        numberOfBundles++;
        bundleIndex++;
    }

    //*************************************************************************************************************
    /**
     * Remove um feixe do grafo
     *
     * @param bundle - feixe a ser removido
     */
//*************************************************************************************************************                 
    public void removeBundle(Bundle bundle) {
        if (bundleList.contains(bundle)) {
            bundleList.remove(bundle);

            compatibility -= bundle.getCompatibility();

            if (bundle.getSize() == 1) {
                numberOfBundlesEqualOne--;
            } else {
                numberOfBundlesMoreThanOne--;
            }

            numberOfBundles--;
        }
    }

    //*************************************************************************************************************
    /**
     * Calcula a quantidade de feixes com mais de uma aresta
     *
     * @return numberOfBundlesMoreThanOne
     */
//*************************************************************************************************************                   
    protected int bundlesMoreThanOneEdge() {
        int size = 0;
        ListIterator<Bundle> listIterator = this.bundleList.listIterator();
        while (listIterator.hasNext()) {
            if (listIterator.next().getSize() > 1) {
                size++;
            }
        }
        this.numberOfBundlesMoreThanOne = size;
        return this.numberOfBundlesMoreThanOne;
    }

    //*************************************************************************************************************
    /**
     * Calcula a quantidade de feixes uma aresta
     *
     * @return numberOfBundlesEqualOne
     */
//*************************************************************************************************************                   
    protected int bundlesEqualOneEdge() {
        int size = getNumberOfBundles() - getNumberOfBundlesMoreThanOne();

        this.numberOfBundlesEqualOne = size;
        return this.numberOfBundlesEqualOne;
    }

    //*************************************************************************************************************
    /**
     * Calcula o valor da compatibilidade total do grafo
     *
     * @return compatibility
     */
//*************************************************************************************************************                      
    protected double compatibility() {

        this.compatibility = 0;
        this.aCompatibility = 0;
        this.sCompatibility = 0;
        this.pCompatibility = 0;

        ListIterator<Bundle> listIterator = this.bundleList.listIterator();
        while (listIterator.hasNext()) {
            Bundle bundle = new Bundle();
            bundle = listIterator.next();
            compatibility += bundle.getCompatibility();

            if (Flags.isOBJECTIVE()) {
                aCompatibility += bundle.getCaCompatibility();
                sCompatibility += bundle.getCsCompatibility();
                pCompatibility += bundle.getCpCompatibility();
            }
        }

        return this.compatibility;
    }

    //*************************************************************************************************************
    /**
     * Calcula o Fitness do indivíduo
     *
     * @return fitness
     */
//*************************************************************************************************************                    
    protected double fitness(Population population) {

        this.compatibility();
        this.bundlesMoreThanOneEdge();
        this.bundlesEqualOneEdge();

        this.density = (double) ((double) this.getNumberOfBundlesMoreThanOne() / (double) this.getSizeEdge());

        this.fitness = 0;
        double bundlesTotal;

        if (this.getBundleList().size() == this.getSizeEdge()) {

            this.fitness = 0;

        } else if (this.getNumberOfBundlesMoreThanOne() != 0) {

            bundlesTotal = this.getNumberOfBundles();

            if (Flags.isOBJECTIVE()) {

                this.fitness = (double) (Flags.COMPATIBILITYYWEIGHT() * this.getCompatibility())
                        + (Flags.BUNDLESYWEIGHT() * (1 / bundlesTotal));

            } else {
                if (Flags.isCONSTRAINT()) {

                    this.fitness = (double) (1 / bundlesTotal);

                }
            }
        }
        return this.fitness;
    }

    //*************************************************************************************************************
    /**
     * Retorna um feixe
     *
     * @param bundleNumber - número do feixe
     * @return bundle
     */
//*************************************************************************************************************                 
    public Bundle getBundle(int bundleNumber) {
        Bundle bundle = new Bundle();
        bundle = bundleList.get(bundleNumber);
        return bundle;
    }

    //*************************************************************************************************************
    /**
     * Gera o cloneBundlesAndAttributs do grafo
     *
     * @return cloneBundlesAndAttributs
     */
//*************************************************************************************************************     
    public GraphBundled cloneListOfNodeAndEdges() {
        GraphBundled clone = new GraphBundled(this.getDirected());
        ArrayList<Bundle> bundleList = new ArrayList();
        List coverList = new ArrayList();
        Node v;
        Edge e;

        //Copia os vértices
        for (int i = 0; i < nodeList.size(); i++) {
            v = new Node();
            v = nodeList.get(i);
            clone.addNode(v);
        }

        //Copia as arestas
        for (int i = 0; i < getEdgeList().size(); i++) {
            e = new Edge();
            e = getEdgeList().get(i);
            clone.addEdge(e);
        }

        //Copia os feixes
        ListIterator<Bundle> listIterator = this.bundleList.listIterator();
        while (listIterator.hasNext()) {
            Bundle bundleTemp = new Bundle();
            bundleTemp = listIterator.next();
            ListIterator<Edge> listIteratorEdge = bundleTemp.getEdges().listIterator();
            Bundle bundle = new Bundle();
            while (listIteratorEdge.hasNext()) {
                Edge edge = (Edge) listIteratorEdge.next();
                bundle.add(edge);
            }
            bundleList.add(bundle);
        }

        //Copia coverlist
        ListIterator listIterator2 = this.coverList.listIterator();
        while (listIterator2.hasNext()) {
            coverList.add((int) listIterator2.next());
        }

        //Copia atributos
        clone.area = this.getArea();
        clone.isDirected = this.isDirected;
        clone.movedNodes = this.movedNodes;
        clone.bundleList = bundleList;
        clone.coverList = coverList;
        clone.fitness = this.fitness;
        clone.compatibility = this.compatibility;
        clone.numberOfBundles = this.getNumberOfBundles();
        clone.numberOfBundlesMoreThanOne = this.getNumberOfBundlesMoreThanOne();
        clone.numberOfBundlesEqualOne = this.getNumberOfBundlesEqualOne();
        clone.bundleIndex = this.bundleIndex;
        clone.density = this.density;
        clone.fitnessRank = this.fitnessRank;
        clone.aCompatibility = this.aCompatibility;
        clone.pCompatibility = this.pCompatibility;
        clone.sCompatibility = this.sCompatibility;
        clone.fitnessRank = this.fitnessRank;
        clone.bundleIndex = this.bundleIndex;
        clone.crossed = this.crossed;
        clone.withCover = this.withCover;

        return clone;
    }

    public GraphBundled clone() {
        GraphBundled clone = new GraphBundled(this.getDirected());
        
        clone = this.cloneListOfNodeAndEdges();
        
        ArrayList<Bundle> bundleList = new ArrayList();
        List coverList = new ArrayList();

        ListIterator<Bundle> listIterator = this.bundleList.listIterator();
        while (listIterator.hasNext()) {
            Bundle bundleTemp = new Bundle();
            bundleTemp = listIterator.next();
            ListIterator<Edge> listIteratorEdge = bundleTemp.getEdges().listIterator();
            Bundle bundle = new Bundle();
            while (listIteratorEdge.hasNext()) {
                Edge edge = (Edge) listIteratorEdge.next();
                bundle.add(edge);
            }
            bundleList.add(bundle);
        }

        ListIterator listIterator2 = this.coverList.listIterator();
        while (listIterator2.hasNext()) {
            coverList.add((int) listIterator2.next());
        }

        clone.bundleList = bundleList;
        clone.coverList = coverList;
        clone.fitness = this.fitness;
        clone.compatibility = this.compatibility;
        clone.numberOfBundles = this.getNumberOfBundles();
        clone.numberOfBundlesMoreThanOne = this.getNumberOfBundlesMoreThanOne();
        clone.numberOfBundlesEqualOne = this.getNumberOfBundlesEqualOne();
        clone.bundleIndex = this.bundleIndex;
        clone.density = this.density;
        clone.fitnessRank = this.fitnessRank;
        clone.aCompatibility = this.aCompatibility;
        clone.pCompatibility = this.pCompatibility;
        clone.sCompatibility = this.sCompatibility;
        clone.fitnessRank = this.fitnessRank;
        clone.bundleIndex = this.bundleIndex;
        clone.checksum = this.getChecksum();
        clone.crossed = this.crossed;
        clone.withCover = this.withCover;
        return clone;
    }

    public void cloneBundlesAndAttributs(GraphBundled clone) {

        ArrayList<Bundle> bundleList = new ArrayList();
        List coverList = new ArrayList();
        int i = 0;
        
        ListIterator<Bundle> listIterator = this.bundleList.listIterator();
        while (listIterator.hasNext()) {
            Bundle bundleTemp = new Bundle();
            bundleTemp = listIterator.next();
            ListIterator<Edge> listIteratorEdge = bundleTemp.getEdges().listIterator();
            Bundle bundle = new Bundle();
            while (listIteratorEdge.hasNext()) {
                Edge edge = (Edge) listIteratorEdge.next();
                bundle.add(edge);
            }
            bundle.setId(i);
            bundle.evaluate();
            bundleList.add(bundle);
            i++;
        }

        //ListIterator listIterator2 = this.coverList.listIterator();
        //while (listIterator2.hasNext()) {
        //    coverList.add((int) listIterator2.next());
        //}

        clone.bundleList = bundleList;
        clone.coverList = coverList;
        clone.fitness = this.fitness;
        clone.compatibility = this.compatibility;
        clone.numberOfBundles = this.getNumberOfBundles();
        clone.numberOfBundlesMoreThanOne = this.getNumberOfBundlesMoreThanOne();
        clone.numberOfBundlesEqualOne = this.getNumberOfBundlesEqualOne();
        clone.bundleIndex = this.bundleIndex;
        clone.density = this.density;
        clone.fitnessRank = this.fitnessRank;
        clone.aCompatibility = this.aCompatibility;
        clone.pCompatibility = this.pCompatibility;
        clone.sCompatibility = this.sCompatibility;
        clone.fitnessRank = this.fitnessRank;
        clone.bundleIndex = this.bundleIndex;
        clone.checksum = this.getChecksum();
        clone.crossed = this.crossed;
        clone.withCover = this.withCover;
    }

    
    public void cloneTotal(GraphBundled clone) {
        
        ListIterator<Node> listIteratorNode = this.nodeList.listIterator();
        while (listIteratorNode.hasNext()) {
            
            Node nodeTemp = (Node)listIteratorNode.next();   
          
            Node node = new Node(nodeTemp.getId(), nodeTemp.getData(), nodeTemp.getxOriginal(), nodeTemp.getyOriginal());
            node.setX(nodeTemp.getX());
            node.setY(nodeTemp.getY());
            node.setCover(nodeTemp.isCover());          
            clone.addNode(node);
        }
        
         ListIterator<Edge> listIteratorEdge = this.getEdgeList().listIterator();
        while (listIteratorEdge.hasNext()) {
            
            Edge edgeTemp = (Edge)listIteratorEdge.next();   
          
            int idstartNode = edgeTemp.getStartNode().getId();
            int idendNode = edgeTemp.getEndNode().getId();           
            Edge edge = new Edge(edgeTemp.getId(), edgeTemp.getData(), edgeTemp.getWeight());
            edge.setStartNode(clone.getNodeList().get(idstartNode));
            edge.setEndNode(clone.getNodeList().get(idendNode));           
            clone.addEdge(edge);
        }
                
        
        ListIterator<Bundle> listIteratorBundle = this.bundleList.listIterator();
        while (listIteratorBundle.hasNext()) {
            Bundle bundleTemp = (Bundle) listIteratorBundle.next();
            
            Bundle bundle = new Bundle();
            bundle.setId(bundleTemp.getId());
                      
            listIteratorEdge = bundleTemp.getEdges().listIterator();
            while (listIteratorEdge.hasNext()) {
                Edge edgeTemp = (Edge) listIteratorEdge.next();      
                int idEdge = edgeTemp.getId();              
                bundle.add(clone.getEdgeList().get(idEdge));
            }
            
            bundle.evaluate();
            clone.add(bundle);
        }

        ListIterator listIteratorCover = this.coverList.listIterator();
        while (listIteratorCover.hasNext()) {         
            clone.coverList.add((int) listIteratorCover.next());
        }
        
        clone.setArea(this.getArea());
        clone.setDirected(this.getDirected());
        clone.setMovedNodes(this.getMovedNodes());       
        clone.fitness = this.fitness;
        clone.compatibility = this.compatibility;
        clone.numberOfBundles = this.getNumberOfBundles();
        clone.numberOfBundlesMoreThanOne = this.getNumberOfBundlesMoreThanOne();
        clone.numberOfBundlesEqualOne = this.getNumberOfBundlesEqualOne();
        clone.bundleIndex = this.bundleIndex;
        clone.density = this.density;
        clone.fitnessRank = this.fitnessRank;
        clone.aCompatibility = this.aCompatibility;
        clone.pCompatibility = this.pCompatibility;
        clone.sCompatibility = this.sCompatibility;
        clone.fitnessRank = this.fitnessRank;
        clone.bundleIndex = this.bundleIndex;
        clone.checksum = this.getChecksum();
        clone.crossed = this.crossed;
        clone.withCover = this.withCover;
    }
        
        
    //*************************************************************************************************************
    /**
     * Mostra a lista dos vértices da cobertura
     *
     * @return listOfNodes
     */
//*************************************************************************************************************               
    public String showCoverList() {
        String listOfNodes = "Cover: ";
        int i = 1;

        ListIterator listIterator = this.coverList.listIterator();
        while (listIterator.hasNext()) {
            listOfNodes += (int) listIterator.next();
            if (i != this.coverList.size()) {
                listOfNodes += " - ";
                i++;
            }
        }
        return listOfNodes;
    }

    //*************************************************************************************************************
    /**
     * Mostra a lista dos feixes da cobertura
     *
     * @return listOfBundles
     */
//*************************************************************************************************************               
    public String showBundles() {
        String listOfBundles = "";

        ListIterator<Bundle> listIterator = this.bundleList.listIterator();
        while (listIterator.hasNext()) {
            Bundle bundle = new Bundle();
            bundle = listIterator.next();

            if (Flags.isLOG()) {
                listOfBundles += bundle.showBundleLog();
            } else {
                listOfBundles += bundle.showBundle();
            }
        }
        return listOfBundles;
    }

    public void setBundleList(ArrayList<Bundle> bundleList) {
        this.bundleList = bundleList;
    }

    public ArrayList<Bundle> getBundleList() {
        return bundleList;
    }

    public List getCoverList() {
        return coverList;
    }

    public void setCoverList(List coverList) {
        this.coverList = coverList;
    }

    public double getCompatibility() {

        return compatibility;
    }

    public int getNumberOfBundles() {
        return numberOfBundles;
    }

    public int getNumberOfBundlesMoreThanOne() {
        return numberOfBundlesMoreThanOne;
    }

    public int getNumberOfBundlesEqualOne() {
        return numberOfBundlesEqualOne;
    }

    public double getFitness() {
        return fitness;
    }

    public double getDensity() {
        return density;
    }

    public int getFitnessRank() {
        return fitnessRank;
    }

    public void setFitnessRank(int fitnessRank) {
        this.fitnessRank = fitnessRank;
    }

    public double getaCompatibility() {
        return aCompatibility;
    }

    public double getpCompatibility() {
        return pCompatibility;
    }

    public double getsCompatibility() {
        return sCompatibility;
    }

    public int getChecksum() {
        return checksum;
    }

    /**
     * @param crossed the crossed to set
     */
    public void setCrossed(int crossed) {
        this.crossed = crossed;
    }

    /**
     * @param withCover the withCover to set
     */
    public void setWithCover(int withCover) {
        this.withCover = withCover;
    }
}
